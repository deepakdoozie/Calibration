
$(function(){
  Date.prototype.getMonthName = function(lang) {
      lang = lang && (lang in Date.locale) ? lang : 'en';
      return Date.locale[lang].month_names[this.getMonth()];
  };

  Date.prototype.getMonthNameShort = function(lang) {
      lang = lang && (lang in Date.locale) ? lang : 'en';
      return Date.locale[lang].month_names_short[this.getMonth()];
  };

  Date.locale = {
      en: {
         month_names: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
         month_names_short: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
      }
  };

 $.ajax({
    type:'POST',
    url:'../libs/check_session.php',
    async:false,
  }).done(function(result){
    if(Number(result) >0){
  console.log(result);
  $.cookie('i',result);
  load_menu(result);
  }
  else{
    window.location.replace('login.html');
  }



});
})
function get_print_format(date)
{
  var d=new Date(date);
  console.log(d,1111);
  var day=d.getDate();
  var month=d.getMonthNameShort();
  var y=d.getFullYear();

  return (day+'-'+month+'-'+y);
  }
var roles;
  function load_menu(id){
  $.ajax({
    type:'POST',
    url:'../libs/auth_user.php',
    async:false,
    data:{id:id}
  }).done(function(result){
   console.log(result);
   if(Number(result.length) >0){
      roles=result;
        $('#home_href').attr('href','index.html');
        $('.previlage').text(result[0]['designation']);
        $('.logged_in').text(result[0]['user_name']);
        $('#user_img').attr('src','../img/user_images/'+result[0]['image_path']);
        $('#nav-user-img').attr('src','../img/user_images/'+result[0]['image_path']);

   }
    else{
      window.location.replace('login.html');
   }

   var dis='';
   var id=0;
   dis+='<ul class="sidebar-menu">';

      dis+='<li>';
      dis+='<a href="index.html">';
      dis+=' <i class="fa fa-dashboard"></i>';
      dis+='<span>Dashboard</span>';
      dis+='</a>';
      dis+='</li>';
     
       
        if(hasRole(8)){
        dis+='<li class="treeview">';
      dis+='<a href="#">';
      dis+=' <i class="fa fa-bar-chart-o"></i>';
      dis+=' <span>Issue to calibrator</span>';
      dis+=' <i class="fa fa-angle-left pull-right"></i>';
      dis+='</a>';
      dis+=' <ul class="treeview-menu">'
          if(hasRole(8)){
        dis+='<li id="8">';
      dis+='<a href="issue_instrument.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Issue Instrument</a></li>'; 
    }
     
        dis+='</ul>';
      dis+='</li>';
        }

        if(hasRole(9)){
        dis+='<li class="treeview">';
      dis+='<a href="#">';
      dis+=' <i class="fa fa-bar-chart-o"></i>';
      dis+=' <span>DC</span>';
      dis+=' <i class="fa fa-angle-left pull-right"></i>';
      dis+='</a>';
      dis+=' <ul class="treeview-menu">'
          if(hasRole(9)){
        dis+='<li id="9">';
      dis+='<a href="print_dc.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Print DC</a></li>';
          }
        dis+='</ul>';
      dis+='</li>';
        }

        if(hasRole(10) || hasRole(11) || hasRole(12)){
        dis+='<li class="treeview">';
      dis+='<a href="#">';
      dis+=' <i class="fa fa-bar-chart-o"></i>';
      dis+=' <span>Calibration Details</span>';
      dis+=' <i class="fa fa-angle-left pull-right"></i>';
      dis+='</a>';
      dis+=' <ul class="treeview-menu">'
          if(hasRole(10)){
        dis+='<li id="10">';
      dis+='<a href="calibration_details.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Calibration Details</a></li>';
          }
          if(hasRole(11)){
        dis+='<li id="11">';
      dis+='<a href="manage_calibration.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Manage Calibration Details</a></li>';
          }
            if(hasRole(12)){
        dis+='<li id="12">';
      dis+='<a href="calibration_report.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Calibration History</a></li>';
          }
        dis+='</ul>';
      dis+='</li>';
        }

        if(hasRole(13)|| hasRole(14) || hasRole(15) || hasRole(16)){
        dis+='<li class="treeview">';
      dis+='<a href="#">';
      dis+=' <i class="fa fa-bar-chart-o"></i>';
      dis+=' <span>Report</span>';
      dis+=' <i class="fa fa-angle-left pull-right"></i>';
      dis+='</a>';
      dis+=' <ul class="treeview-menu">'
          if(hasRole(13)){
        dis+='<li id="13">';
      dis+='<a href="calibration_pending_report.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Calibration Pending Report</a></li>';
          }
           if(hasRole(14)){
        dis+='<li id="14">';
      dis+='<a href="calibration_due_report.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Calibration Due Report</a></li>';
          }
           if(hasRole(15)){
        dis+='<li id="15">';
      dis+='<a href="issue_report.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Instrument Issued Report</a></li>';
          }
           if(hasRole(16)){
        dis+='<li id="16">';
      dis+='<a href="discarded_instrument_report.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Discarded Instrument Report</a></li>';
          }
        dis+='</ul>';
      dis+='</li>';
        }

            if(hasRole(17) || hasRole(18)){
        dis+='<li class="treeview">';
      dis+='<a href="#">';
      dis+=' <i class="a fa-bar-chart-o"></i>';
      dis+=' <span>User Management</span>';
      dis+=' <i class="fa fa-angle-left pull-right"></i>';
      dis+='</a>';
      dis+=' <ul class="treeview-menu">'
          if(hasRole(17)){
        dis+='<li id="17">';
      dis+='<a href="add_user.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Add User</a></li>';
          }
           if(hasRole(18)){
        dis+='<li id="18">';
      dis+='<a href="manage_user.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Manage User</a></li>';
          }
        
        dis+='</ul>';
      dis+='</li>';
        }
         
         if(hasRole(1) || hasRole(2) || hasRole(3) || hasRole(4) || hasRole(5) || hasRole(6) || hasRole(7)){
        dis+='<li class="treeview">';
      dis+='<a href="#">';
      dis+=' <i class="a fa-bar-chart-o"></i>';
      dis+=' <span>Master</span>';
      dis+=' <i class="fa fa-angle-left pull-right"></i>';
      dis+='</a>';
      dis+=' <ul class="treeview-menu">'
          if(hasRole(1)){
        dis+='<li id="1">';
      dis+='<a href="employment_type_master.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Employment Type Master</a></li>';
          }
           if(hasRole(2)){
        dis+='<li id="2">';
      dis+='<a href="employee_master.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Employee Master</a></li>';
          }
            if(hasRole(3)){
        dis+='<li id="3">';
      dis+='<a href="instrument_type_master.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Instrument Type Master</a></li>';
          }
         if(hasRole(4)){
        dis+='<li id="4">';
      dis+='<a href="instrument_details.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Instrument Details</a></li>';
          }
           if(hasRole(5)){
        dis+='<li id="5">';
      dis+='<a href="calibrator_type_master.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Calibrator Type Master</a></li>';
          }
           if(hasRole(6)){
        dis+='<li id="6">';
      dis+='<a href="calibrator_details.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Calibrator Details</a></li>';
          }
          if(hasRole(7)){
        dis+='<li id="7">';
      dis+='<a href="transport_master.html">';
      dis+='<i class="fa fa-angle-double-right"></i>';
      dis+='Transport Master</a></li>';
          }
          
        dis+='</ul>';
      dis+='</li>';
        }

    
     dis+=' </ul>';
     $('#nav').html(dis);
        $(function(){
         $(".sidebar-menu .treeview").tree();
    });
  })
}

function hasRole(role){
  var f=0;
  for(i=0;i<roles.length;i++){
    if(role==roles[i]['role_id']){
      f=1;
      break;
    }
  }
  return f;
}