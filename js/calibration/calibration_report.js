$(function(){
	load_all();
  load_calibrator_type();

	$('#export_excel').click(function(e){
       
      var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "-" + month + "-" + year + "_" + hour + ":" + mins;
        var a = document.createElement('a');
        var data_type = 'data:application/calibration_report.ms-excel';
        var table_div = document.getElementById('show_calibration_row');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'calibration_report' + postfix + '.xls';
        a.click();
        e.preventDefault();
         
    })

    $('#clear_btn').click(function(){
    	$('#s_ctype').val('');
    	$('#s_iname').val('');
    	$('#s_ino').val('');
    	load_all();


      $('#ctype_div').html('<label>Calibrator Type Name</label><select class="form-control filter" id="s_ctype" name="s_ctype" data-col="Calibrator Type Name" notEqual="---" data-live-search="true" placeholder="Enter Instrument Type Name"></select>');
     load_calibrator_type();
    });

    $('#view_btn').click(function(){
       var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(Number(id)>0){
    $('#view_box').show();
    $('#table_box').hide();
    $('#search_box').hide();
    var operation='loadCalibrationForSelectedInstriment';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_report.php",
    data:{operation:operation,id:id}
   }).done(function(result){
    
    var dis='';

      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td>"+result[i]['instrument_type_name']+"</td>";
        dis+="<td>"+result[i]['instrument_name']+"</td>"
        dis+="<td>"+result[i]['instrument_no']+"</td>"
        dis+="<td>"+get_print_format(result[i]['calibration_date'])+"</td>" 

        dis+="<td>"+result[i]['calibration_details']+"</td>"
        dis+="<td>"+result[i]['calibration_charges']+"</td>"
        dis+="<td>"+result[i]['other_charges']+"</td>" 

        if(result[i]['calibration_certificate_path']=='')
        {
          dis+="<td>---</td>"
        }else{
          dis+='<td><button id='+result[i]['uid']+' name='+result[i]['calibration_certificate_path']+'  > <a target="_blank" href="../documents/'+result[i]['calibration_certificate_path']+'" >View</button></td>'
        }
        
        

      dis+='</tr>';
  }

      $('#calibration_view').html(dis);

  });

   }
   else{
$('#select-calibrator').fadeIn(200).delay(5000).fadeOut(1000);

} 


    });

$('#view_close_btn').click(function(){
   $('#view_box').hide();
    $('#table_box').show();
    $('#search_box').show();
    load_all();
    load_calibrator_type();

});

});
function load_all(){
    var operation='loadAllCalibration';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_report.php",
    data:{operation:operation}
   }).done(function(result){

   	 var date=new Date();
      var today=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate());
      var dis='';

      for(i=0;i<result.length;i++){
         dis+='<tr>';
        dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";
        dis+="<td>"+result[i]['instrument_type_name']+"</td>"
        dis+="<td>"+result[i]['instrument_description']+"</td>"

        dis+='</tr>';
  }

      $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
       
      

});
}

function load_calibrator_type(){
    var operation='loadAllcalibratorType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_report.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
        dis+='<option id>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['calibrator_type_name']+'</option>';
  }
      $('#s_ctype').html(dis).selectpicker();
    
});
}
