 var filename=[];
 var oldFileName='';
 var once=1;
$(function(){
var login_by=$.cookie('i');
load_all();
load_instrument_type();
load_instrument_name();


 $('#add_select_all_employees').click(function(){

 if(this.checked){
  sf=1;
  $('.flat-red-add').each(function(){
    this.checked = true;
  })
 }else{
  sf=0;
  $('.flat-red-add').each(function(){
    this.checked=false;
  })
 } 

 }); 


  $('#edit_select_all_employees').click(function(){

 if(this.checked){
  sf=1;
  $('.flat-red-edit').each(function(){
    this.checked = true;
  })
 }else{
  sf=0;
  $('.flat-red-edit').each(function(){
    this.checked=false;
  })
 } 

 });



  $('#export_excel').click(function(e){
       
      var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "-" + month + "-" + year + "_" + hour + ":" + mins;
        var a = document.createElement('a');
        var data_type = 'data:application/instrument_details.ms-excel';
        var table_div = document.getElementById('show_tbl_row');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'instrument_details' + postfix + '.xls';
        a.click();
        e.preventDefault();
         
    })


$('#add_btn').click(function(){
	$('#add_box').show();
	$('#table_box').hide();
	$('#search_box').hide();
  load_employee_name();

});

$('#image-upload-btn').click(function(){
     $('#image-upload-btn').hide();
        $('#upload-img-shw').show();
       });

       var uploader = new plupload.Uploader({
                runtimes : 'html5,flash,silverlight', // Set runtimes, here it will use HTML5, if not supported will use flash, etc.
                browse_button : 'pickfiles', // The id on the select files button
                multi_selection: false, // Allow to select one file each time
                container : 'uploader', // The id of the upload form container
                max_file_size : '10Mb', // Maximum file size allowed
                url : '../php/upload.php', // The url to the upload.php file
                flash_swf_url : 'js/plupload.flash.swf', // The url to thye flash file
                silverlight_xap_url : 'js/plupload.silverlight.xap', // The url to the silverlight file
                filters : [
                                        {title : "All files", extensions : "jpg,gif,tiff,png,psd,php,htm,html,css,zip,rar,tar,gzip,txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv,mp3,ogg,wav,mov,mp4,f4v,flv"},
                                        {title : "Image files", extensions : "jpg,gif,tiff,png,psd"},
                                        {title : "Web files", extensions : "php,htm,html,css"},
                                        {title : "Archive files", extensions : "zip,rar,tar,gzip"},
                                        {title : "Document files", extensions : "txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv"},
                                        
                                                 ] // Filter the files that will be showed on the select files window
            });

            // RUNTIME
            uploader.bind('Init', function(up, params) {
                $('#runtime').text(params.runtime);
            });

            // Start Upload ////////////////////////////////////////////
            // When the button with the id "#uploadfiles" is clicked the upload will start
            $('#uploadfiles').click(function(e) {
                uploader.start();
                e.preventDefault();
                $('#update_flag').val(1);
                
            });

            uploader.init(); // Initializes the Uploader instance and adds internal event listeners.

            // Selected Files //////////////////////////////////////////
            // When the user select a file it wiil append one div with the class "addedFile" and a unique id to the "#filelist" div.
            // This appended div will contain the file name and a remove button
            uploader.bind('FilesAdded', function(up, files) {
                $.each(files, function(i, file) {
                    $('#filelist').append('<div class="addedFile" id="' + file.id + '">' + file.name + '<a href="#" id="' + file.id + '" class="removeFile"></a>' + '</div>');
                file=file.name;
                $('#document').val(file);
               
                });
                up.refresh(); // Reposition Flash/Silverlight
            });

            // Error Alert /////////////////////////////////////////////
            // If an error occurs an alert window will popup with the error code and error message.
            // Ex: when a user adds a file with now allowed extension
            uploader.bind('Error', function(up, err) {
                alert("Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "");
                up.refresh(); // Reposition Flash/Silverlight
            });

            

            // Progress bar ////////////////////////////////////////////
            // Add the progress bar when the upload starts
            // Append the tooltip with the current percentage
            uploader.bind('UploadProgress', function(up, file) {
              
                var progressBarValue = up.total.percent;
                
                $('#progressbar').fadeIn().progressbar({
                    value: progressBarValue

                });
                $('#progressbar .ui-progressbar-value').html('<span class="progressTooltip">' + up.total.percent + '%</span>');
            });

             uploader.bind('FileUploaded', function(up, file, info) {
              var obj = JSON.parse(info.response);
             
              filename.push(file.name);
             
            });
             

$('#save_btn').click(function(){
  var instument_id=$('#add_type').children(':selected').attr('id');
  if(Number(instument_id)>0)
  {

	if($('#add_form').valid()){

    var len=$("[type='checkbox'][name='checkbox']:checked").length;
 if(len >0){
		
     var file=$('#document').val();
       var uom=$('#add_uom').val();

     var flag=$('#update_flag').val();
    
      if(flag==''){
      y=confirm('Do you want to really save without image...?')
         if(y){
          flag=1;
          
         }

    }
    if(flag=='1'){
     var operation='saveInstrumentDetails';
		var disabled = $('#add_form').find(':input:disabled').removeAttr('disabled');
    var datastring=[];
    datastring+=$("#add_form").serialize();
    datastring+='&'+'instument_id'+'='+encodeURIComponent(instument_id)+'&';
    datastring+='file'+'='+encodeURIComponent(file)+'&';
    datastring+='uom'+'='+encodeURIComponent(uom)+'&';
    datastring+='operation'+'='+encodeURIComponent(operation)+'&'; 
     datastring+='login_by'+'='+encodeURIComponent(login_by)+'&';     
    disabled.attr('disabled','disabled');

     $.ajax({
      type: "POST",
      url: "../php/ajaxInterface/instrument_details.php",
      data:datastring
     }).done(function(result)
     {

       $("#view_emp_name > tr").each(function(){
    var x=$(this).find('input').serializeArray();
    if(x[0]['value']=='on'){

      var operation='assignEmployeeToInstrument';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation,eid:x[1]['value'],instrument_id:result}
    }).done(function(result)
    { 

        if(once>0){
          once--;
             load_all();
            $('#add-task-success').fadeIn(200).delay(5000).fadeOut(1000);
            $('#add_form')[0].reset(); 
            $('#add_box').hide();
            $('#table_box').show();
            $('#search_box').show();
          
            $('#upload-img-shw').hide();
            $('#image-upload-btn').show();
            $('#pbar').html('');
            $('#progressbar').html('');
            $('#filelist').html('');
            $('#document').val('');
            $('#update_flag').val('');

            $('#add_ename').val('');
            $('#assign_to_div').html('<label> Assign To</label><select class="form-control required" id="add_ename" name="add_ename" notEqual="---"></select>');
            load_employee_name();
            $('#add_type').val('');
            $('#type_div').html('<label> Type of Instrument</label><select class="form-control required" id="add_type" name="add_type" notEqual="---" data-live-search="true"></select>');
             load_instrument_type();

         }
     
    });

    }
  });

         });
 once=1;
	}
}

else{
$('#select-atleast-one').fadeIn(200).delay(5000).fadeOut(1000);

}
}
}else{
 $('#add_select_type_of_instrument').fadeIn(200).delay(5000).fadeOut(1000);
}
});

$('#close_btn').click(function(){
	        load_all();
            $('#add_form')[0].reset(); 
            $('#add_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_instrument_type();
           $('#upload-img-shw').hide();
            $('#image-upload-btn').show();
            $('#pbar').html('');
            $('#progressbar').html('');
            $('#filelist').html('');
            $('#document').val('');
          $('#update_flag').val('');

          $('#add_ename').val('');
          $('#assign_to_div').html('<label> Assign To</label><select class="form-control required" id="add_ename" name="add_ename" notEqual="---"></select>');
          load_employee_name()

            $('#add_type').val('');
            $('#type_div').html('<label> Type of Instrument</label><select class="form-control required" id="add_type" name="add_type" notEqual="---" data-live-search="true"></select>');
             load_instrument_type();


	});

$('#view_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
		
    $('#add_box').hide();
    $('#view_box').show();
    $('#table_box').hide();
    $('#search_box').hide();

    var operation='viewSpecificInstrument';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation,id:id}
    }).done(function(result)
    { 
      console.log(result);
    	$('#view_type').val(result[0]['instrument_type_name']);
    	$('#view_iname').val(result[0]['instrument_name']);
      $('#view_ino').val(result[0]['instrument_no']);
      $('#view_pname').val(result[0]['purchesed_by']);
      $('#view_pdate').val(result[0]['purchesed_date']);
      $('#view_cdate').val(result[0]['first_calibration_date']);
      $('#view_cfreq').val(result[0]['calibration_frequency']);
      $('#view_adays').val(result[0]['calibration_alert_days']);
      $('#view_cost').val(result[0]['purchesed_cost']);
      $('#view_code').val(result[0]['punch_code']);
      $('#view_remark').val(result[0]['remarks']);
      $('#view_used').val(result[0]['used_at']);
      $('#view_uom').val(result[0]['uom']);
      $('#show').html('<button > <a target="_blank" href="../documents/'+result[0]['document_path']+'" >View Document</button>');
      var dis='';
      for (var i = 0; i < result.length; i++) {
       dis+='<tr>';
       dis+='<td>'+result[i]['first_name']+' '+result[i]['middle_name']+' '+result[i]['last_name']+'</td>';
       dis+='</tr>';
      };
      $('#view_employee_name').html(dis);

    });
}
else{
$('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);

}
});

$('#view_close_btn').click(function(){
	
    $('#view_box').hide();
    $('#table_box').show();
    $('#search_box').show();
    load_all();
    load_instrument_type();

	});


$('#edit_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
    $('#edit_box').show();
    $('#table_box').hide();
    $('#search_box').hide();
     $('#edit_id').val(id);
  var employeeRst=[];
    var operation='editSpecificInstrument';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation,id:id},
    async:false
    }).done(function(result)
    { 
    	console.log(result);
      console.log(result[0]['instrument_type_id']);
      var dis='<option id='+result[0]['instrument_type_id']+'>'+result[0]['instrument_type_name']+'</option>';
      var operation='loadAllInstrumentType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation},
    async:false
   }).done(function(result3){
    var dis1='';
      for(i=0;i<result3.length;i++){  
      if(result['instrument_type_id'] != result3[i]['uid']){  
         dis1+='<option id='+result3[i]['uid']+'>'+result3[i]['instrument_type_name']+'</option>';
  }
}
  $('#edit_type').html(dis+dis1).selectpicker();
     });
      

      $('#edit_iname').val(result[0]['instrument_name']);
      $('#edit_ino').val(result[0]['instrument_no']);
      $('#edit_pname').val(result[0]['purchesed_by']);
      $('#edit_pdate').val(result[0]['purchesed_date']);
      $('#edit_cdate').val(result[0]['first_calibration_date']);
      $('#edit_cfreq').val(result[0]['calibration_frequency']);
      $('#edit_adays').val(result[0]['calibration_alert_days']);
      $('#edit_cost').val(result[0]['purchesed_cost']);
       $('#edit_code').val(result[0]['punch_code']);
        $('#edit_remark').val(result[0]['remarks']);
          $('#edit_used').val(result[0]['used_at']);

          $('#edit_uom').val(result[0]['uom']);

            oldFileName=result[0]['document_path'];
    
     var operation='loadAllEmployee';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation},
    async:false
   }).done(function(result){
      var dis='';
      employeeRst=result;
      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='checkbox' class='flat-red-edit'  name='edit_checkbox' id='e"+result[i]['uid']+"'></td>";
        dis+="<td>"+result[i]['first_name']+''+result[i]['middle_name']+''+result[i]['last_name']+"</td>"
        dis+='<input type="hidden" name="eid" value='+result[i]['uid']+'>';
        dis+='</tr>';
  }
      $('#edit_employee_name').html(dis);
  });
 console.log(employeeRst);
  console.log(result);

  for(l=0;l<employeeRst.length;l++){
     for(m=0;m<result.length;m++){
      if(employeeRst[l]['uid']==result[m]['employee_id']){
        $('#e'+result[m]['employee_id']).prop('checked',true);
     
    }
     }
   }


 
    
 });
}
else{
$('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);

}
});

 $('#edit_image-upload-btn').click(function(){
     $('#edit_image-upload-btn').hide();
        $('#edit_upload-img-shw').show();
       });

       var edit_uploader = new plupload.Uploader({
                runtimes : 'html5,flash,silverlight', // Set runtimes, here it will use HTML5, if not supported will use flash, etc.
                browse_button : 'edit_pickfiles', // The id on the select files button
                multi_selection: false, // Allow to select one file each time
                container : 'edit_uploader', // The id of the upload form container
                max_file_size : '10Mb', // Maximum file size allowed
                url : '../php/upload.php', // The url to the upload.php file
                flash_swf_url : 'js/plupload.flash.swf', // The url to thye flash file
                silverlight_xap_url : 'js/plupload.silverlight.xap', // The url to the silverlight file
                filters : [
                                        {title : "All files", extensions : "jpg,gif,tiff,png,psd,php,htm,html,css,zip,rar,tar,gzip,txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv,mp3,ogg,wav,mov,mp4,f4v,flv"},
                                        {title : "Image files", extensions : "jpg,gif,tiff,png,psd"},
                                        {title : "Web files", extensions : "php,htm,html,css"},
                                        {title : "Archive files", extensions : "zip,rar,tar,gzip"},
                                        {title : "Document files", extensions : "txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv"},
                                        
                                                 ]

            });

            // RUNTIME
            edit_uploader.bind('Init', function(up, params) {
                $('#runtime').text(params.runtime);
            });

            // Start Upload ////////////////////////////////////////////
            // When the button with the id "#uploadfiles" is clicked the upload will start
            $('#edit_uploadfiles').click(function(e) {
                edit_uploader.start();
                e.preventDefault();
                $('#update_flag').val(1);
            });

            edit_uploader.init(); // Initializes the Uploader instance and adds internal event listeners.

            // Selected Files //////////////////////////////////////////
            // When the user select a file it wiil append one div with the class "addedFile" and a unique id to the "#filelist" div.
            // This appended div will contain the file name and a remove button
            edit_uploader.bind('FilesAdded', function(up, files) {
                $.each(files, function(i, file) {
                    $('#edit_filelist').append('<div class="addedFile" id="' + file.id + '">' + file.name + '<a href="#" id="' + file.id + '" class="removeFile"></a>' + '</div>');
                file=file.name;
                $('#edit_document').val(file);
               
                });
                up.refresh(); // Reposition Flash/Silverlight
            });

            // Error Alert /////////////////////////////////////////////
            // If an error occurs an alert window will popup with the error code and error message.
            // Ex: when a user adds a file with now allowed extension
            edit_uploader.bind('Error', function(up, err) {
                alert("Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "");
                up.refresh(); // Reposition Flash/Silverlight
            });

            

            // Progress bar ////////////////////////////////////////////
            // Add the progress bar when the upload starts
            // Append the tooltip with the current percentage
            edit_uploader.bind('UploadProgress', function(up, file) {
                var progressBarValue = up.total.percent;
                $('#edit_progressbar').fadeIn().progressbar({
                    value: progressBarValue
                });
                $('#progressbar .ui-progressbar-value').html('<span class="progressTooltip">' + up.total.percent + '%</span>');
            });

             edit_uploader.bind('FileUploaded', function(up, file, info) {
              var obj = JSON.parse(info.response);
             
              filename.push(file.name);
            
            });


$('#update_btn').click(function(){
  var x=confirm('Do you want to really update...?')
  if(x){
	if($('#edit_form').valid()){
      var len=$("[type='checkbox'][name='edit_checkbox']:checked").length;
 if(len >0){

		
		var id= $('#edit_id').val();
    var instument_id=$('#edit_type').children(':selected').attr('id');
    var uom=$('#edit_uom').val();
    var document_file=$('#edit_document').val();

    var document_flag=$('#update_flag').val();
    if(document_flag==''){
      y=confirm('Do you want to really update without image...?')
         if(y){
          document_flag=1;
          document_file=oldFileName;
         }

    }
    if(document_flag=='1'){
      var operation='updateInstrumentDetails';
      var disabled = $('#edit_form').find(':input:disabled').removeAttr('disabled');
    var datastring=[];
    datastring+=$("#edit_form").serialize();
    datastring+='&'+'instument_id'+'='+encodeURIComponent(instument_id)+'&';
    datastring+='uom'+'='+encodeURIComponent(uom)+'&';
    datastring+='file'+'='+encodeURIComponent(document_file)+'&';
    datastring+='operation'+'='+encodeURIComponent(operation)+'&';
        datastring+='login_by'+'='+encodeURIComponent(login_by)+'&';
    datastring+='id'+'='+encodeURIComponent(id)+'&';
    

     $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/instrument_details.php",
         data:datastring,
         async:false
         }).done(function(result)
         {

         });
         $("#edit_employee_name > tr").each(function(){
    var x=$(this).find('input').serializeArray();
    if(x[0]['value']=='on'){

      var operation='updateAassignEmployeeToInstrument';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation,eid:x[1]['value'],instrument_id:id},
    async:false
    }).done(function(result)
    { 

        if(once>0){
          once--;
          
             $('#edit-update-success').fadeIn(200).delay(5000).fadeOut(1000);
            $('#edit_form')[0].reset(); 
            $('#edit_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           $('#edit_pbar').html('');
               $('#edit_progressbar').html('');
                 $('#edit_filelist').html('');
          $('#edit_document').val('');
          $('#update_flag').val('');
         }
        
         });
}
  });
            load_all();
             load_instrument_type();
       
       }
     }
     else{
$('#edit_select-atleast-one').fadeIn(200).delay(5000).fadeOut(1000);

     }
	}
}
});

$('#edit_clear_btn').click(function(){
	        load_all();
             load_instrument_type();
            $('#edit_form')[0].reset(); 
            $('#edit_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           $('#edit_pbar').html('');
               $('#edit_progressbar').html('');
                 $('#edit_filelist').html('');
          $('#edit_document').val('');
          $('#update_flag').val('');

	});

$('#clear_btn').click(function(){
	      
           $('#s_iname').val('');
           $('#s_des').val('');
           load_all();
           load_instrument_type();
           $('#iname_div').html('<select class="form-control filter" id="s_iname" name="s_iname" data-col="Instrument Name" notEqual="---" data-live-search="true"> <option>---</option></select>')

	});

$('#ena-dis-btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
    $('#active_box').show();
     $('#search_box').hide();
      $('#table_box').hide();
      $('#active_id').val(id);
  }
  else{
  $('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);
  }


});

$('#delete_btn').click(function(){
  var x=confirm('Do you want to really Delete...?')
  if(x){
    var id=$('#active_id').val();
    var reason=$('#add_reason').val();

     var operation='activeDeactiveInstrument';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation,id:id,reason:reason}
   }).done(function(result){
 
      $('#active_box').hide();
     $('#search_box').show();
      $('#table_box').show();
    $('#delete-success').fadeIn(200).delay(5000).fadeOut(1000);
 load_all();


   });
  }


});


$('#delete_clear_btn').click(function(){
   $('#active_box').hide();
     $('#search_box').show();
      $('#table_box').show();
      $('#add_reason').val('');

});

$('#clear_btn').click(function(){
           $('#s_itype').val('');
           $('#s_iname').val('');
       
           $('#itype_div').html('<label>Instrument Type</label><select class="form-control filter" id="s_itype" name="s_itype" data-col="Instrument Type" notEqual="---" data-live-search="true"><option>---</option></select>');
           $('#iname_div').html('<label>Instrument Name</label><select class="form-control filter" id="s_iname" name="s_iname" data-col="Instrument Name" notEqual="---" data-live-search="true"><option>---</option></select>');
    load_all();
           load_instrument_type();
           load_instrument_name();
});




});

function load_all(){
    var operation='getAllInstrument';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
      $('#total').text(result.length);

      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";
        dis+="<td>"+result[i]['instrument_type_name']+"</td>";
        dis+="<td>"+result[i]['instrument_name']+"</td>"
        dis+="<td>"+result[i]['instrument_no']+"</td>";
        dis+="<td>"+result[i]['purchesed_by']+"</td>"
        dis+="<td>"+get_print_format(result[i]['purchesed_date'])+"</td>";
        dis+="<td>"+get_print_format(result[i]['first_calibration_date'])+"</td>"
        
      dis+='</tr>';
  }
      $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
      

});
}

function load_instrument_type(){
    var operation='loadAllInstrumentType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
        dis+='<option id>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['instrument_type_name']+'</option>';
  }
      $('#s_itype').html(dis).selectpicker();
      $('#add_type').html(dis).selectpicker();
       
      

});
}

function load_employee_name(){
   var operation='loadAllEmployee';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
      
      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='checkbox' class='flat-red-add'  name='checkbox' id="+result[i]['uid']+"></td>";
        dis+="<td>"+result[i]['e_id']+"</td>";
        dis+="<td>"+result[i]['first_name']+' '+result[i]['middle_name']+' '+result[i]['last_name']+"</td>"
        dis+='<input type="hidden" name="eid" value='+result[i]['uid']+'>';
        dis+='</tr>';
  }
      $('#view_emp_name').html(dis);
  });
 }

 function load_instrument_name(){
    var operation='loadAllInstrumentName';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/instrument_details.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
        dis+='<option id>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option>'+result[i]['instrument_name']+'</option>';
  }
      $('#s_iname').html(dis).selectpicker();
     
       
      

});
}
  