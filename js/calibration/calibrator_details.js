var instrumentType='';
var rs1='';
$(function(){
  var login_by=$.cookie('i');
	load_all();
load_calibrator_type();
load_calibrator_name();


 $('#add_select_all_instrument_type').click(function(){

 if(this.checked){
  sf=1;
  $('.flat-red-add').each(function(){
    this.checked = true;
  })
 }else{
  sf=0;
  $('.flat-red-add').each(function(){
    this.checked=false;
  })
 } 

 });



  $('#edit_select_all_instrument_type').click(function(){

 if(this.checked){
  sf=1;
  $('.flat-red-edit').each(function(){
    this.checked = true;
  })
 }else{
  sf=0;
  $('.flat-red-edit').each(function(){
    this.checked=false;
  })
 } 

 });

$('#export_excel').click(function(e){
       
      var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "-" + month + "-" + year + "_" + hour + ":" + mins;
        var a = document.createElement('a');
        var data_type = 'data:application/calibrator_details.ms-excel';
        var table_div = document.getElementById('show_tbl_row');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'calibrator_details' + postfix + '.xls';
        a.click();
        e.preventDefault();
         
    })



$('#add_btn').click(function(){
	$('#add_box').show();
	$('#table_box').hide();
	$('#search_box').hide();
  load_instrument_type();
	    var operation='loadCalibrator';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
    data:{operation:operation}
   }).done(function(result){
     var dis='';
        dis+='<option id="-1">---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['calibrator_type_name']+'</option>';
  }
      $('#calibrator_type').html(dis).selectpicker();

   });

});

$('#save_btn').click(function(){
  var calibrator_id=$('#calibrator_type').children(':selected').attr('id');
  if(Number(calibrator_id)>0){
	if($('#add_form').valid()){
		var id='';
		var rs='';
	
  var calibrator=$('#calibrator_type').val();

	var disabled = $('#add_form').find(':input:disabled').removeAttr('disabled');
    var datastring=[];
    datastring+=$("#add_form").serialize();
    datastring+='&'+'calibrator_id'+'='+encodeURIComponent(calibrator_id)+'&';
     datastring+='&'+'login_by'+'='+encodeURIComponent(login_by)+'&';
  
        if(calibrator != '---'){
        var len=$("[type='checkbox'][name='checkbox']:checked").length;
     
        if(len>0){
        $.ajax({
       type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
         data:datastring,
         async:false
         }).done(function(result)
         {
      
         	id=result;

         });

          $("#instrument_tbl > tr").each(function(){
    var x=$(this).find('input').serializeArray();
  
    if(x.length>0)
    if(x[0]['value']=='on'){
      var datastring=[];
        datastring+='operation'+'='+encodeURIComponent('saveCalibratorInstrument')+'&';
        datastring+='id'+'='+encodeURIComponent(id)+'&'; 
        datastring+=$(this).find('input').serialize();
      
        $.ajax({
          type:'POST',
          url: "../php/ajaxInterface/calibrator_details.php",
         data:datastring,
          async:false
         }).done(function(result)
         {
         
         	rs=result;


        });
    }
  });
        
          if(rs){
          
  $('#add-task-success').fadeIn(200).delay(5000).fadeOut(1000);
   $('#add_form')[0].reset(); 
            $('#add_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_all();
          }
          else{
        
          }


	}
else{
 $('#select-one-instrument').fadeIn(200).delay(5000).fadeOut(1000);

}
}
  else{
  
  $('#select-dif-name').fadeIn(200).delay(5000).fadeOut(1000);
  }
}
}else{
  $('#select_calibration_type_error').fadeIn(200).delay(5000).fadeOut(1000);
}
});

$('#save_cancel_btn').click(function(){
	$('#add_form')[0].reset(); 
            $('#add_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_all();

});


$('#view_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
    $('#view_box').show();
    $('#table_box').hide();
    $('#search_box').hide();
    var operation='viewSpecificCalibratorDetails';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
    data:{operation:operation,id:id}
   }).done(function(result){

   	$('#view_calibrator_type').val(result[0]['calibrator_type_name']);
   	$('#view_calibrator_name').val(result[0]['calibrator_name']);
   	$('#view_calibrator_email').val(result[0]['calibrator_email']);
   	$('#view_calibrator_contact').val(result[0]['calibrator_contact_no']);
   	$('#view_contact_name').val(result[0]['contact_person_name']);
   	$('#view_contact_number').val(result[0]['contact_person_no']);
   	$('#view_additional_contact_number').val(result[0]['additional_contact_person_no']);
   	$('#view_email').val(result[0]['contact_person_email']);
   	$('#view_additional_email').val(result[0]['additional_email']);
   	$('#view_credit_days').val(result[0]['credit_days']);
   	$('#view_add_address').val(result[0]['calibrator_address']);
     
      var dis='';

      for(i=0;i<result.length;i++){
         dis+='<tr>';
        dis+="<td>"+result[i]['instrument_type_name']+"</td>";        
      dis+="<td>"+result[i]['instrument_description']+"</td>";        

      dis+='</tr>';
  }
      $('#view_instrument_tbl').html(dis);
   });
    }
    else{
  $('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);
    }
});

$('#view_close_btn').click(function(){
	$('#view_form')[0].reset(); 
            $('#view_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_all();

});

$('#edit_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
   	$('#edit_id').val(id);
    $('#edit_box').show();
    $('#table_box').hide();
    $('#search_box').hide();
    var operation='editSpecificCalibratorDetails';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
    data:{operation:operation,id:id},
    async:false
   }).done(function(result){

   	$('#edit_calibrator_name').val(result[0]['calibrator_name']);
   	$('#edit_calibrator_email').val(result[0]['calibrator_email']);
   	$('#edit_calibrator_contact').val(result[0]['calibrator_contact_no']);
   	$('#edit_contact_name').val(result[0]['contact_person_name']);
   	$('#edit_contact_number').val(result[0]['contact_person_no']);
   	$('#edit_additional_contact_number').val(result[0]['additional_contact_person_no']);
   	$('#edit_email').val(result[0]['contact_person_email']);
   	$('#edit_additional_email').val(result[0]['additional_email']);
   	$('#edit_credit_days').val(result[0]['credit_days']);
   	$('#edit_add_address').val(result[0]['calibrator_address']);
    $('#edit_calibrator_type').val(result[0]['calibrator_type_name']);
    

      var dis='<option id='+result[0]['calibrator_type_id']+'>'+result[0]['calibrator_type_name']+'</option>';
     var operation='loadAllcalibratorType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
    data:{operation:operation},
     async:false
   }).done(function(result2){
      for(i=0;i<result2.length;i++){ 
      if(result[0]['calibrator_type_id'] != result2[i]['uid']){  
         dis+='<option id='+result2[i]['uid']+'>'+result2[i]['calibrator_type_name']+'</option>';
  }
}
      $('#edit_calibrator_type').html(dis).selectpicker();
   });
    
     var operation='loadALLInstrumentType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
    data:{operation:operation}
   }).done(function(result3){
   	
      var dis='';
     instrumentType=result3;
      for(i=0;i<result3.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='checkbox' class='flat-red-edit' name='checkbox1' id='i"+result3[i]['uid']+"'><input type='hidden' class='flat-red' name='edit_check' value="+result3[i]['uid']+"></td>";
        dis+="<td>"+result3[i]['instrument_type_name']+"</td>";        
        dis+="<td>"+result3[i]['instrument_description']+"</td>"
        
      dis+='</tr>';
  }
      $('#edit_instrument_tbl').html(dis);
            



      for(l=0;l<result.length;l++){
     for(m=0;m<instrumentType.length;m++){
      if(result[l]['instrument_type_id']==instrumentType[m]['uid']){
        $('#i'+instrumentType[m]['uid']).prop('checked',true);
     
    }
     }
   }
  


});

});
    }
    else{
  $('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);
    }
});

$('#edit_close_btn').click(function(){
	$('#edit_form')[0].reset(); 
            $('#edit_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_all();

});

$('#update_btn').click(function(){
	/*var x=confirm('Do you want to really Update...?')
  if(x){*/
	if($('#edit_form').valid()){
var x=confirm('Do you want to really Update...?')
  if(x){
		var id='';
		var rs='';
	var id=$('#edit_id').val();
	var calibrator_id=$('#edit_calibrator_type').children(':selected').attr('id');
   
    var len=$("[type='checkbox'][name='checkbox1']:checked").length;
       
        if(len>0){
	var disabled = $('#edit_form').find(':input:disabled').removeAttr('disabled');
    var datastring=[];
    datastring+=$("#edit_form").serialize();
    datastring+='&'+'calibrator_id'+'='+encodeURIComponent(calibrator_id)+'&';
    datastring+='&'+'id'+'='+encodeURIComponent(id)+'&';
    datastring+='operation'+'='+encodeURIComponent('updateCalibratorDetails')+'&';
    datastring+='login_by'+'='+encodeURIComponent(login_by)+'&';


        $.ajax({
       type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
         data:datastring,
         async:false
         }).done(function(result)
         {
           console.log(result);
         });
          var id1=$('#edit_id').val();
          var operation='deleteCalibratorInstrument';
           $.ajax({
       type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
         data:{operation:operation,id:id1},
         async:false
         }).done(function(result)
         {
      
         });


          $("#edit_instrument_tbl > tr").each(function(){
    var x=$(this).find('input').serializeArray();
 
    if(x.length>0)
    if(x[0]['value']=='on'){
    
      var datastring=[];
        datastring+='operation'+'='+encodeURIComponent('updateCalibratorInstrument')+'&';
        datastring+='id'+'='+encodeURIComponent(id1)+'&'; 
        datastring+=$(this).find('input').serialize();

        $.ajax({
          type:'POST',
          url: "../php/ajaxInterface/calibrator_details.php",
         data:datastring,
          async:false
         }).done(function(result)
         {
      
         	rs1=result;


        });
    }
  });
      
          if(rs1){
          	
  $('#edit-update-success').fadeIn(200).delay(5000).fadeOut(1000);
   $('#edit_form')[0].reset(); 
            $('#edit_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_all();
          }
          else{
      
          }

}else{
 $('#select-one-instrument1').fadeIn(200).delay(5000).fadeOut(1000);
	}
}
}
//}

});

$('#clear_btn').click(function(){
           $('#s_ctype').val('');
           $('#s_cname').val('');
       
           $('#ctype_div').html('<label>Calibrator Type Name</label><select class="form-control filter" id="s_ctype" name="s_ctype" data-col="Calibrator Type Name" notEqual="---" data-live-search="true"><option>---</option></select>');
           $('#cname_div').html('<label>Calibrator Name</label><select class="form-control filter" id="s_cname" name="s_cname" data-col="Calibrator Name" notEqual="---" data-live-search="true"><option>---</option></select></div>');
            load_all();
load_calibrator_type();
load_calibrator_name();

});

$('#active_dea_btn').click(function(){
	var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
  var x=confirm('Do you want to really Acitve/Deactive...?')
  if(x){
     var operation='activeDeactiveCalibrator';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
    data:{operation:operation,id:id}
   }).done(function(result){

    if(result){
     load_all();
      id='';

    $('#delete-success').fadeIn(200).delay(5000).fadeOut(1000);
     window.location.reload();
     load_calibrator_type();
     load_calibrator_name();
  
    }
   });
  }
}
else{
 $('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);
}


});


});

function load_calibrator_type(){
    var operation='loadAllcalibratorType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
        dis+='<option>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['calibrator_type_name']+'</option>';
  }
      // $('#calibrator_type').html(dis).selectpicker();
      $('#s_ctype').html(dis).selectpicker();
       
      

});
}

function load_instrument_type(){
    var operation='loadALLInstrumentType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';

      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='checkbox' class='flat-red-add' name='checkbox' id="+result[i]['uid']+"><input type='hidden' class='flat-red' name='check' value="+result[i]['uid']+"></td>";

        dis+="<td>"+result[i]['instrument_type_name']+"</td>";
        dis+="<td>"+result[i]['instrument_description']+"</td>"
        
      dis+='</tr>';
  }
      $('#instrument_tbl').html(dis);
       
      

});
}

function load_all(){
    var operation='loadAllCalibratorDetails';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
    $('#total').text(result.length);
      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";
        dis+="<td>"+result[i]['calibrator_type_name']+"</td>";
        dis+="<td>"+result[i]['calibrator_name']+"</td>"
        dis+="<td>"+result[i]['calibrator_email']+"</td>"
        dis+="<td>"+result[i]['calibrator_contact_no']+"</td>"       
       if(result[i]['active_flag']=='1'){
      dis+="<td>"+"Active"+"</td>"       
       }
       else{
    dis+="<td style='color:red'>"+"De-Active"+"</td>" 
       }
      dis+='</tr>';
  }
      $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
       
      

});
}

function load_calibrator_name(){
    var operation='loadAllcalibratorname';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_details.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
        dis+='<option></option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['calibrator_name']+'</option>';
  }
     
      $('#s_cname').html(dis).selectpicker();
       
      

});
}