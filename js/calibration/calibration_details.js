 var filename=[];
$(function(){
	load_all();
  load_instrument_type();
var login_by=$.cookie('i');
	$('#add_btn').click(function(e){
      var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
    load_calibrator_type();
    OnChangeInstrumentType();
    $('#add_box').show();
    $('#table_box').hide();
    $('#search_box').hide();
     $('#add_id').val(id);

    var operation='viewSpecificInstrument';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/calibration_details.php",
    data:{operation:operation,id:id}
    }).done(function(result)
    { 
      $('#view_type').val(result['instrument_type_name']);
      $('#view_iname').val(result['instrument_name']);
      $('#view_ino').val(result['instrument_no']);
      $('#view_pname').val(result['purchesed_by']);
      $('#view_pdate').val(result['purchesed_date']);
      $('#view_cdate').val(result['next_calibration_date']);
      $('#view_cfreq').val(result['calibration_frequency']);
      $('#view_adays').val(result['calibration_alert_days']);
      $('#view_cost').val(result['purchesed_cost']);
      var name=result['first_name']+result['middle_name']+result['last_name']
      $('#view_ename').val(name);
      var dc_no=$("input[type='radio'][name='ch']:checked").attr('id1');

      $('#view_dcno').val(dc_no);



    });
}else{
$('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);

}      
    });

 $('#calibrator_type').on('change',function(){

  var calibratorType=$('#calibrator_type').val();
   var calibratorTypeId=$('#calibrator_type').children(':selected').attr('id');
 
   if(calibratorType != '---'){
       $('#calibrator').val('');
      $('#calibrator_div').html('<label>Calibrator </label><select class="form-control required" name="calibrator" id="calibrator"  notEqual="---" data-live-search="true"></select>');

    var operation='loadCalibrator';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/calibration_details.php",
    data:{operation:operation,calibratorTypeId:calibratorTypeId}
    }).done(function(result)
    { 
     
       var dis='';
          
        dis+='<option id>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['calibrator_name']+'</option>';
  }
      $('#calibrator').html(dis).selectpicker();
    });

   }else{
  
    $('#calibrator').val('');

    $('#calibrator_div').html('<label>Calibrator </label><select class="form-control required" name="calibrator" id="calibrator"  notEqual="---" data-live-search="true"></select>');
    

   }
 });

$('#image-upload-btn').click(function(){
     $('#image-upload-btn').hide();
        $('#upload-img-shw').show();
       });

 var uploader = new plupload.Uploader({
                runtimes : 'html5,flash,silverlight', // Set runtimes, here it will use HTML5, if not supported will use flash, etc.
                browse_button : 'pickfiles', // The id on the select files button
                multi_selection: false, // Allow to select one file each time
                container : 'uploader', // The id of the upload form container
                max_file_size : '10Mb', // Maximum file size allowed
                url : '../php/upload.php', // The url to the upload.php file
                flash_swf_url : 'js/plupload.flash.swf', // The url to thye flash file
                silverlight_xap_url : 'js/plupload.silverlight.xap', // The url to the silverlight file
               filters : [
                                        {title : "All files", extensions : "jpg,gif,tiff,png,psd,php,htm,html,css,zip,rar,tar,gzip,txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv,mp3,ogg,wav,mov,mp4,f4v,flv"},
                                        {title : "Image files", extensions : "jpg,gif,tiff,png,psd"},
                                        {title : "Web files", extensions : "php,htm,html,css"},
                                        {title : "Archive files", extensions : "zip,rar,tar,gzip"},
                                        {title : "Document files", extensions : "txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv"},
                                        
                                                 ]
            });

            // RUNTIME
            uploader.bind('Init', function(up, params) {
                $('#runtime').text(params.runtime);
            });

            // Start Upload ////////////////////////////////////////////
            // When the button with the id "#uploadfiles" is clicked the upload will start
            $('#uploadfiles').click(function(e) {
                uploader.start();
                e.preventDefault();
                $('#update_flag').val(1);
            });

            uploader.init(); // Initializes the Uploader instance and adds internal event listeners.

            // Selected Files //////////////////////////////////////////
            // When the user select a file it wiil append one div with the class "addedFile" and a unique id to the "#filelist" div.
            // This appended div will contain the file name and a remove button
            uploader.bind('FilesAdded', function(up, files) {
                $.each(files, function(i, file) {
                    $('#filelist').append('<div class="addedFile" id="' + file.id + '">' + file.name + '<a href="#" id="' + file.id + '" class="removeFile"></a>' + '</div>');
                file=file.name;
                $('#document').val(file);
             
                });
                up.refresh(); // Reposition Flash/Silverlight
            });

            // Error Alert /////////////////////////////////////////////
            // If an error occurs an alert window will popup with the error code and error message.
            // Ex: when a user adds a file with now allowed extension
            uploader.bind('Error', function(up, err) {
                alert("Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "");
                up.refresh(); // Reposition Flash/Silverlight
            });

            

            // Progress bar ////////////////////////////////////////////
            // Add the progress bar when the upload starts
            // Append the tooltip with the current percentage
            uploader.bind('UploadProgress', function(up, file) {
                var progressBarValue = up.total.percent;
                $('#progressbar').fadeIn().progressbar({
                    value: progressBarValue
                });
                $('#progressbar .ui-progressbar-value').html('<span class="progressTooltip">' + up.total.percent + '%</span>');
            });

             uploader.bind('FileUploaded', function(up, file, info) {
              var obj = JSON.parse(info.response);
             
              filename.push(file.name);
           
            });

   
    $('#save_btn').click(function(){
      if($('#add_form').valid()){
    // var calibratorTypeId=$('#calibrator_type').children(':selected').attr('id');
    var calibrationDetails=$('#calibration_details').val();
    var nextCalibrationDate=$('#calibrator_date').val();
     var calibrationCharges=$('#calibrator_charge').val();
      var otherCharges=$('#other_charge').val();

     var file=$('#document').val();
     var updateFlag=$('#update_flag').val();
     var id=$('#add_id').val();
     // var calibratorId=$('#calibrator').children(':selected').attr('id');


     var reportNo=$('#add_report_no').val();
    var reffStd=$('#add_std').val();
     var code=$('#add_code').val();
    var remark=$('#add_remark').val();  



         if(updateFlag==''){
      y=confirm('Do you want to really add without documents...?')
         if(y){
          updateFlag=1;
          file='';
         }

    }
    if(updateFlag=='1'){
     var operation='saveCalibrationDetails';
         $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_details.php",
    data:{operation:operation,id:id,calibrationDetails:calibrationDetails,nextCalibrationDate:nextCalibrationDate,calibrationCharges:calibrationCharges,otherCharges:otherCharges,file:file,reportNo:reportNo,reffStd:reffStd,code:code,remark:remark,login_by:login_by}
   }).done(function(result){
       
        if(result){
            $('#add-task-success').fadeIn(200).delay(5000).fadeOut(1000);

          $('#add_form')[0].reset();
          $('#add_box').hide();
          $('#table_box').show();
          $('#search_box').show();
          $('#image-upload-btn').show();
          $('#upload-img-shw').hide();
          $('#document').val('');
           $('#update_flag').val('');
            $('#add_id').val('');

            $('#calibrator_type').val('');
            $('#calibrator').val('');

            $('#add_ctype_div').html('<label>Calibrator Type</label><select class="form-control required" name="calibrator_type" id="calibrator_type" data-col="Calibrator Type Name" notEqual="---" data-live-search="true"></select>');
           $('#calibrator_div').html('<label>Calibrator </label><select class="form-control required" name="calibrator" id="calibrator"  notEqual="---" data-live-search="true"></select>');
         OnChangeInstrumentType(); 
              $('#pbar').html('');
               $('#progressbar').html('');
              $('#filelist').html('');
              $('#document').val('');
              $('#update_flag').val('');
              load_all();
        }
   });


}
else{
 $('#select-calibrator').fadeIn(200).delay(5000).fadeOut(1000);
}
      }


    });

 $('#close_btn').click(function(){
  $('#add_form')[0].reset();
          $('#add_box').hide();
          $('#table_box').show();
          $('#search_box').show();
          $('#image-upload-btn').show();
          $('#upload-img-shw').hide();
          $('#document').val('');
           $('#update_flag').val('');
            $('#add_id').val('');
            $('#calibrator_type').val('---');

            $('#calibrator_type').val('');
            $('#calibrator').val('');

            $('#add_ctype_div').html('<label>Calibrator Type</label><select class="form-control required" name="calibrator_type" id="calibrator_type" data-col="Calibrator Type Name" notEqual="---" data-live-search="true"></select>');
  $('#calibrator_div').html('<label>Calibrator </label><select class="form-control required" name="calibrator" id="calibrator"  notEqual="---" data-live-search="true"></select>');
OnChangeInstrumentType();

               $('#document').val('');
              $('#update_flag').val('');
 });

  

    $('#search-btn').click(function(){
  var instrument_id=$('#s_itype').children(':selected').attr('id');
  var name_id=$('#s_iname').children(':selected').attr('id');
   
   
   var operation='loadSelectedInstrumentDetails';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_details.php",
    data:{operation:operation,instrument_id:instrument_id,nameId:name_id}
   }).done(function(result){
  
      var dis='';
    for(i=0;i<result.length;i++){
         dis+='<tr>';
        dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";

        dis+="<td>"+result[i]['instrument_type_name']+"</td>";
        dis+="<td>"+result[i]['instrument_name']+"</td>"
        dis+="<td>"+result[i]['instrument_no']+"</td>"
        dis+="<td>"+result[i]['purchesed_by']+"</td>" 
        dis+="<td>"+result[i]['purchesed_date']+"</td>"
        var calibrationDate=result[i]['next_calibration_date'];
       var m=moment(calibrationDate,'YYYY-MM-DD');
       var newdate=moment(m).add(result[i]['calibration_frequency'],'days');
       var calibrationpending=moment(newdate).toDate();
      var date = moment(calibrationpending).format('YYYY-MM-DD');
        dis+="<td>"+date+"</td>"
      dis+='</tr>';
  
}
  $('#view').html(dis);
});

    });

 $('#clear_btn').click(function(){
           $('#s_itype').val('');
           $('#s_iname').val('');
       
           $('#itype_div').html('<label>Instrument Type</label><select class="form-control filter" id="s_itype" name="s_itype" data-col="Instrument Type" notEqual="---" data-live-search="true" placeholder="Enter Instrument Type Name"></select>');
           $('#iname_div').html('<label>Instrument Name</label><select class="form-control filter" id="s_iname" name="s_iname" data-col="Instrument Name" notEqual="---" data-live-search="true" placeholder="Enter Instrument Name"></select>');
            load_all();
           load_instrument_type();
           OnChangeInstrumentType()
});


});
function load_all(){
    var operation='loadAllCalibration';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_details.php",
    data:{operation:operation}
   }).done(function(result){
    var dis='';
      for(i=0;i<result.length;i++){
         dis+='<tr>';
        dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+" id1="+result[i]['dc_id']+"></td>";
        // dis+="<td>"+result[i]['calibrator_type_name']+"</td>" 
        dis+="<td>"+result[i]['instrument_type_name']+"</td>";
        dis+="<td>"+result[i]['instrument_name']+"</td>"
        dis+="<td>"+result[i]['instrument_no']+"</td>"
        dis+="<td>"+result[i]['dc_id']+"</td>"
      //   var calibrationDate=result[i]['next_calibration_date'];
      //  var m=moment(calibrationDate,'YYYY-MM-DD');
      //  var newdate=moment(m).add(result[i]['calibration_frequency'],'days');
      //  var calibrationpending=moment(newdate).toDate();
      // var date = moment(calibrationpending).format('YYYY-MM-DD');
      //   dis+="<td>"+date+"</td>"
      dis+='</tr>';  
}

  $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });

//     var operation='loadAllInstrumentCalibration';
//     $.ajax({
//         type: "POST",
//       url: "../php/ajaxInterface/calibration_details.php",
//     data:{operation:operation}
//    }).done(function(result1){
//  console.log(result1);
//     for(j=0;j<result1.length;j++){
//          dis+='<tr>';
//         dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result1[j]['uid']+"></td>";
//         dis+="<td>"+result1[j]['instrument_type_name']+"</td>";
//         dis+="<td>"+result1[j]['instrument_name']+"</td>"
//         dis+="<td>"+result1[j]['instrument_no']+"</td>"
//         dis+="<td>"+result1[j]['purchesed_by']+"</td>" 
//         dis+="<td>"+result1[j]['purchesed_date']+"</td>"   
//         dis+="<td>"+result1[j]['first_calibration_date']+"</td>"
//       dis+='</tr>'; 
// }
    
     
      
// });
});
}

function load_calibrator_type(){
    var operation='loadAllcalibratorType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_details.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
        dis+='<option id>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['calibrator_type_name']+'</option>';
  }
      $('#calibrator_type').html(dis).selectpicker();
    
});
}

function load_instrument_type(){
    var operation='loadAllInstrumentType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_details.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
        dis+='<option id>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['instrument_type_name']+'</option>';
  }
      $('#s_itype').html(dis).selectpicker();
});
}

function OnChangeInstrumentType(){
    $('#s_itype').on('change',function(){
var instrument_id=$('#s_itype').children(':selected').attr('id');
  var instrumentName=$('#s_itype').val();
  if(instrumentName != '---'){
    $('#s_iname').val('');
     $('#iname_div').html('<label>Instrument Name</label><select class="form-control filter" id="s_iname" name="s_iname" data-col="Instrument Name" notEqual="--- "data-live-search="true" placeholder="Enter Instrument Name"></select>');
  var operation='loadSpecificInstrumentName';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_details.php",
    data:{operation:operation,id:instrument_id}
   }).done(function(result){
    
      var dis='';
        dis+='<option id>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['instrument_name']+'</option>';
  }
      $('#s_iname').html(dis).selectpicker();
});
 }
 else{

   $('#s_iname').val('');
     $('#iname_div').html('<label>Instrument Name</label><select class="form-control filter" id="s_iname" name="s_iname" data-col="Instrument Name" notEqual="--- "data-live-search="true" placeholder="Enter Instrument Name"></select>');

 }
});
}