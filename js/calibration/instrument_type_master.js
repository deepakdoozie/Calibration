var add_res='',edit_res='';
$(function(){
load_all();
load_instrument_type();
var login_by=$.cookie('i');
$('#add_btn').click(function(){
	$('#add_box').show();
	$('#table_box').hide();
	$('#search_box').hide();

});

$('#export_excel').click(function(e){
       
      var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "-" + month + "-" + year + "_" + hour + ":" + mins;
        var a = document.createElement('a');
        var data_type = 'data:application/calibration_report.ms-excel';
        var table_div = document.getElementById('show_tbl_row');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'calibration_report' + postfix + '.xls';
        a.click();
        e.preventDefault();
         
    })


$('#save_btn').click(function(){
	if($('#add_form').valid()){
		var iname=$('#add_iname').val();
		var description=$('#add_description').val();
     validateForAdd(iname);
     console.log(add_res,121212);
    if(Number(add_res)==1)
    {
       var operation='saveInstrumentType';
     $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/instrument_type_master.php",
         data:{operation:operation,iname:iname,description:description,login_by:login_by}
         }).done(function(result)
         {
          if(result){
             load_all();
             $('#add-task-success').fadeIn(200).delay(5000).fadeOut(1000);
            $('#add_form')[0].reset(); 
            $('#add_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_instrument_type();
         }
        
         });
    }else{
      $('#duplicate_entry_found').fadeIn(200).delay(5000).fadeOut(1000);
    }
		
	}

});

$('#close_btn').click(function(){
	       $('#add_form')[0].reset(); 
            $('#add_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_all();
           load_instrument_type();

	});

$('#view_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
		
    $('#add_box').hide();
    $('#view_box').show();
    $('#table_box').hide();
    $('#search_box').hide();

    var operation='viewSpecificInstrumentType';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/instrument_type_master.php",
    data:{operation:operation,id:id}
    }).done(function(result)
    { 
    	$('#view_iname').val(result['instrument_type_name']);
    	$('#view_description').val(result['instrument_description']);


    });
}
else{
$('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);

}
});

$('#view_close_btn').click(function(){
	
    $('#view_box').hide();
    $('#table_box').show();
    $('#search_box').show();
    load_all();
    load_instrument_type();

	});


$('#edit_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
    $('#edit_box').show();
    $('#table_box').hide();
    $('#search_box').hide();
     $('#edit_id').val(id);

    var operation='editSpecificInstrumentType';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/instrument_type_master.php",
    data:{operation:operation,id:id}
    }).done(function(result)
    { 
    	$('#edit_iname').val(result['instrument_type_name']);
    	$('#edit_description').val(result['instrument_description']);


    });
}
else{
$('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);

}
});


$('#update_btn').click(function(){
  // var x=confirm('Do you want to really update...?')
  // if(x){
	if($('#edit_form').valid()){
    // var id=$("input[type='radio'][name='ch']:checked").attr('id');
		var iname=$('#edit_iname').val();
		var description=$('#edit_description').val();
		var id= $('#edit_id').val();

   validateForUpdate(id,iname);
    console.log(edit_res,1111);
    if(Number(edit_res)==0){
    
		var operation='updateInstrumentType';
     $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/instrument_type_master.php",
         data:{operation:operation,id:id,iname:iname,description:description,login_by:login_by}
         }).done(function(result)
         {
         	if(result){
             load_all();
             load_instrument_type();
             $('#edit-update-success').fadeIn(200).delay(5000).fadeOut(1000);
            $('#edit_form')[0].reset(); 
            $('#edit_box').hide();
            $('#table_box').show();
           $('#search_box').show();
         }
        
         });

         }else{
       $('#update-task-fail').fadeIn(200).delay(5000).fadeOut(1000);
    }
	}

//}

});

$('#edit_clear_btn').click(function(){
	       $('#edit_form')[0].reset(); 
            $('#edit_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_all();
           load_instrument_type();

	});

$('#clear_btn').click(function(){
	      
           $('#s_iname').val('');
           $('#s_des').val('');
           load_all();
           load_instrument_type();
           $('#iname_div').html('<label>Instrument Type</label><select class="form-control filter" id="s_iname" name="s_iname" data-col="Instrument Name" notEqual="---" data-live-search="true"> <option>---</option></select>')

	});



});

function load_all(){
    var operation='getAllInstrumentType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/instrument_type_master.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
           $('#total').text(result.length);

      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";

        dis+="<td>"+result[i]['instrument_type_name']+"</td>";
        dis+="<td>"+result[i]['instrument_description']+"</td>"
        
      dis+='</tr>';
  }
      $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
      

});
}

function load_instrument_type(){
    var operation='loadAllInstrumentType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/instrument_type_master.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
        dis+='<option id>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option>'+result[i]['instrument_type_name']+'</option>';
  }
      $('#s_iname').html(dis).selectpicker();;
       
      

});
}


function validateForAdd(iname){
    var operation='checkForItemExists';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/instrument_type_master.php",
    async:false,
    data:{operation:operation,iname:iname}
   }).done(function(result){
      console.log(result,Number(result.length)); 
      if(Number(result.length)>0){
        add_res=0;
      }else{
        add_res=1;
      }

   });
}

function validateForUpdate(id,iname){
    var operation='checkForItemUpdate';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/instrument_type_master.php",
    async:false,
    data:{operation:operation,id:id,iname:iname}
   }).done(function(result){
      console.log(result); 
      if(Number(result.length)>0){
        edit_res=1;
      }else{
        edit_res=0;
      }

   });
}
  