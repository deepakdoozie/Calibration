var options = {
  useEasing : true, 
  useGrouping : true, 
  separator : ',', 
  decimal : '.', 
  prefix : '' ,
  suffix : '' ,
};
$(function(){
load_instrument_types();
load_instrument();
load_pending_report();
load_due_report();
load_discarded_instrument();
load_issue_instrument();


});

function load_instrument_types(){
	var operation='countAllInstrumentType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/dashboard.php",
    data:{operation:operation}
   }).done(function(result){
$('#total_instrument').text(result['total']);
   });

}

function load_instrument(){
	var operation='countAllInstrument';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/dashboard.php",
    data:{operation:operation}
   }).done(function(result){
$('#no_of_instrument').text(result['total']);

   });

}

function load_pending_report(){
	var operation='countAllPendingReport';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/dashboard.php",
    data:{operation:operation}
   }).done(function(result){
   	var count=0;

var date=new Date();
      var today=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate());
      var dis='';

      for(i=0;i<result.length;i++){
      	var calibrationDate=result[i]['next_calibration_date'];
   var m=moment(calibrationDate,'YYYY-MM-DD');
   var dif=result[i]['calibration_frequency'];
  var calibrationPeriods=dif;
  var newdate=moment(m).add(calibrationPeriods,'days');
  
  var newdate1=moment(newdate).subtract(result[i]['calibration_alert_days'],'days');


  var endDate = new Date(newdate)
  , startDate   = new Date(newdate1)
  , date  = new Date();
  
  if((startDate <= date) && (date <= endDate)) {
         count++;
  }
}

var operation='countAllInstrumentPendingReport';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/dashboard.php",
    data:{operation:operation}
   }).done(function(result1){

   	 for(j=0;j<result1.length;j++){
    var calibrationDate1=result1[j]['first_calibration_date'];
   var m1=moment(calibrationDate1,'YYYY-MM-DD');
  var newdate2=moment(m1).subtract(result1[j]['calibration_alert_days'],'days');


  var endDate1 = new Date(result1[j]['first_calibration_date'])
  , startDate1   = new Date(newdate2)
  , date1  = new Date();
  
  if((startDate1 <= date1) && (date1 <= endDate1)) {
        count++
  }
}
$('#pending').text(count);
   });

});

}

function load_due_report(){

	 var operation='loadAllDueCalibration';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/dashboard.php",
    data:{operation:operation}
   }).done(function(result){
   	 var date=new Date();
      var today=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate());
      var count1=0;

      for(i=0;i<result.length;i++){
    var calibrationDate=result[i]['next_calibration_date'];
   var m=moment(calibrationDate,'YYYY-MM-DD');
   var dif=result[i]['calibration_frequency'];
  var calibrationPeriods=dif;
  var newdate=moment(m).add(calibrationPeriods,'days');
  var calibrationpending=moment(newdate).toDate();
  var date = moment(newdate).format('YYYY-MM-DD');
  var diffDate=new Date(today)-new Date(date);
  if(diffDate>0){
        count1++;

  }
}
         
         var operation='loadAllDueInstrumentCalibration';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/dashboard.php",
    data:{operation:operation}
   }).done(function(result1){
     for(j=0;j<result1.length;j++){
  
  var diffDate1=new Date(today)-new Date(result1[j]['first_calibration_date']);
  if(diffDate1>0){
       count1++;
  }
}
     $('#due').text(count1);    
});
});
} 

function load_discarded_instrument(){

	   var operation='countDiscardedInstrument';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/dashboard.php",
    data:{operation:operation}
   }).done(function(result){
   	$('#descarded').text(result['total']);

   });

	}

  function load_issue_instrument(){

     var operation='countIssuedInstrument';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/dashboard.php",
    data:{operation:operation}
   }).done(function(result){
    $('#issued').text(result['total']);

   });

  }

