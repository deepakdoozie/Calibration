$(function(){
	load_all();

	$('#export_excel').click(function(e){
       
      var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "-" + month + "-" + year + "_" + hour + ":" + mins;
        var a = document.createElement('a');
        var data_type = 'data:application/discarded_instrument_report.ms-excel';
        var table_div = document.getElementById('show_tbl_row');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'discarded_instrument_' + postfix + '.xls';
        a.click();
        e.preventDefault();
         
    })

    $('#clear_btn').click(function(){
    	$('#s_itype').val('');
    	$('#s_iname').val('');
    	$('#s_ino').val('');
    	load_all();

    });

});
function load_all(){
    var operation='loadDiscardedInstrument';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/discarded_instrument_report.php",
    data:{operation:operation}
   }).done(function(result){
   	
   	 var date=new Date();
      var today=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate());
      var dis='';

      for(i=0;i<result.length;i++){
         dis+='<tr>';
        dis+="<td>"+result[i]['instrument_type_name']+"</td>";
        dis+="<td>"+result[i]['instrument_name']+"</td>"
        dis+="<td>"+result[i]['instrument_no']+"</td>"
        dis+="<td>"+result[i]['purchesed_by']+"</td>" 
        dis+="<td>"+get_print_format(result[i]['purchesed_date'])+"</td>"
        
      dis+="<td>"+get_print_format(result[i]['discarded_date'])+"</td>"
        dis+="<td>"+result[i]['reason']+"</td>"
      dis+='</tr>';
  
}
      $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
       
      

});
}