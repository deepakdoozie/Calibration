$(function(){
    load_All();
    $('#add-transport-master').click(function(){
      $('#view-box').hide();
      $('#filter-box').hide();
      $('#add-box').show();
    });

    $('#edit-transport-master').click(function(){
      
      var t_id=$("input[type='radio'][name='ch']:checked").attr('id');
      if(t_id>0){
      var operation='getSpecificTransportMaster';
      $.ajax({
        type: "POST",
        url: "../php/ajaxInterface/transport_master.php",
        data:{operation:operation,t_id:t_id}
        }).done(function(result)
        { 
          console.log(result);
          $('#t_code').val(pad_with_zeroes(result['t_id'],2));
          $('#t_type').val(result['t_type']);
          $('#d_type').val(result['d_type']);
          $('#desc').val(result['desc']);
          $('#t_id').val(result['t_id']);
          $('#view-box').hide();
          $('#filter-box').hide();
          $('#edit-box').show();
        }) 
      }else{
        $('#select').fadeIn(200).delay(5000).fadeOut(1000);
      }
    })

    $('#save-btn').click(function(){
      if($('#add_form').valid()){
        var disabled = $('#add_form').find(':input:disabled').removeAttr('disabled');
        var datastring = $("#add_form").serialize();
        disabled.attr('disabled','disabled');
        console.log(datastring);
      $.ajax({
        type: "POST",
        url: "../php/ajaxInterface/transport_master.php",
        data:datastring
        }).done(function(result)
        {
          console.log(result);
          if(result=='1'){
            $('#success').fadeIn(200).delay(5000).fadeOut(1000);
            cancel_All();
            load_All();
          }else{
            $('#fail').fadeIn(200).delay(5000).fadeOut(1000);
          }
       });
      }
    })

    $('#update-btn').click(function(){
      if($('#edit_form').valid()){
         
        var disabled = $('#edit_form').find(':input:disabled').removeAttr('disabled');
        var datastring = $("#edit_form").serialize();
        disabled.attr('disabled','disabled');
      $.ajax({
        type: "POST",
        url: "../php/ajaxInterface/transport_master.php",
        data:datastring
        }).done(function(result)
        {
          $('#update').fadeIn(200).delay(5000).fadeOut(1000);
          cancel_All();
          load_All();
        });
      }
    })

    $('#clear_btn').click(function(){
      $('#s_dtype').val('');
       $('#s_type').val('');
       load_All();
    })

}); 

function clear_All(){
  $("#filter_form")[0].reset();
  load_All();
}

function load_All(){
  console.log(2);
  var operation='getAllTransportMaster';
  $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/transport_master.php",
    data:{operation:operation}
    }).done(function(result)
    { 
      console.log(result);
      var dis='';
      for(i=0;i<result.length;i++){
        dis+='<tr>';
        dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['t_id']+"></td>";
        dis+="<td>"+pad_with_zeroes(result[i]['t_id'],2)+"</td>";
        dis+="<td>"+result[i]['t_type']+"</td>";
        dis+="<td>"+result[i]['d_type']+"</td>";
        dis+="<td>"+result[i]['desc']+"</td>";
        dis+='</tr>';
      }
      $('#row').html(dis);
      $('.filter').multifilter({
      'target' : $('#view')
    });
    });
}

function cancel_All(){
  $('#view-box').show();
  $('#filter-box').show();
  $('#add-box').hide();
  $('#edit-box').hide();
  $('#add_form')[0].reset();
  $('#edit_form')[0].reset();
}

function pad_with_zeroes(number, length) {

    var my_string = '' + number;
    while (my_string.length < length) {
        my_string = '0' + my_string;
    }

    return my_string;

}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function getFinancialYear(date){
  var d = new Date(date);
  var full_year=d.getFullYear();
  var month=d.getMonth();
  var s=full_year.toString();
  var year=s.slice('2');
  if(month == 0 || month == 1 || month == 2){
        
        return year-1;
  }else{
    return year;
  }
}
