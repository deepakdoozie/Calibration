$(function(){
  var login_by=$.cookie('i');
	load_all();
  load_name();
 $('#export_excel').click(function(e){
       
      var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "-" + month + "-" + year + "_" + hour + ":" + mins;
        var a = document.createElement('a');
        var data_type = 'data:application/employee.ms-excel';
        var table_div = document.getElementById('myTable');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'Emp_' + postfix + '.xls';
        a.click();
        e.preventDefault();
         
    })
  

$('#add_save_btn').click(function(){

  if($('#add_form').valid()){
    var employmentTypeId=$('#add_type').children(':selected').attr('id');
    if(Number(employmentTypeId)>0)
    {

    

    
    var modeOfPaymentTypeId=$('#add_mode_of_payment').children(':selected').attr('id');
   

	var disabled = $('#add_form').find(':input:disabled').removeAttr('disabled');
    var datastring=[];
    datastring+='operation'+'='+encodeURIComponent('addEmployee')+'&';
    datastring+=$("#add_form").serialize();
    datastring+='&'+'emply_typ_id'+'='+encodeURIComponent(employmentTypeId)+'&';
    datastring+='payment_id'+'='+encodeURIComponent(modeOfPaymentTypeId)+'&';
    datastring+='login_by'+'='+encodeURIComponent(login_by)+'&';
       
    disabled.attr('disabled','disabled');
    
    
    $.ajax({

       type: "POST",
        url: "../php/ajaxInterface/employee_master.php",
        data:datastring
         }).done(function(result)
         {
         	 
           if(result>0){
             load_all();
             load_name();
             $('#add-task-success').fadeIn(200).delay(5000).fadeOut(1000);
            $('#add_form')[0].reset(); 
            $('#add_box').hide();
            $('#table_box').show();
           $('#search_box').show();
         }
         else{
        
    $('#eid-already-exist').fadeIn(200).delay(5000).fadeOut(1000);
 
         }


        });

}else{
  $('#select_employement_type_error').fadeIn(200).delay(5000).fadeOut(1000);
}
}

});

$('#add_emply').click(function(){
	load_all();
	$('#add_box').show();
  $('#table_box').hide();
    $('#search_box').hide();

    var operation='loadEmploymentType';
     $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/employee_master.php",
         data:{operation:operation}
         }).done(function(result)
         {
          
          var dis='';
        dis+='<option id>---</option>';
          for(i=0;i<result.length;i++){
         dis+='<option id='+result[i]['employment_type_uid']+'>'+result[i]['employment_type_name']+'</option>';
         }
          $('#add_type').html(dis).selectpicker();

         });

         var operation='loadModeOfPaymentType';
     $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/employee_master.php",
         data:{operation:operation}
         }).done(function(result)
         {
       
          var dis='';
        dis+='<option id>---</option>';
          for(i=0;i<result.length;i++){
         dis+='<option id='+result[i]['mode_of_payment_uid']+'>'+result[i]['mode_of_payment_name']+'</option>';
         }
          $('#add_mode_of_payment').html(dis);

         });

});

$('#add_clear_btn').click(function(){

  $('#add_form')[0].reset();

   $('#add_box').hide();
    $('#edit_box').hide();
    $('#table_box').show();
    $('#search_box').show();
     $('#view_box').hide();

    load_all();
  });


$('#edit_emply').click(function(){

  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
  var id1=$("input[type='radio'][name='ch']:checked").attr('id1');
    $('#edit_id').val(id);
    $('#edit_id1').val(id1);
	  $('#add_box').hide();
    
    $('#table_box').hide();
    $('#search_box').hide();
    var operation='editEmply';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/employee_master.php",
    data:{operation:operation,id:id}
    }).done(function(result)
    { 
      $('#edit_box').show();
      $('#edit_eid').val(result[0]['e_id']); 
     $('#edit_fname').val(result[0]['first_name']);
        $('#edit_mname').val(result[0]['middle_name']);
         $('#edit_lname').val(result[0]['last_name']);
      $('#edit_dob').val(result[0]['dob']);
      $('#edit_doj').val(result[0]['doj']);
      $('#edit_ecno').val(result[0]['con_no']);
      $('#edit_eecno').val(result[0]['econ_no']);
      $('#edit_eeid').val(result[0]['email']);
      $('#edit_ebld').val(result[0]['blood']);
      $('#edit_eadrs').val(result[0]['con_adrs']);
      $('#edit_epadrs').val(result[0]['pem_adrs']);

     
      $('#edit_desination').val(result[0]['designation']);
      $('#edit_department').val(result[0]['department']);
      $('#edit_esi').val(result[0]['esi_no']);
      $('#edit_pf').val(result[0]['pf_no']);
      $('#edit_bank_name').val(result[0]['bank_name']);
      $('#edit_acc_no').val(result[0]['account_no']);
      $('#edit_ifsc_code').val(result[0]['ifsc_code']);
      $('#edit_pan_no').val(result[0]['pan_no']);
      $('#edit_type').val(result[0]['employment_type_name']);
      $('#edit_mode_of_payment').val(result[0]['mode_of_payment_name']);
      
      var dis='';
     dis='<option id='+result[0]['employment_type_uid']+'>'+result[0]['employment_type_name']+'</option>';
    var operation='loadEmploymentType';
     $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/employee_master.php",
         data:{operation:operation}
         }).done(function(result1)
         {
          for(i=0;i<result1.length;i++){
         dis+='<option id='+result1[i]['employment_type_uid']+'>'+result1[i]['employment_type_name']+'</option>';
         }
          $('#edit_type').html(dis).selectpicker();

         });


         $('#edit_mode_of_payment').val(result[0]['mode_of_payment_name']);
      
      var dis2='';
     dis2='<option id='+result[0]['mode_of_payment_uid']+'>'+result[0]['mode_of_payment_name']+'</option>';
    var operation='loadModeOfPaymentType';
     $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/employee_master.php",
         data:{operation:operation}
         }).done(function(result2)
         {
       
          for(i=0;i<result2.length;i++){
         dis2+='<option id='+result2[i]['mode_of_payment_uid']+'>'+result2[i]['mode_of_payment_name']+'</option>';
         }
          $('#edit_mode_of_payment').html(dis2);

         });

    });

  }
  else{
    $('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);
  }


});

$('#edit_update_btn').click(function(){
  var employmentTypeId=$('#edit_type').children(':selected').attr('id');
  if($('#edit_form').valid()){
    if(Number(employmentTypeId)>0)
    {
  x=confirm('Do you want to really update...?')
    if(x){
    var updateId=$('#edit_id').val();
    // var employmentTypeId=$('#edit_type').children(':selected').attr('id');
    var modeOfPaymentTypeId=$('#edit_mode_of_payment').children(':selected').attr('id');
    var disabled = $('#edit_form').find(':input:disabled').removeAttr('disabled');
    var datastring=[];
    datastring+='operation'+'='+encodeURIComponent('updateEmply')+'&';
    datastring+=$("#edit_form").serialize();
    datastring+='&'+'emply_typ_id'+'='+encodeURIComponent(employmentTypeId)+'&';
    datastring+='payment_id'+'='+encodeURIComponent(modeOfPaymentTypeId);
    datastring+='&'+'update_id'+'='+encodeURIComponent(updateId);
    datastring+='&'+'login_by'+'='+encodeURIComponent(login_by)+'&';

    disabled.attr('disabled','disabled');
  
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/employee_master.php",
        data:datastring
    }).done(function(result)
    { 
      console.log(result);
      if(result != 0){
    $('#edit-update-success').fadeIn(200).delay(5000).fadeOut(1000);
      load_all();
      $('#edit_box').hide();
      $('#table_box').show();
      $('#search_box').show();
    }else{
    $('#edit-eid-already-exist').fadeIn(200).delay(5000).fadeOut(1000);
    }

        });
  }
}else{
  $('#select_employement_type_error_edit').fadeIn(200).delay(5000).fadeOut(1000);
}
}
});

$('#edit_clear_btn').click(function(){

    $('#edit_form')[0].reset();

   //$('#add_box').hide();
    $('#edit_box').hide();
    $('#table_box').show();
    $('#search_box').show();
     //$('#view_box').hide();

    load_all();
    });




$('#view_emply').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
		
    $('#add_box').hide();
    $('#edit_box').hide();
    $('#table_box').hide();
    $('#search_box').hide();

    var operation='viewEmply';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/employee_master.php",
    data:{operation:operation,id:id}
    }).done(function(result)

    { 
      
      $('#view_box').show();
      $('#view_eid').val(result[0]['e_id']); 
       $('#view_fname').val(result[0]['first_name']);
        $('#view_mname').val(result[0]['middle_name']);
         $('#view_lname').val(result[0]['last_name']);
      $('#view_dob').val(result[0]['dob']);
      $('#view_doj').val(result[0]['doj']);
      $('#view_ecno').val(result[0]['con_no']);
      $('#view_eecno').val(result[0]['econ_no']);
      $('#view_eeid').val(result[0]['email']);
      $('#view_ebld').val(result[0]['blood']);
      $('#view_eadrs').val(result[0]['con_adrs']);
      $('#view_epadrs').val(result[0]['pem_adrs']);

      $('#view_type').val(result[0]['employment_type_name']);
      $('#view_desination').val(result[0]['designation']);
      $('#view_department').val(result[0]['department']);
      $('#view_esi').val(result[0]['esi_no']);
      $('#view_pf').val(result[0]['pf_no']);
      $('#view_bank_name').val(result[0]['bank_name']);
      $('#view_acc_no').val(result[0]['account_no']);
      $('#view_ifsc_code').val(result[0]['ifsc_code']);
      $('#view_pan_no').val(result[0]['pan_no']);
      $('#view_mode_of_payment').val(result[0]['mode_of_payment_name']);
     
              });

  }
  else{
    $('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);
  }

});

$('#view_close_btn').click(function(){

    $('#add_box').hide();
    $('#edit_box').hide();
    $('#table_box').show();
    $('#search_box').show();
     $('#view_box').hide();
     load_name();
    load_all();
    $('#view_form')[0].reset();


});

$('#active_btn').click(function(){
   x=confirm('Do you want to really Active/De-active...?')
    if(x){
    var id=$('#save_active_id').val();
    var operation='deleteEmply';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/employee_master.php",
    data:{operation:operation,id:id}
    }).done(function(result)
    { 
      $('#table_box').show();
      $('#search_box').show();
      $('#active_box').hide();
      load_all();
      load_name();
      
    });
   }
 });

$('#active_cancel_btn').click(function(){
   $('#table_box').show();
      $('#search_box').show();
      $('#active_box').hide();
      load_all();
      load_name();

});

$('#del_emply').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
    $('#save_active_id').val(id);
    $('#active_box').show();
     $('#table_box').hide();
     $('#search_box').hide();
      var operation='viewEmply';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/employee_master.php",
    data:{operation:operation,id:id}
    }).done(function(result)

    { 
      
      $('#active_box').show();
      $('#active_eid').val(result[0]['e_id']); 
      $('#active_fname').val(result[0]['first_name']);
        $('#active_mname').val(result[0]['middle_name']);
         $('#active_lname').val(result[0]['last_name']);
      $('#active_dob').val(result[0]['dob']);
      $('#active_doj').val(result[0]['doj']);
      $('#active_ecno').val(result[0]['con_no']);
      $('#active_eecno').val(result[0]['econ_no']);
      $('#active_eeid').val(result[0]['email']);
      $('#active_ebld').val(result[0]['blood']);
      $('#active_eadrs').val(result[0]['con_adrs']);
      $('#active_epadrs').val(result[0]['pem_adrs']);
              });
     


   }
   else{
    $('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);
   }


});

$('#search-btn').click(function(){

  var ename=$('#s_ename').children(':selected').attr('id');
  var date=$('#s_doj').val();

  if(ename=="" && date=="")
  {
     $('#search_box_select_error').fadeIn(200).delay(5000).fadeOut(1000);
  }else{


  var operation='searchEmplyName';
  $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/employee_master.php",
    data:{operation:operation,ename:ename,date:date}
    }).done(function(result)
    { var dis='';

      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";

        dis+="<td>"+result[i]['e_id']+"</td>";
        dis+="<td>"+result[i]['first_name']+' '+result[i]['middle_name']+' '+result[i]['last_name']+"</td>"

        dis+="<td>"+get_print_format(result[i]['dob'])+"</td>"
        dis+="<td>"+get_print_format(result[i]['doj'])+"</td>"
        dis+="<td>"+result[i]['blood']+"</td>"
        dis+="<td>"+result[i]['con_no']+"</td>"
        if(result[i]['active_flag']==1){
          dis+="<td>"+'Active'+"</td>"
         }
         else{
          dis+="<td>"+'De-active'+"</td>"
         }
      dis+='</tr>';
  }
      $('#view').html(dis);


    });
 }
});
$('#clear_btn').click(function(){
  $('#s_doj').val('');
  $('#s_ename').val('');
  $('#ename_div').html('<label>Employment Name</label><select class="form-control filter" id="s_ename" name="s_ename" data-live-search="true" notEqual="---"><option>---</option></select>')
   load_name();
   load_all();
});
})

function load_all(){
    var operation='getAllEmployee';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/employee_master.php",
    data:{operation:operation}
   }).done(function(result){
   

      var dis='';

      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+" id1="+result[i]['e_id']+"></td>";

        dis+="<td>"+result[i]['e_id']+"</td>";
        dis+="<td>"+result[i]['first_name']+' '+result[i]['middle_name']+' '+result[i]['last_name']+"</td>"
        dis+="<td>"+get_print_format(result[i]['dob'])+"</td>"
        dis+="<td>"+get_print_format(result[i]['doj'])+"</td>"
        dis+="<td>"+result[i]['blood']+"</td>"
        dis+="<td>"+result[i]['con_no']+"</td>"
         if(result[i]['active_flag']==1){
          dis+="<td>"+'Active'+"</td>"
         }
         else{
          dis+="<td style='color:red'>"+'De-active'+"</td>"
         }
      dis+='</tr>';
  }
      $('#view').html(dis);
      

});
}

function load_name(){
  var operation='loadname';
  $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/employee_master.php",
    data:{operation:operation}
    }).done(function(result)
    { 
       var dis='';
        dis+='<option id>---</option>';
          for(i=0;i<result.length;i++){
         dis+='<option id='+result[i]['uid']+'>'+result[i]['first_name']+' '+result[i]['middle_name']+' '+result[i]['last_name']+'</option>';
         }
          $('#s_ename').html(dis).selectpicker();



    });
}
