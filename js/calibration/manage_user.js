 var filename=[];
$(function(){
  var updated_by=$.cookie('i');
  load_designation();
  load_all();

  	

  $('#update-btn').click(function(){
  	if($('#manage-form').valid()){
      var x=confirm('Do you want to really update...?')
      if(x){
    var flag=$('#upload_flag').val();
        if(Number(flag)==1){
           console.log(flag);
          var image_path=$('#document').val();
        }
        else{
          console.log(flag);
           var image_path=$('#user_image').val();
        }

    var id=$("input[type='radio'][name='ch']:checked").attr('id');
  	var designation=$('#designation_edit').children(':selected').attr('id');
   if(designation != '-1'){
   var loginName=$('#login_name_edit').val();
   var userName= $('#user_name_edit').val();
  var length=$("input[type='checkbox'][name='role_check']:checked").length;
 if(Number(length)>0){
    var operation='updateUser';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/manage_user.php",
    data:{operation:operation,loginName:loginName,designation:designation,userName:userName,updated_by:updated_by,id:id,image_path:image_path},
    async:false
   }).done(function(result){
  
   });
    $("#view_all_role > tr").each(function(){
    var x=$(this).find('input').serializeArray();
    if(x.length>0)
    if(x[0]['value']=='on'){
       var operation='saveUserRole';
         $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/add_user.php",
    data:{operation:operation,id:id,role_id:x[1]['value']},
    async:false
   }).done(function(result){
    console.log(result)
   });

    }
  });

      $('#edit-update-success').fadeIn(200).delay(5000).fadeOut(1000);
    update_clr();
 
    }else{
 $('#select-role').fadeIn(200).delay(5000).fadeOut(1000);
 }
   }
   else{
$('#select-designation').fadeIn(200).delay(5000).fadeOut(1000);
   }
}
  	}
  });



  $('#manage-btn').click(function(){
 	var id=$("input[type='radio'][name='ch']:checked").attr('id');
 	console.log(id);
  if(id>0){
    $('#table-box').hide();
    $('#search-box').hide();
    $('#manage-box').show();

      	 var operation='manageUser';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/manage_user.php",
    data:{operation:operation,id:id},
    async:false
   }).done(function(result){
    
       $('#login_name_edit').val(result[0]['name']);
      $('#user_name_edit').val(result[0]['user_name']);
      $('#user_image').val(result[0]['image_path']);
       var dis='<option id="'+result[0]['designation_id']+'">'+result[0]['designation']+'</option>';
       var operation='getAllDesignation';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/add_user.php",
    data:{operation:operation},
    async:false
   }).done(function(result){
      for (var i = 0; i < result.length; i++) {
        dis+='<option id='+result[i]['uid']+'>'+result[i]['designation']+'</option>'
      };
      $('#designation_edit').html(dis);

    });

      var operation='getAllRoles';
      $.ajax({
          type: "POST",
      url: "../php/ajaxInterface/add_user.php",
      data:{operation:operation},
      async:false
     }).done(function(result1){
     
           var dis='';
        for(i=0;i<result1.length;i++){
           dis+='<tr>';
           dis+="<td  style='width:10px;'><input type='checkbox' class='flat-red' name='role_check' id='i"+result1[i]['uid']+"'><input type='hidden' name='uid' value="+result1[i]['uid']+"></td>";
          dis+="<td>"+result1[i]['role']+"</td>"
           dis+='</tr>';
  }
        $('#view_all_role').html(dis);
            for(l=0;l<result.length;l++){
     for(m=0;m<result1.length;m++){
      if(Number(result[l]['role_id'])==Number(result1[m]['uid'])){
        $('#i'+result1[m]['uid']).prop('checked',true);
     
    }
     }
   } 

        

  });


   });


 	}
 	else{
      $('#select-entry').fadeIn(200).delay(5000).fadeOut(1000);
  }
 });
   $('#clear_btn').click(function(){
   	$('#designation_search').val('');
   	$('#s_lname').val('');
   	load_all();
   	load_designation();

   });

     $('#update-cancel').click(function(){
   $('#table-box').show();
    $('#search-box').show();
    $('#manage-box').hide();
    $('#manage-form')[0].reset();
     $('#designation_edit').html('');
  	 load_all();

   });
          $('#image-upload-btn').click(function(){
     $('#image-upload-btn').hide();
        $('#upload-img-shw').show();
       });

     var uploader = new plupload.Uploader({
                runtimes : 'html5,flash,silverlight', // Set runtimes, here it will use HTML5, if not supported will use flash, etc.
                browse_button : 'pickfiles', // The id on the select files button
                multi_selection: false, // Allow to select one file each time
                container : 'uploader', // The id of the upload form container
                max_file_size : '10Mb', // Maximum file size allowed
                url : '../php/user_image_upload.php', // The url to the upload.php file
                flash_swf_url : 'js/plupload.flash.swf', // The url to thye flash file
                silverlight_xap_url : 'js/plupload.silverlight.xap', // The url to the silverlight file
                filters : [
                                        {title : "All files", extensions : "jpg,gif,tiff,png,psd,php,htm,html,css,zip,rar,tar,gzip,txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv,mp3,ogg,wav,mov,mp4,f4v,flv"},
                                        {title : "Image files", extensions : "jpg,gif,tiff,png,psd"},
                                        {title : "Web files", extensions : "php,htm,html,css"},
                                        {title : "Archive files", extensions : "zip,rar,tar,gzip"},
                                        {title : "Document files", extensions : "txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv"},
                                        
                                                 ] // Filter the files that will be showed on the select files window
            });

            // RUNTIME
            uploader.bind('Init', function(up, params) {
                $('#runtime').text(params.runtime);
            });

            // Start Upload ////////////////////////////////////////////
            // When the button with the id "#uploadfiles" is clicked the upload will start
            $('#uploadfiles').click(function(e) {
                uploader.start();
                e.preventDefault();
                $('#upload_flag').val(1);
                
            });

            uploader.init(); // Initializes the Uploader instance and adds internal event listeners.

            // Selected Files //////////////////////////////////////////
            // When the user select a file it wiil append one div with the class "addedFile" and a unique id to the "#filelist" div.
            // This appended div will contain the file name and a remove button
            uploader.bind('FilesAdded', function(up, files) {
                $.each(files, function(i, file) {
                    $('#filelist').append('<div class="addedFile" id="' + file.id + '">' + file.name + '<a href="#" id="' + file.id + '" class="removeFile"></a>' + '</div>');
                file=file.name;
                $('#document').val(file);
               
                });
                up.refresh(); // Reposition Flash/Silverlight
            });

            // Error Alert /////////////////////////////////////////////
            // If an error occurs an alert window will popup with the error code and error message.
            // Ex: when a user adds a file with now allowed extension
            uploader.bind('Error', function(up, err) {
                alert("Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "");
                up.refresh(); // Reposition Flash/Silverlight
            });

            

            // Progress bar ////////////////////////////////////////////
            // Add the progress bar when the upload starts
            // Append the tooltip with the current percentage
            uploader.bind('UploadProgress', function(up, file) {
              
                var progressBarValue = up.total.percent;
                
                $('#progressbar').fadeIn().progressbar({
                    value: progressBarValue
                 

                });
                $('#progressbar .ui-progressbar-value').html('<span class="progressTooltip">' + up.total.percent + '%</span>');
            });

             uploader.bind('FileUploaded', function(up, file, info) {
              var obj = JSON.parse(info.response);
             
              filename.push(file.name);
              console.log(filename);
             
            });

$('#change_password_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
  console.log(id);
  if(id>0){
    $('#table-box').hide();
    $('#search-box').hide();
    $('#password-box').show();
  }
  else{
$('#select-entry').fadeIn(200).delay(5000).fadeOut(1000);
  }
 });


  $('#update-password-btn').click(function(){
    if($('#manage-form').valid()){
      var x=confirm('Do you want to really update...?')
      if(x){
var id=$("input[type='radio'][name='ch']:checked").attr('id');
   var password=hex_md5($('#password_edit').val());
    var rePassword=hex_md5($('#re_password_edit').val());
   if(password == rePassword){
    var operation='updateUserPassword';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/manage_user.php",
    data:{operation:operation,password:password,updated_by:updated_by,id:id}
   }).done(function(result){
    $('#edit-password-success').fadeIn(200).delay(5000).fadeOut(1000);
    update_password_clr();

   });

}
else{
$('#select_same_password').fadeIn(200).delay(5000).fadeOut(1000);
}

}
    }
  });



});

 function update_password_clr(){
   $('#table-box').show();
    $('#search-box').show();
    $('#password-box').hide();
    $('#password-form')[0].reset();
 }

function load_designation(){
	 var operation='getAllDesignation';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/add_user.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='<option id="-1">---</option>'
      for (var i = 0; i < result.length; i++) {
        dis+='<option id='+result[i]['uid']+'>'+result[i]['designation']+'</option>'
      };
      $('#designation_search').html(dis);
    });
}

function load_all(){

	  var operation='loadAllUser';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/add_user.php",
    data:{operation:operation}
   }).done(function(result){
   	var dis='';
   	 for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";
         dis+="<td>"+result[i]['user_name']+"</td>"
        dis+="<td>"+result[i]['name']+"</td>"
        dis+="<td>"+result[i]['designation']+"</td>"
        if(result[i]['active_flag']==1){
       dis+="<td style='color:green'>"+'Active'+"</td>"
        }
        else{
    dis+="<td style='color:red'>"+'DeActive'+"</td>"
        }
      dis+='</tr>';
  }
      $('#view').html(dis);
       $('.filter').multifilter({
        'target' : $('#myTable1')
        });

   });
}
 function update_clr(){
  $('#table-box').show();
    $('#search-box').show();
    $('#manage-box').hide();
    $('#manage-form')[0].reset();
  $('#search_form')[0].reset();
     $('#designation_edit').html('');
     load_all();
      $('#upload_flag').val(0);
   $('#document').val(0);

   $('#progressbar').html('');
    $('#pbar').html('');
     $('#filename').html('');
     $('#filelist').html('');
  $('#upload-img-shw').hide();
  $('#image-upload-btn').show();
  filename=[];
 }