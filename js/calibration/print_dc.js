$(function(){
   load_all();
   var dis='';
  for(var i=0;i<100;i++){
    dis+="<td colspan=1% style='padding:0;margin:0;' >";
    dis+="<div style='text-align:center'><span style='font-size:9.0pt'><span></span></div>";
    dis+="</td>";
  }
  $('#setter').html(dis);

  $('#print_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
   	 $('#print-box').show();
    $('#table_box').hide();
    $('#search_box').hide();
    $('#print').show();
      var operation='loadALLDCItems';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/print_dc.php",
    data:{operation:operation,id:id},
     async:false
    }).done(function(result)
    { 
    console.log(result);
    $('#note').html(result[0]['note']);
    $('#ur_dc_no').html(result[0]['uid']);
    $('#ur_dc_date').html(get_print_format(result[0]['created_at']));
    $('#transport').html(result[0]['t_type']);
   var inc=1;
   var dis='';
    for (var i = 0; i < result.length; i++) {
    	console.log(i);
    	var operation='loadALLDCInstrument';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/print_dc.php",
    data:{operation:operation,instrument_type_details_id:result[i]['instrument_type_details_id'],dc_id:result[i]['uid']},
     async:false
    }).done(function(result)
    { 
    console.log(result);
         $('#calibrator_address').html(result['calibrator_address']);
         $('#calibrator_name').html(result['calibrator_name']);
    	    dis+='<tr style="text-align: center;"  class="b_row">';
          dis+='<td colspan="10%" style="padding-top: 2px; padding-bottom:1px;">'+inc+'</td>';
          dis+='<td colspan="50%" style="padding-top: 2px; padding-bottom:1px;">'+result['instrument_type_name']+'-'+result['instrument_name']+'-'+result['instrument_no']+'</td>';
          dis+='<td colspan="10%" style="padding-top: 2px; padding-bottom:1px;">'+'1'+'</td>';
          dis+='<td colspan="10%" style="padding-top: 2px; padding-bottom:1px;">'+result['uom']+'</td>';
          dis+='<td colspan="10%" style="padding-top: 2px; padding-bottom:1px; text-align:centre;">'+get_print_format(result['expected_date'])+'</td>';
          dis+='<td colspan="10%" style="padding-top: 2px; padding-bottom:1px;"></td>'
          dis+='</tr>';
   

});
    	inc++;
    };
    $('#view-item-row').html(dis);

});

   }
   else{
$('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);

   }
});

$('#print').on('click', function() {
    $('#print').hide();
     $('#parameter').text('-For Calibrator');
     $('#p_bill').printThis();
    $(this).hide();
  });

$('#print_bill').on('click', function() {
    $('#print_bill').hide();
     $('#parameter').text('-Return along with bill');
     $('#p_bill').printThis();
    $(this).hide();
  });

$('#print_internal').on('click', function() {
    $('#print_internal').hide();
     $('#parameter').text('-For Internal Reference');
     $('#p_bill').printThis();
    $(this).hide();
  });

$('#cancel-print').click(function(){

	$('#print-box').hide();
    $('#table_box').show();
    $('#search_box').show();	
	load_all();
	 $('#view-item-row').html('');
    $('#print_internal').show();
      $('#print').show();
        $('#print_bill').show();

});
$('#clear_btn').click(function(){

  $('#s_date').val('');
  $('#s_dc').val('');
  load_all();
 });

});

function load_all(){

    var operation='loadALLDC';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/print_dc.php",
    data:{operation:operation},
     async:false
    }).done(function(result)
    { 
    console.log(result);
    var dis='';
    for (var i = 0; i < result.length; i++) {
        dis+='<tr>';
        dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";
        dis+="<td>"+result[i]['uid']+"</td>";
        dis+="<td>"+get_print_format(result[i]['created_at'])+"</td>"
        // dis+="<td>"+result[i]['expected_date']+"</td>"
        dis+='</tr>';
    }
    $('#view').html(dis);
     $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });

  });


}