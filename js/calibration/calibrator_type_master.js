$(function(){
  var login_by=$.cookie('i');
load_all();
load_calibrator_type();

  $('#export_excel').click(function(e){
       
      var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "-" + month + "-" + year + "_" + hour + ":" + mins;
        var a = document.createElement('a');
        var data_type = 'data:application/calibrator_type_master.ms-excel';
        var table_div = document.getElementById('show_tbl_row');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'calibrator_type_master' + postfix + '.xls';
        a.click();
        e.preventDefault();
         
    })

$('#add_btn').click(function(){
	$('#add_box').show();
	$('#table_box').hide();
	$('#search_box').hide();

});

$('#save_btn').click(function(){
	if($('#add_form').valid()){
		var cname=$('#add_cname').val();
		var description=$('#add_description').val();

		var operation='savecalibratorType';


     $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/calibrator_type_master.php",
         data:{operation:operation,cname:cname,description:description,login_by:login_by}
         }).done(function(result)
         {
         	if(Number(result)>0){
             load_all();
             $('#add-task-success').fadeIn(200).delay(5000).fadeOut(1000);
            $('#add_form')[0].reset(); 
            $('#add_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_calibrator_type();
         }else{
          $('#add_duplicate_entry').fadeIn(200).delay(5000).fadeOut(1000);
         }
        
         });
	}

});

$('#close_btn').click(function(){
	       $('#add_form')[0].reset(); 
            $('#add_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_all();
           load_calibrator_type();

	});

$('#view_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
		
    $('#add_box').hide();
    $('#view_box').show();
    $('#table_box').hide();
    $('#search_box').hide();

    var operation='viewSpecificcalibratorType';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/calibrator_type_master.php",
    data:{operation:operation,id:id}
    }).done(function(result)
    { 
    	$('#view_cname').val(result['calibrator_type_name']);
    	$('#view_description').val(result['calibrator_type_description']);


    });
}
else{
$('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);

}
});

$('#view_close_btn').click(function(){
	
    $('#view_box').hide();
    $('#table_box').show();
    $('#search_box').show();
    load_all();
    load_calibrator_type();

	});


$('#edit_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
    $('#edit_box').show();
    $('#table_box').hide();
    $('#search_box').hide();
     $('#edit_id').val(id);

    var operation='editSpecificcalibratorType';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/calibrator_type_master.php",
    data:{operation:operation,id:id}
    }).done(function(result)
    { 
    	$('#edit_cname').val(result['calibrator_type_name']);
    	$('#edit_description').val(result['calibrator_type_description']);


    });
}
else{
$('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);

}
});


$('#update_btn').click(function(){
 // var x=confirm('Do you want to really update...?')
  //if(x){
	if($('#edit_form').valid()){
		var cname=$('#edit_cname').val();
		var description=$('#edit_description').val();
		var id= $('#edit_id').val();

		var operation='updatecalibratorType';

     $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/calibrator_type_master.php",
         data:{operation:operation,id:id,cname:cname,description:description,login_by:login_by}
         }).done(function(result)
         {
         	if(Number(result)>0){
             load_all();
             load_calibrator_type();
             $('#edit-update-success').fadeIn(200).delay(5000).fadeOut(1000);
            $('#edit_form')[0].reset(); 
            $('#edit_box').hide();
            $('#table_box').show();
           $('#search_box').show();
         }else{
          $('#edit_duplicate_entry').fadeIn(200).delay(5000).fadeOut(1000);
         }
        
         });
	}
//}

});

$('#edit_clear_btn').click(function(){
	       $('#edit_form')[0].reset(); 
            $('#edit_box').hide();
            $('#table_box').show();
           $('#search_box').show();
           load_all();
           load_calibrator_type();

	});

$('#clear_btn').click(function(){
	      
           $('#s_cname').val('');
           $('#s_des').val('');
           
           
           $('#cname_div').html('<label>Calibrator Name</label><select class="form-control filter" id="s_cname" name="s_cname" data-col="Calibrator Type" notEqual="---" data-live-search="true"><option>---</option></select>')
           load_calibrator_type();
           load_all();
	});



});

function load_all(){
    var operation='getAllcalibratorType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_type_master.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
      $('#total').text(result.length);

      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";

        dis+="<td>"+result[i]['calibrator_type_name']+"</td>";
        dis+="<td>"+result[i]['calibrator_type_description']+"</td>"
        
      dis+='</tr>';
  }
      $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
      

});
}

function load_calibrator_type(){
    var operation='loadAllcalibratorType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibrator_type_master.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='';
        dis+='<option id>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option>'+result[i]['calibrator_type_name']+'</option>';
  }
      $('#s_cname').html(dis).selectpicker();;
       
      

});
}
  