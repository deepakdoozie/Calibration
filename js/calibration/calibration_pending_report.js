$(function(){
	load_all();

	$('#export_excel').click(function(e){
       
      var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "-" + month + "-" + year + "_" + hour + ":" + mins;
        var a = document.createElement('a');
        var data_type = 'data:application/calibration_pending_report.ms-excel';
        var table_div = document.getElementById('show_tbl_row');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'calibration_pending_report_' + postfix + '.xls';
        a.click();
        e.preventDefault();
         
    })

    $('#clear_btn').click(function(){
    	$('#s_itype').val('');
    	$('#s_iname').val('');
    	$('#s_ino').val('');
    	load_all();

    });

});
function load_all(){
    var operation='loadAllPendingCalibration';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_pending_report.php",
    data:{operation:operation},
    async:false
   }).done(function(result){
   	
   	 var date=new Date();
      var today=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate());
      var dis='';

      for(i=0;i<result.length;i++){
      	var calibrationDate=result[i]['first_calibration_date'];
   var m=moment(calibrationDate,'YYYY-MM-DD');
   var dif=result[i]['calibration_frequency'];
  var calibrationPeriods=dif;
  var newdate=moment(m).add(calibrationPeriods,'days');
  
  var newdate1=moment(newdate).subtract(result[i]['calibration_alert_days'],'days');


  var endDate = new Date(newdate)
  , startDate   = new Date(newdate1)
  , date  = new Date();
 
  if((startDate <= date) && (date <= endDate)) {
         dis+='<tr>';
        dis+="<td>"+result[i]['instrument_type_name']+"</td>";
        dis+="<td>"+result[i]['instrument_name']+"</td>"
        dis+="<td>"+result[i]['instrument_no']+"</td>"
        dis+="<td>"+result[i]['purchesed_by']+"</td>" 
        //console.log(result[i]['purchesed_date']);
        dis+="<td>"+result[i]['purchesed_date']+"</td>"
        var calibrationDate=result[i]['first_calibration_date'];
       var m=moment(calibrationDate,'YYYY-MM-DD');
       var newdate=moment(m).add(result[i]['calibration_frequency'],'days');
       var calibrationpending=moment(newdate).toDate();
      var date = moment(calibrationpending).format('YYYY-MM-DD');
          console.log(date);
        dis+="<td>"+date+"</td>"
        // console.log(get_print_format(date));
      dis+='</tr>';
  }
}

/*var operation='loadAllPendingInstrumentCalibration';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_pending_report.php",
    data:{operation:operation},
     async:false
   }).done(function(result1){
    console.log(result1);*/
 

/*    for(j=0;j<result1.length;j++){
    var calibrationDate1=result1[j]['next_calibration_date'];
   var m1=moment(calibrationDate1,'YYYY-MM-DD');
  var newdate2=moment(m1).subtract(result1[j]['calibration_alert_days'],'days');


  var endDate1 = new Date(result1[j]['next_calibration_date'])
  , startDate1   = new Date(newdate2)
  , date1  = new Date();
  console.log(startDate1);
   console.log(date1);
    console.log(endDate1);

    var today=moment();
    var start=moment(startDate1);
     var end=moment(endDate1);
     var m1=today.diff(start);
      var m2=end.diff(today);

  var days1 = Number(((m1 / (1000*60*60*24)) % 7));
  

   var days2 = Number(((m2 / (1000*60*60*24)) % 7));



  if((startDate1 <= date1) && (date1 <= endDate1)) {*/
/*var endDate11 = new Date(result1[j]['next_calibration_date']);*/
// console.log(get_print_format(startDate1),111);
/*        dis+='<tr>';
        dis+="<td>"+result1[j]['instrument_type_name']+"</td>";
        dis+="<td>"+result1[j]['instrument_name']+"</td>"
        dis+="<td>"+result1[j]['instrument_no']+"</td>"
        dis+="<td>"+result1[j]['purchesed_by']+"</td>" 
        dis+="<td>"+result1[j]['purchesed_date']+"</td>"       
       dis+="<td>"+result1[j]['next_calibration_date']+"</td>"
        dis+='</tr>';
  }
}*/


     
       
      
// });
    $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
});
}