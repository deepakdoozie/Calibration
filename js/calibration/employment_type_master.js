var flag='';
$(function(){
  var login_by=$.cookie('i');
	load_all();
    load_employment();
    $('#clear_btn').click(function(){ 
       $('#emp_type').val('');
       $('#ename_div').html('<label>Employment Type</label><select class="form-control filter" id="emp_type" name="emp_type" data-col="Employment Type" data-live-search="true"><option>---</option></select>');
       load_employment();
       load_all();
    });

	$('#activate_emply').click(function(){
		var id=$("input[type='radio'][name='ch']:checked").attr('id');
    if(id>0){
		var x=confirm('Do you really want to activate/deactivate employment type?');
		if(x){
		var operation='getActivateFlag';
        $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/employment_type_master.php",
         data:{operation:operation,id:id}
         }).done(function(result)
         {
            console.log(result);
            var a=result[0]['active_flag'];
            var u=result[0]['used_flag'];
            if(a==1){
            	if(u==0){
                console.log(a);
                 console.log(u);
                    var operation='deactivate';
        $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/employment_type_master.php",
         data:{operation:operation,id:id}
         }).done(function(result)
         {
             console.log(result);
             load_all();
             $('#delete-success').fadeIn(200).delay(5000).fadeOut(1000);
         });  
            	}else{
                console.log(a);
                 console.log(u);
            		$('#select-active').fadeIn(200).delay(5000).fadeOut(1000);
            	}
            }else{
               console.log(a);
                 console.log(u);
            	var operation='activate';
        $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/employment_type_master.php",
         data:{operation:operation,id:id}
         }).done(function(result)
         {
             console.log(result);
             load_all();
             $('#delete-success').fadeIn(200).delay(5000).fadeOut(1000);
         });  
            }

         });
     }
   }else{
    $('#select-entry').fadeIn(200).delay(5000).fadeOut(1000);
   }
	});

	$('#edit_clear_btn').click(function(){
       $('#search_box').show();
	   $('#table_box').show();
	   $('#edit_box').hide();
	   load_all();
	});

	$('#edit_update_btn').click(function(){
		var id=$("input[type='radio'][name='ch']:checked").attr('id');
       var type=$('#edit_type').val();
       var desc=$('#edit_desc').val();
       if(type.length==0 && desc.length==0){
          $('#enter-task-fail').fadeIn(200).delay(5000).fadeOut(1000);
       }else{
        validate1(type,desc,id);
       if(flag==1){
       var operation='editEmploymentType';
        $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/employment_type_master.php",
         data:{operation:operation,type:type,desc:desc,id:id,login_by:login_by}
         }).done(function(result)
         {
            console.log(result);
            $('#search_box').show();
          $('#table_box').show();
         $('#edit_box').hide();
         load_all();
         $('#edit-update-success').fadeIn(200).delay(5000).fadeOut(1000);
         });
     }else{
     	$('#update-task-fail').fadeIn(200).delay(5000).fadeOut(1000);
     }
   }
	});

	$('#edit_emply').click(function(){
		var id=$("input[type='radio'][name='ch']:checked").attr('id');
		if(id>0){
			$('#search_box').hide();
			$('#table_box').hide();
			$('#edit_box').show();
           var operation='getEmploymentType';
            $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/employment_type_master.php",
         data:{operation:operation,id:id}
         }).done(function(result)
         {
            console.log(result);
            $('#edit_type').val(result[0]['employment_type_name']);
            $('#edit_desc').val(result[0]['description']);
        });
		}else{
			$('#select-entry').fadeIn(200).delay(5000).fadeOut(1000);
		}

	});

	$('#add_emply').click(function(){
         $('#table_box').hide();
         $('#search_box').hide();
         $('#add_box').show();
	});

	$('#add_save_btn').click(function(){
       
       var type=$('#add_etype').val();
       var desc=$('#add_desc').val();
        console.log(type);
         console.log(desc);
       if(type==''){
        $('#enter-fail').fadeIn(200).delay(5000).fadeOut(1000);
       }else{
       validate(type);
       if(flag==1){
        var operation='addEmploymentType';
     $.ajax({
       type: "POST",
        url: "../php/ajaxInterface/employment_type_master.php",
         data:{operation:operation,type:type,desc:desc,login_by:login_by}
         }).done(function(result)
         {
            console.log(result);
          $('#table_box').show();
         $('#add_box').hide();
         load_all();
         $('#add_etype').val('');
         $('#add_desc').val('');
         $('#add-task-success').fadeIn(200).delay(5000).fadeOut(1000);
         });
     }else{
     	$('#add-task-fail').fadeIn(200).delay(5000).fadeOut(1000);
     }
   }
	});


	$('#add_clear_btn').click(function(){
       $('#search_box').show();
	   $('#table_box').show();
	   $('#add_box').hide();
	    $('#add_etype').val('');
         $('#add_desc').val('');
	   load_all();
	});

});

function load_employment(){
  var operation='loademployment';
  $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/employment_type_master.php",
    data:{operation:operation}
    }).done(function(result)
    { 
       var dis='';
        dis+='<option>---</option>';
          for(i=0;i<result.length;i++){
         dis+='<option>'+result[i]['employment_type_name']+'</option>';
         }
          $('#emp_type').html(dis).selectpicker();
         /* $('.filter').multifilter({
          'target' : $('#myTable')
         });*/

    });
}


function load_all(){
    var operation='getAllEmploymentType';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/employment_type_master.php",
    data:{operation:operation}
   }).done(function(result){
      //console.log(result);

      var dis='';

      for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['employment_type_uid']+"></td>";
        dis+="<td>"+result[i]['employment_type_name']+"</td>"
        dis+="<td>"+result[i]['description']+"</td>"
         if(result[i]['active_flag']==1){
          dis+="<td>"+'Active'+"</td>"
         }
         else{
          dis+="<td style='color:red'>"+'De-active'+"</td>"
         }
      dis+='</tr>';
  }
      $('#view').html(dis);
      $('.filter').multifilter({
          'target' : $('#myTable')
        });
      

});
}

function validate(type){
	var type1=type;
	console.log(type);
	console.log(type1);
    var operation='invalidate';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/employment_type_master.php",
    async:false,
    data:{operation:operation,type:type1}
   }).done(function(result){
      console.log(result); 
      if(result.length>0){
      	flag=0;
      }else{
      	flag=1;
      }

   });
}

function validate1(type,desc,id){
  var type1=type;
  console.log(type);
  console.log(type1);
    var operation='invalidate1';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/employment_type_master.php",
    async:false,
    data:{operation:operation,type:type1,id:id}
   }).done(function(result){
      console.log(result); 
      if(result.length >0){
         flag=0;
      }
      else{
        flag=1;
      }
  

   });
}