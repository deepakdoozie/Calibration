 var filename=[];
 var calibrator_address=[];
 var instrument_details=[];
 var instrument_array=[];

 
$(function(){
  var login_by=$.cookie('i');
   load_all();
   var dis='';
  for(var i=0;i<100;i++){
    dis+="<td colspan=1% style='padding:0;margin:0;' >";
    dis+="<div style='text-align:center'><span style='font-size:9.0pt'><span></span></div>";
    dis+="</td>";
  }
  $('#setter').html(dis);

  $('#issue_btn').click(function(){
     $('#add_box').show();
    $('#table_box').hide();
    $('#search_box').hide();
    load_all_calibrator_type();
    load_transport_type();

  });
   $('#add_calibrator_type').on('change',function(){
     on_change_calibrator_type();
    });

     $('#add_instrument_type').on('change',function(){
     on_change_instrument_type();
    });

       $('#add_instrument_name').on('change',function(){
    on_change_instrument_name();

    });

  $('#add_instrument_btn').click(function(){
   var transport_id=$('#add_transport_type').children(':selected').attr('id');
    if(Number(transport_id) >0){
    var arrayLengh=instrument_array.length; 
    var instrument_type=$('#add_instrument_type').val();
    var instrument_name=$('#add_instrument_name').val();
    if($('#add_instrument_form').valid()){
    if(instrument_type != '---' && instrument_name != '---'){
         
   if(arrayLengh=='0'){
    console.log("empty");
  var instrument_name_id=$('#add_instrument_name').children(':selected').attr('id');
   instrument_array.push(instrument_name_id);
   add_issue_instrument();
   }
   else{
    var flag=1;
      console.log("non-empty");
    var instrument_name_id=$('#add_instrument_name').children(':selected').attr('id');
    for (var i = 0; i < instrument_array.length; i++) {
      if(instrument_name_id==instrument_array[i]){
        flag=0;
$('#instrument-already-enterd').fadeIn(200).delay(5000).fadeOut(1000);
      }
       };
      if(flag){
          instrument_array.push(instrument_name_id);
          add_issue_instrument();
      }
   

   }
   
}
else{
$('#select-all').fadeIn(200).delay(5000).fadeOut(1000);
}
}
}
else{
$('#select-transport-type').fadeIn(200).delay(5000).fadeOut(1000);

}
  });

 $('#add_due_btn').click(function(){
  var d=validate_due_expected_date();
  if(d){
var transport_id=$('#add_transport_type').children(':selected').attr('id');
    if(Number(transport_id) >0){
    var due_insert='';
     var arrayLengh=instrument_array.length; 
     $('#show_instrument').show();
        $('#save_div').show();
      if(arrayLengh=='0'){
          $('#view_due_instrument> tr').each(function(){
      var x=$(this).find('input').serializeArray();
    console.log(x);
       if(x[0]['value']=='on'){
         $(this).hide();
       var  c_type=$('#add_calibrator_type').val();
       var  calibrator=$('#add_calibrator').val();
       var instrument_type=x[3]['value'];
       var instrument_name=x[4]['value'];
       var instrument_no=x[5]['value'];
       var expected_date=x[1]['value'];
      var instrument_id=x[2]['value'];
       
       var uom=x[6]['value'];
        var desc=x[7]['value'];
      instrument_array.push(instrument_id);

       due_insert+='<tr>'
       due_insert+='<td>'+c_type+'</td>'
       due_insert+='<td>'+calibrator+'</td>'
       due_insert+='<td>'+instrument_type+'</td>'
       due_insert+='<td>'+instrument_name+'</td>'
       due_insert+='<td>'+instrument_no+'</td>'
       due_insert+='<td>'+get_print_format(expected_date)+'</td>'
       due_insert+='<input type="hidden" name="instrument_id" value='+instrument_id+'>'
       due_insert+='<input type="hidden" name="e_date" value='+expected_date+'>'
       
       due_insert+='<input type="hidden" name="desc" value="'+desc+'">'
       due_insert+='<input type="hidden" name="uom" value='+uom+'>'
       
       due_insert+='</tr>'
        
     }
       

       });
          $('#view_instrument').append(due_insert);
   
   }
   else{
    var flag=1;
      console.log("non-empty");
      var n=due_validate_instrument();
      console.log(n);
      if(n){
       console.log(instrument_array);
          $('#view_due_instrument> tr').each(function(){
      var x=$(this).find('input').serializeArray();

       if(x[0]['value']=='on'){
        var instrument_id=x[2]['value'];
        console.log(instrument_id);
        console.log(instrument_array);
           for (var i = 0; i < instrument_array.length; i++) {
      if(Number(instrument_id)==Number(instrument_array[i])){
          console.log(i);
          flag=0;
         $(this).css('background','red');
$('#instrument-already-enterd').fadeIn(200).delay(5000).fadeOut(1000);
       
      }
    }
      if(flag){
        console.log(i);
        $(this).hide();
         var  c_type=$('#add_calibrator_type').val();
       var  calibrator=$('#add_calibrator').val();
       var instrument_type=x[3]['value'];
       var instrument_name=x[4]['value'];
       var instrument_no=x[5]['value'];
       var expected_date=x[1]['value'];
      var instrument_id=x[2]['value'];

       var uom=x[6]['value'];
        var desc=x[7]['value'];
       
       instrument_array.push(instrument_id);

       due_insert+='<tr>'
       due_insert+='<td>'+c_type+'</td>'
       due_insert+='<td>'+calibrator+'</td>'
       due_insert+='<td>'+instrument_type+'</td>'
       due_insert+='<td>'+instrument_name+'</td>'
       due_insert+='<td>'+instrument_no+'</td>'
       due_insert+='<td>'+get_print_format(expected_date)+'</td>'
       due_insert+='<input type="hidden" name="instrument_id" value='+instrument_id+'>'
       due_insert+='<input type="hidden" name="e_date" value='+expected_date+'>'
       
       due_insert+='<input type="hidden" name="desc" value="'+desc+'">'
       due_insert+='<input type="hidden" name="uom" value='+uom+'>'

       due_insert+='</tr>'
         
      }
   
       }
     });
      $('#view_instrument').append(due_insert);
   }
 }
}
else{
$('#select-transport-type').fadeIn(200).delay(5000).fadeOut(1000);

}
  }
 });

  $('#add_pending_btn').click(function(){

    var d=validate_pending_expected_date();
  if(d){
    var transport_id=$('#add_transport_type').children(':selected').attr('id');
    if(Number(transport_id) >0){
    var pending_insert='';
     var arrayLengh=instrument_array.length; 
     $('#show_instrument').show();
        $('#save_div').show();
      if(arrayLengh=='0'){
          $('#view_pending_instrument> tr').each(function(){
      var x=$(this).find('input').serializeArray();

       if(x[0]['value']=='on'){
         $(this).hide();
       var  c_type=$('#add_calibrator_type').val();
       var  calibrator=$('#add_calibrator').val();
       var instrument_type=x[3]['value'];
       var instrument_name=x[4]['value'];
       var instrument_no=x[5]['value'];
       var expected_date=x[1]['value'];
      var instrument_id=x[2]['value'];

       var uom=x[6]['value'];
      var desc=x[7]['value'];
       
      instrument_array.push(instrument_id);

       pending_insert+='<tr>'
       pending_insert+='<td>'+c_type+'</td>'
       pending_insert+='<td>'+calibrator+'</td>'
       pending_insert+='<td>'+instrument_type+'</td>'
       pending_insert+='<td>'+instrument_name+'</td>'
       pending_insert+='<td>'+instrument_no+'</td>'
       pending_insert+='<td>'+get_print_format(expected_date)+'</td>'
       pending_insert+='<input type="hidden" name="instrument_id" value='+instrument_id+'>'
       pending_insert+='<input type="hidden" name="e_date" value='+expected_date+'>'
        
       pending_insert+='<input type="hidden" name="desc" value="'+desc+'">'
      pending_insert+='<input type="hidden" name="uom" value='+uom+'>'

       pending_insert+='</tr>'
        
     }
       

       });
          $('#view_instrument').append(pending_insert);
   
   }
   else{
    var flag=1;
      console.log("non-empty");
      var n=pending_validate_instrument();
      console.log(n);
      if(n){
       console.log(instrument_array);
          $('#view_pending_instrument> tr').each(function(){
      var x=$(this).find('input').serializeArray();

       if(x[0]['value']=='on'){
        var instrument_id=x[2]['value'];
        console.log(instrument_id);
        console.log(instrument_array);
           for (var i = 0; i < instrument_array.length; i++) {
      if(Number(instrument_id)==Number(instrument_array[i])){
          console.log(i);
          flag=0;
         $(this).css('background','red');
$('#instrument-already-enterd').fadeIn(200).delay(5000).fadeOut(1000);
       
      }
    }
      if(flag){
        console.log(i);
        $(this).hide();
         var  c_type=$('#add_calibrator_type').val();
       var  calibrator=$('#add_calibrator').val();
       var instrument_type=x[3]['value'];
       var instrument_name=x[4]['value'];
       var instrument_no=x[5]['value'];
       var expected_date=x[1]['value'];
      var instrument_id=x[2]['value'];

       var uom=x[6]['value'];
        var desc=x[7]['value'];
       
       instrument_array.push(instrument_id);

       pending_insert+='<tr>'
       pending_insert+='<td>'+c_type+'</td>'
       pending_insert+='<td>'+calibrator+'</td>'
       pending_insert+='<td>'+instrument_type+'</td>'
       pending_insert+='<td>'+instrument_name+'</td>'
       pending_insert+='<td>'+instrument_no+'</td>'
       pending_insert+='<td>'+get_print_format(expected_date)+'</td>'
       pending_insert+='<input type="hidden" name="instrument_id" value='+instrument_id+'>'
       pending_insert+='<input type="hidden" name="e_date" value='+expected_date+'>'
       

       pending_insert+='<input type="hidden" name="desc" value="'+desc+'">'
      pending_insert+='<input type="hidden" name="uom" value='+uom+'>'

       pending_insert+='</tr>'
         
      }
   
       }
     });
      $('#view_instrument').append(pending_insert);
   }
 }
  }

else{
$('#select-transport-type').fadeIn(200).delay(5000).fadeOut(1000);
  }
}

 });


 $('#confirm_btn').click(function(){
 $('#print-box').show();
 $('#calibrator_address').html(calibrator_address['calibrator_address']);
  var dis='';
  var i=1;
$('#view_instrument> tr').each(function(){
      var x=$(this).find('input').serializeArray();
          console.log(x);
          dis+='<tr style="text-align: center;"  class="b_row">';
          dis+='<td colspan="10%" style="padding-top: 2px; padding-bottom:1px;">'+i+'</td>';
          dis+='<td colspan="50%" style="padding-top: 2px; padding-bottom:1px;">'+x[2]['value']+'</td>';
          dis+='<td colspan="10%" style="padding-top: 2px; padding-bottom:1px;">'+1+'</td>';
          dis+='<td colspan="10%" style="padding-top: 2px; padding-bottom:1px;">'+x[3]['value']+'</td>';
          dis+='<td colspan="10%" style="padding-top: 2px; padding-bottom:1px; text-align:centre;">'+get_print_format(x[1]['value'])+'</td>';
          dis+='<td colspan="10%" style="padding-top: 2px; padding-bottom:1px;"></td>'
          dis+='</tr>';
          i++;
        });

$('#view-item-row').html(dis);


$('#note').html($('#add_note').val());
 });

 $('#confirm_dc').click(function(){
  console.log("dfsdfsdfsdfsdf");
  var add_calibrator_type=$('#add_calibrator_type').children(':selected').attr('id');
   var transport_id=$('#add_transport_type').children(':selected').attr('id');
   console.log(transport_id);
   if(Number(transport_id != '-1')){
    var refStd=$('#add_std').val();
   var note=$('#add_note').val();
   console.log(refStd);
   console.log(note);
   var operation='saveDC';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation,refStd:refStd,note:note,transport_id:transport_id,login_by:login_by,add_calibrator_type:add_calibrator_type},
    async:false
    }).done(function(result)
    { 
    console.log(result);

    $('#view_instrument> tr').each(function(){
      var x=$(this).find('input').serializeArray();
          console.log(x);
      var instrument_name_id=x[0]['value'];
      var e_date=x[1]['value']
          var operation='saveDCItems';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation,dc_id:result,instrument_name_id:instrument_name_id,e_date:e_date},
    async:false
    }).done(function(result)
    { 
    console.log(result);
    $('#view-item-row').html('');
    $('#print-box').hide();
    $('#save_div').hide();
    $('#view_instrument').html('');
    $('#show_instrument').hide();
    $('#add_box').hide();
    $('#table_box').show();
    $('#search_box').hide();
    $('#add-task-success').fadeIn(200).delay(5000).fadeOut(1000);
    

     $('#add_calibrator_type').val('');
     $('#calibrator_type_div').html('<label>Calibrator Type</label><select class="form-control filter" id="add_calibrator_type" name="add_calibrator_type" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>')
     $('#add_calibrator').val('');
     $('#add_std').val('');
     $('#add_note').val('');

     $('#add_instrument_type').val('');
     $('#instrument_type_div').html('<label>Instrument Type</label><select class="form-control filter" id="add_instrument_type" name="add_instrument_type" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>');
     
     $('#add_instrument_name').val('');
     $('#instrument_name_div').html('<label>Instrument Name</label><select class="form-control filter" id="add_instrument_name" name="add_instrument_name" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>');
     $('#add_instrument_no').val('');
     $('#add_instrument_uom').val('');
     $('#add_cali_date').val('');
      $('#add_edate').val('');
     $('#show_instrument').hide();

     $('#save_div').hide();
    });
    });
 instrument_array='';
      load_all();
      $('#view-item-row').html('');
      window.location.reload();
        });
     }
     else{
  $('#select-transport-type').fadeIn(200).delay(5000).fadeOut(1000);
     }
    });




 $('#confirm_cancel_btn').click(function(){

   $('#view-item-row').html('');
    $('#print-box').hide();
    $('#save_div').hide();
    $('#view_instrument').html('');
    $('#show_instrument').hide();
    $('#add_box').hide();
    $('#table_box').show();
    $('#search_box').hide();
     load_all();

     $('#add_calibrator_type').val('');
     $('#calibrator_type_div').html('<label>Calibrator Type</label><select class="form-control filter" id="add_calibrator_type" name="add_calibrator_type" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>')
     $('#add_calibrator').val('');
     $('#add_std').val('');
     $('#add_note').val('');

     $('#add_instrument_type').val('');
     $('#instrument_type_div').html('<label>Instrument Type</label><select class="form-control filter" id="add_instrument_type" name="add_instrument_type" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>');
     
     $('#add_instrument_name').val('');
     $('#instrument_name_div').html('<label>Instrument Name</label><select class="form-control filter" id="add_instrument_name" name="add_instrument_name" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>');
     $('#add_instrument_no').val('');
     $('#add_instrument_uom').val('');
     $('#add_cali_date').val('');
      $('#add_edate').val('');
     $('#show_instrument').hide();

     $('#save_div').hide();
     window.location.reload();

 });

 $('#clear_btn').click(function(){

  $('#s_date').val('');
  $('#s_dc').val('');
  load_all();
 });

 $('#view_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
    
    $('#add_box').hide();
    $('#view_box').show();
    $('#table_box').hide();
    $('#search_box').hide();

    var operation='loadALLDCItems';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation,id:id},
     async:false
    }).done(function(result)
    { 
    console.log(result);
      $('#view_dc_date').val(get_print_format(result[0]['created_at']));
      $('#view_ref_std').val(result[0]['reference_standard']);
      $('#view_note').val(result[0]['note']);
      $('#view_dc_no').val(result[0]['uid']);
      var dis='';
      var inc=1;
    for (var i = 0; i < result.length; i++) {
      console.log(i);
      var operation='loadALLDCInstrument';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation,instrument_type_details_id:result[i]['instrument_type_details_id'],dc_id:result[i]['uid']},
     async:false
    }).done(function(result)
    { 
    console.log(result);
    $('#view_cali_type').val(result['calibrator_type_name']);
    $('#view_cali_address').val(result['calibrator_address']);
      $('#view_cali_name').val(result['calibrator_name']);
        dis+='<tr>';
        dis+='<th>'+inc+'</th>';
        dis+="<td>"+result['instrument_type_name']+"</td>"
        dis+="<td>"+result['instrument_name']+"</td>";
        dis+="<td>"+result['instrument_no']+"</td>"
        dis+="<td>"+get_print_format(result['expected_date'])+"</td>";
        dis+="<td>"+get_print_format(result['next_calibration_date'])+"</td>"
        dis+='</tr>';

  });
    inc++;
  }
   $('#view_dc_tbl').html(dis);
});

}
else{
$('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);

}
});

 $('#add_instrument').click(function(){
  var check=check_calibration_type();
  if(Number(check)=='1'){
   $('#add_instrument_div').show();
    $('#load_due_div').hide();
  $('#load_pending_div').hide();
  $('#add_instrument').css('background','#d43f3a');
  $('#add_due').css('background','yellow');
  $('#add_pending').css('background','yellow');


}
else{
 $('#select-calibrator').fadeIn(200).delay(5000).fadeOut(1000);
}

 });

 $('#add_instrument_close_btn').click(function(){
  $('#add_instrument_form')[0].reset();
  $('#add_box').hide();
   $('#table_box').show();
    $('#search_box').show();
    load_all();

     $('#add_calibrator_type').val('');
     $('#calibrator_type_div').html('<label>Calibrator Type</label><select class="form-control filter" id="add_calibrator_type" name="add_calibrator_type" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>')
     $('#add_calibrator').val('');
     $('#add_std').val('');
     $('#add_note').val('');

     $('#add_instrument_type').val('');
     $('#instrument_type_div').html('<label>Instrument Type</label><select class="form-control filter" id="add_instrument_type" name="add_instrument_type" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>');
     
     $('#add_instrument_name').val('');
     $('#instrument_name_div').html('<label>Instrument Name</label><select class="form-control filter" id="add_instrument_name" name="add_instrument_name" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>');
     $('#add_instrument_no').val('');
     $('#add_instrument_uom').val('');
     $('#add_cali_date').val('');
      $('#add_edate').val('');
     $('#show_instrument').hide();
    
    $('#add_transport_type').val('');
    $('#transport_type_div').html(' <label>Transport Type</label><select class="form-control filter" id="add_transport_type" name="add_transport_type"  notEqual="---" data-live-search="true"></select>');
    load_all_calibrator_type();
    load_transport_type();
    on_change_calibrator_type();
    window.location.reload();



 });

  $('#add_due_close_btn').click(function(){
  $('#view_due_instrument').html('');
  $('#load_due_div').hide();
    $('#add_due').css('background','yellow');

 });

    $('#add_pending_close_btn').click(function(){
  $('#view_pending_instrument').html('');
  $('#load_pending_div').hide();
    $('#add_pending').css('background','yellow');

 });

   $('#add_due').click(function(){
  var check=check_calibration_type();
  if(Number(check)=='1'){
   $('#add_instrument_div').hide();
  $('#load_due_div').show();
  $('#load_pending_div').hide();
   $('#add_due').css('background','#d43f3a');
  $('#add_instrument').css('background','yellow');
  $('#add_pending').css('background','yellow');
  var calintator_id=$('#add_calibrator_type').children(':selected').attr('id');

  load_due_instrument(calintator_id,'due');
}
else{
 $('#select-calibrator').fadeIn(200).delay(5000).fadeOut(1000);
}

 });

    $('#add_pending').click(function(){
  var check=check_calibration_type();
  if(Number(check)=='1'){
    $('#add_instrument_div').hide();
    $('#load_due_div').hide();
  $('#load_pending_div').show();
  $('#add_pending').css('background','#d43f3a');
    $('#add_due').css('background','yellow');
  $('#add_instrument').css('background','yellow');
  var calintator_id=$('#add_calibrator_type').children(':selected').attr('id');
  load_pending_instrument(calintator_id,'pending');
}
else{
 $('#select-calibrator').fadeIn(200).delay(5000).fadeOut(1000);
}

 });

    $('#cancel-print').click(function(){

      $('#view-item-row').html('');
      $('#print-box').hide();
      window.location.reload();


    });



})

 function check_calibration_type(){
  var type=$('#add_calibrator_type').val();
  if(type != '---'){
    return 1;
  }
  else{
    return -1;
  }

 }

 function load_due_instrument(calibrator_id,type){
  console.log(calibrator_id);
   console.log(type);
   var dueDis='';
  

   var operation='loadAllDueCalibration';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation,calibrator_id:calibrator_id},
    async:false
    }).done(function(result)
    { 
      console.log(result);
      var date=new Date();
      var today=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate());
      for(i=0;i<result.length;i++){
    var calibrationDate=result[i]['first_calibration_date'];
   console.log(calibrationDate,"calibrationDate");
   var m=moment(calibrationDate,'YYYY-MM-DD');
   console.log(m,"m");
   var dif=result[i]['calibration_frequency'];
  var calibrationPeriods=dif;
  console.log(dif,"dif");
  var newdate=moment(m).add(calibrationPeriods,'days');
  console.log(newdate,"newdate");
  var calibrationpending=moment(newdate).toDate();
  var date = moment(newdate).format('YYYY-MM-DD');
  
  var diffDate=new Date(today)-new Date(date);
  // var diffDate=new Date(date)-new Date(today);
console.log(today,"today");
console.log(date,"date");
console.log(diffDate,"diffDate");
  if(diffDate>0){
         dueDis+='<tr>';
         dueDis+="<td  style='width:10px;'><input type='checkbox'  name='due_check' id="+result[i]['uid']+"></td>";
        dueDis+="<td>"+result[i]['instrument_type_name']+"</td>";
        dueDis+="<td>"+result[i]['instrument_name']+"</td>"
        dueDis+="<td>"+result[i]['instrument_no']+"</td>"
        var calibrationDate=result[i]['first_calibration_date'];
       var m=moment(calibrationDate,'YYYY-MM-DD');
       var newdate=moment(m).add(result[i]['calibration_frequency'],'days');
       var calibrationpending=moment(newdate).toDate();
      var date = moment(calibrationpending).format('YYYY-MM-DD');
        dueDis+="<td>"+date+"</td>"
        dueDis+='<td><input type="date" name="add_due_edate"></td>'
        dueDis+='<td><select class="form-control" id="d_calibrator_type'+i+'" name="d_calibrator_type'+i+'" notEqual="0" data-live-search="true" onchange="selectDueCalibratorType('+i+')"></select></td>'
        dueDis+='<input type="hidden" id="instrument_id" name="instrument_id" value='+result[i]['uid']+'>'
      
      dueDis+='<input type="hidden"  name="instrument_type" value="'+result[i]['instrument_type_name']+'">'
      dueDis+='<input type="hidden"  name="instrument_name" value="'+result[i]['instrument_name']+'">'
      dueDis+='<input type="hidden"  name="instrument_no" value="'+result[i]['instrument_no']+'">'
      
      dueDis+='<input type="hidden"  name="instrument_uom" value="'+result[i]['uom']+'">'
      dueDis+='<input type="hidden"  name="item_description" value="'+result[i]['instrument_type_name']+'-'+result[i]['instrument_name']+'-'+result[i]['instrument_no']+'">'
dueDis+='<input type="hidden"  name="due_calibrator_type_'+i+'" id="due_calibrator_type_'+i+'" value="">'
      dueDis+='</tr>';
  }
}
    
      $('#view_due_instrument').html(dueDis);

      for(var j=0;j<result.length;j++){
var dis1='<option id="0">---</option>';
       var res=result[j]['calibrator_type_array'];
      for(i=0;i<res.length;i++){  
        dis1+='<option id='+res[i]['uid']+'>'+res[i]['calibrator_type_name']+'</option>';
       }
      $('#d_calibrator_type'+j).html(dis1).selectpicker();
}
});

 }

  function load_pending_instrument(type){
  // console.log(calibrator_id);
  var calibrator_id=1;
   console.log(type);
   var pendingDis='';   

   // same operation is used for both pending and due
   var operation='loadAllPendingInstrumentCalibration';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation},
    async:false
    }).done(function(result1)
    { 
      console.log(result1);
      var date=new Date();
      var today=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate());
      var dis='';

      for(j=0;j<result1.length;j++){
        var calibrationDate=result1[j]['first_calibration_date'];
   var m=moment(calibrationDate,'YYYY-MM-DD');
   var dif=result1[j]['calibration_frequency'];
  var calibrationPeriods=dif;
  var newdate=moment(m).add(calibrationPeriods,'days');
  
  var newdate1=moment(newdate).subtract(result1[j]['calibration_alert_days'],'days');

  var endDate = new Date(newdate)
  , startDate   = new Date(newdate1)
  , date  = new Date();

  /*console.log(endDate,"final calibration date");
  console.log(date,"today");
  console.log(startDate,"alert day");*/
 
  if((startDate <= date) && (date <= endDate)) {
        pendingDis+='<tr>';
        pendingDis+="<td  style='width:10px;'><input type='checkbox'  name='pending_check' id="+result1[j]['uid']+"></td>";
        pendingDis+="<td>"+result1[j]['instrument_type_name']+"</td>";
        pendingDis+="<td>"+result1[j]['instrument_name']+"</td>"
        pendingDis+="<td>"+result1[j]['instrument_no']+"</td>"
        var calibrationDate=result1[j]['first_calibration_date'];
       var m=moment(calibrationDate,'YYYY-MM-DD');
       var newdate=moment(m).add(result1[j]['calibration_frequency'],'days');
       var calibrationpending=moment(newdate).toDate();
      var date = moment(calibrationpending).format('YYYY-MM-DD');
        
        pendingDis+="<td>"+date+"</td>"
         pendingDis+='<td><input type="date" name="add_pending_edate"></td>'
         pendingDis+='<td><select class="form-control" id="s_calibrator_type'+j+'" name="s_calibrator_type'+j+'" notEqual="0" data-live-search="true" onchange="selectCalibratorType('+j+');"></select></td>'

        pendingDis+='<input type="hidden" id="instrument_id"  name="instrument_id" value='+result1[j]['uid']+'>'
        

      pendingDis+='<input type="hidden"  name="instrument_type" value="'+result1[j]['instrument_type_name']+'">'
      pendingDis+='<input type="hidden"  name="instrument_name" value="'+result1[j]['instrument_name']+'">'
      pendingDis+='<input type="hidden"  name="instrument_no" value="'+result1[j]['instrument_no']+'">'
      
      pendingDis+='<input type="hidden"  name="instrument_uom" value="'+result1[j]['uom']+'">'
      pendingDis+='<input type="hidden"  name="item_description" value="'+result1[j]['instrument_type_name']+'-'+result1[j]['instrument_name']+'-'+result1[j]['instrument_no']+'">'
      pendingDis+='<input type="hidden"  name="pending_calibrator_type_'+j+'" id=pending_calibrator_type_'+j+' value="">'
      pendingDis+='</tr>';

       

  }
}

$('#view_pending_instrument').html(pendingDis);  

for(j=0;j<result1.length;j++){
var dis1='<option id="0">---</option>';
       var res=result1[j]['calibrator_type_array'];
      for(i=0;i<res.length;i++){  
        dis1+='<option id='+res[i]['uid']+'>'+res[i]['calibrator_type_name']+'</option>';
       }
      $('#s_calibrator_type'+j).html(dis1).selectpicker();
}
      
    });
      

      


 }

function load_all_calibrator_type(){
      var operation='loadAllCalibratorType';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation},
    async:false
    }).done(function(result)
    { 
    console.log(result);
     var dis='';
        dis+='<option id="-1">---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['calibrator_type_name']+'</option>';
  }
      $('#add_calibrator_type').html(dis).selectpicker();
      $('#add_calibrator_type').selectpicker('refresh');

    });
}

function on_change_calibrator_type(){
  var calibrator_type_id=$('#add_calibrator_type').children(':selected').attr('id');
   if(calibrator_type_id>0){
     $('#tab_div').show();
    var operation='loadSpecificCalibrator';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation,calibrator_type_id:calibrator_type_id},
     async:false
    }).done(function(result)
    { 
      console.log(result);
      $('#add_calibrator').val(result['calibrator_name']);
      calibrator_address=result;
       var operation='loadSpecificCalibratorInstrument';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation,calibrator_id:result['uid']},
     async:false
    }).done(function(result1)
    { 
      console.log(result1);
      
       var dis1='<option id="-1">---</option>';
      for(i=0;i<result1.length;i++){  
      console.log(result1[i]['uid']);
         dis1+='<option id='+result1[i]['uid']+'>'+result1[i]['instrument_type_name']+'</option>';
  }
      $('#add_instrument_type').html(dis1).selectpicker();
        $('#add_instrument_type').selectpicker('refresh');


    });

    });

   }
   else{
     $('#tab_div').hide();
    $('#add_calibrator').val('');
    $('#add_instrument_type').val('');
    $('#instrument_type_div').html('<label>Instrument Type</label><select class="form-control filter" id="add_instrument_type" name="add_instrument_type" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>');
 $('#select-calibrator').fadeIn(200).delay(5000).fadeOut(1000);
   
   $('#add_instrument_name').val('');
    $('#instrument_name_div').html('<label>Instrument Name</label><select class="form-control filter" id="add_instrument_name" name="add_instrument_name" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>');
   
   }
}


function selectCalibratorType(j)
{
  $('#pending_calibrator_type_'+j).val($('#s_calibrator_type'+j).children(':selected').attr('id'));
}


function selectDueCalibratorType(i)
{
  $('#due_calibrator_type_'+i).val($('#d_calibrator_type'+i).children(':selected').attr('id'));
}


function on_change_instrument_type(){

  var instrument_type_id=$('#add_instrument_type').children(':selected').attr('id');
   if(instrument_type_id>0){
    $('#add_instrument_no').val('');
    $('#add_purchased').val('');
    $('#add_cali_date').val('');
    
     // $('#add_instrument_name').val('');
     // $('#instrument_name_div').html('<label>Instrument Name</label><select class="form-control filter" id="add_instrument_name" name="add_instrument_name" notEqual="---" data-live-search="true"></select>');

    var operation='loadSpecificInstrumentDetails';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation,instrument_type_id:instrument_type_id},
     async:false
    }).done(function(result)
    { 
    console.log(result);
     instrument_details=result;
     // $('#add_instrument_name').selectpicker('refresh');

     var dis1='<option id="-1">---</option>';
      for(i=0;i<result.length;i++){  
    dis1+='<option id='+result[i]['uid']+'>'+result[i]['instrument_name']+'</option>';
  }
      $('#add_instrument_name').html(dis1).selectpicker();
          $('#add_instrument_name').selectpicker('refresh');

      });
  }
  else{
    $('#add_instrument_name').val('');
      $('#add_instrument_no').val('');
    $('#add_purchased').val('');
    $('#add_cali_date').val('');
    $('#instrument_name_div').html('<label>Instrument Name</label><select class="form-control filter" id="add_instrument_name" name="add_instrument_name" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>');
     $('#select-instrument-type').fadeIn(200).delay(5000).fadeOut(1000);
  

  }
}


function on_change_instrument_name(){
  var instrument_name_id=$('#add_instrument_name').children(':selected').attr('id');
   if(instrument_name_id>0){
    var operation='loadSpecificInstrumentName';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation,instrument_name_id:instrument_name_id},
     async:false
    }).done(function(result)
    { 
    console.log(result);

    $('#add_instrument_no').val(result['instrument_no']);
    $('#add_instrument_uom').val(result['uom']);
    $('#add_cali_date').val(result['next_calibration_date']);
 

  });
  }
  else{
     $('#add_instrument_no').val('');
    $('#add_purchased').val('');
    $('#add_cali_date').val('');
 $('#select-instrument-name').fadeIn(200).delay(5000).fadeOut(1000);
  

  }
}

function load_all(){

    var operation='loadALLDC';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation},
     async:false
    }).done(function(result)
    { 
    console.log(result);
    var dis='';
    for (var i = 0; i < result.length; i++) {
        dis+='<tr>';
        dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";
        dis+="<td>"+result[i]['uid']+"</td>";
        dis+="<td>"+get_print_format(result[i]['created_at'])+"</td>"
        dis+="<td>"+result[i]['calibrator_type_name']+"</td>"
        dis+="<td>"+result[i]['calibrator_contact_no']+"</td>"
        dis+="<td>"+result[i]['total']+"</td>"
        dis+='</tr>';
    }
    $('#view').html(dis);
     $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
  });
}

function load_transport_type(){

  var operation='loadAllTranport';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/issue_instrument.php",
    data:{operation:operation},
     async:false
    }).done(function(result)
    { 
    console.log(result);
     var dis1='<option id="-1">---</option>';
      for(i=0;i<result.length;i++){  
    dis1+='<option id='+result[i]['t_id']+'>'+result[i]['t_type']+'</option>';
  }
      $('#add_transport_type').html(dis1).selectpicker();

      });

}

function add_issue_instrument(){
  $('#show_instrument').show();
    $('#save_div').show();
   var insert='';
    var  c_type=$('#add_calibrator_type').val();
    var  calibrator=$('#add_calibrator').val();
    var  i_type=$('#add_instrument_type').val();
    var  i_name=$('#add_instrument_name').val();
    var  i_no=$('#add_instrument_no').val();

    var e_date=$('#add_edate').val();
    var ref=$('#add_std').val();

    var instrument_name_id=$('#add_instrument_name').children(':selected').attr('id');

    var instrument_type=$('#add_instrument_type').val();
    var instrument_name=$('#add_instrument_name').val();
    var instrument_no=$('#add_instrument_no').val();
    var uom=$('#add_instrument_uom').val();

    insert+='<tr>';
    insert+='<td>'+c_type+'</td>';
    insert+='<td>'+calibrator+'</td>';
    insert+='<td>'+i_type+'</td>';
    insert+='<td>'+i_name+'</td>';
    insert+='<td>'+i_no+'</td>';
    insert+='<td>'+get_print_format(e_date)+'</td>';
    insert+='<input type="hidden" name="instrument_name_id" value="'+instrument_name_id+'">';
    insert+='<input type="hidden" name="e_date" value="'+e_date+'">';
    insert+='<input type="hidden" name="item" value="'+instrument_type+'-'+instrument_name+'-'+instrument_no+'">';
    insert+='<input type="hidden" name="uom" value="'+uom+'">';
    insert+='</tr>';

    $('#view_instrument').append(insert);


    // var  i_name=$('#add_instrument_name').val('');
    $('#add_instrument_no').val('');
    $('#add_edate').val('');
    $('#add_cali_date').val('');
    $('#add_purchased').val('');
    $('#add_calibrator_type').attr('disabled',true);

   //  $('#add_instrument_type').val('');
   //  $('#instrument_type_div').html('<label>Instrument Type</label><select class="form-control filter" id="add_instrument_type" name="add_instrument_type" data-col="Instrument Type" notEqual="---" data-live-search="true"></select>');
   //  // on_change_instrument_type();
    
   //  $('#add_instrument_name').val('');
   //   $('#instrument_name_div').html('<label>Instrument Name</label><select class="form-control filter" id="add_instrument_name" name="add_instrument_name" notEqual="---" data-live-search="true"></select>');
   // on_change_calibrator_type();
}

function validate_due_expected_date(){
  var add_calibrator_type_id=$('#add_calibrator_type').children(':selected').attr('id');
  var d_length=$("input[type='checkbox'][name='due_check']:checked").length;

var flag=0;

if(Number(add_calibrator_type_id)>0)
{
  if(Number(d_length)>0)
  {
    $('#view_due_instrument> tr').each(function(){
    var x=$(this).find('input').serializeArray();
     console.log(x);
     if(x[0]['value']=='on'){
       if(x[1]['value']>'0'){
         if(Number(add_calibrator_type_id)!=Number(x[8]['value']))
               {
                 flag=0;
                 $('#issue_calibrator_type_error').fadeIn(200).delay(5000).fadeOut(1000);
                 return flag;
               }else{
                flag=1;
               }
       }
       else{
        flag=0;
      $('#expected-date').fadeIn(200).delay(5000).fadeOut(1000);
      $(this).css('background','#d43f3a');
       }
     }
     });
  }else{
    $('#due_select_pending_instrument').fadeIn(200).delay(5000).fadeOut(1000);
  }
  
}else{
  $('#select_calibrator_type').fadeIn(200).delay(5000).fadeOut(1000);
}
  

  return flag;

}

  function due_validate_instrument(){
    var flag=1;
      $('#view_due_instrument> tr').each(function(){
      var x=$(this).find('input').serializeArray();

       if(x[0]['value']=='on'){
        var instrument_id=x[2]['value'];
           for (var i = 0; i < instrument_array.length; i++) {
      if(instrument_id==instrument_array[i]){
         $(this).css('background','red');
$('#instrument-already-enterd').fadeIn(200).delay(5000).fadeOut(1000);
      flag=0;
     
      }
   
    };
       }
     });
      return flag;
   }

   function validate_pending_expected_date(){
    var calibrator_type_id=$('#add_calibrator_type').children(':selected').attr('id');
    var p_length=$("input[type='checkbox'][name='pending_check']:checked").length;
    var flag=0;
    if(Number(calibrator_type_id)>0)
    {
      if(Number(p_length)>0)
      {
         $('#view_pending_instrument> tr').each(function(){
          var x=$(this).find('input').serializeArray();
           console.log(x);
           if(x[0]['value']=='on'){
             if(x[1]['value']>'0'){

               if(Number(calibrator_type_id)!=Number(x[8]['value']))
               {
                 flag=0;
                 $('#issue_calibrator_type_error').fadeIn(200).delay(5000).fadeOut(1000);
                 return flag;
               }else{
                flag=1;
               }
               
             }
             else{
              flag=0;
            $('#expected-date').fadeIn(200).delay(5000).fadeOut(1000);
            $(this).css('background','#d43f3a');
             }
           }
           });
      }else{
        $('#select_pending_instrument').fadeIn(200).delay(5000).fadeOut(1000);
      }
      
    }else{
      $('#select_calibrator_type').fadeIn(200).delay(5000).fadeOut(1000);
    }
  

  return flag;

}

  function pending_validate_instrument(){
    var flag=1;
      $('#view_pending_instrument> tr').each(function(){
      var x=$(this).find('input').serializeArray();

       if(x[0]['value']=='on'){
        var instrument_id=x[2]['value'];
           for (var i = 0; i < instrument_array.length; i++) {
      if(instrument_id==instrument_array[i]){
         $(this).css('background','red');
$('#instrument-already-enterd').fadeIn(200).delay(5000).fadeOut(1000);
      flag=0;
     
      }
   
    };
       }
     });
      return flag;
   }

   function load_pending(){
    console.log("pending");
    load_pending_instrument('pending');
     /* var check=check_calibration_type();
  if(Number(check)=='1'){
  var calintator_id=$('#add_calibrator_type').children(':selected').attr('id');
  load_pending_instrument(calintator_id,'pending');
}
else{
 $('#select-calibrator').fadeIn(200).delay(5000).fadeOut(1000);
}*/

   }
   function load_due(){
    load_due_instrument(1,'due');
     // var check=check_calibration_type();
/*  if(Number(check)=='1'){
  var calintator_id=$('#add_calibrator_type').children(':selected').attr('id');
  load_due_instrument(calintator_id,'due');
}
else{
 $('#select-calibrator').fadeIn(200).delay(5000).fadeOut(1000);
}*/

   }