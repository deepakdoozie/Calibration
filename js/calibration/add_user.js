 var filename=[];
$(function(){
  var login_by=$.cookie('i');
  load_designation();
  load_all();

  $('#add-btn').click(function(){
  	$('#table-box').hide();
  	$('#search-box').hide();
  	$('#add-box').show();
    load_all_roles();
  });

  $('#save-btn').click(function(){

  	if($('#add-form').valid()){
    var allow=1;
  	var designation=$('#designation_add').children(':selected').attr('id');
   if(designation != '-1'){
    var flag=$('#update_flag').val();
     console.log(flag);
      var image_path=$('#document').val();
    if(Number(flag) !=1){
      var x=confirm('Do you want to save without user image...?')
      if(x){
        flag=1;
        var image_path='';
      }
    }
    if(Number(flag)==1){
   var userName=$('#user_name_add').val();
   var loginName=$('#login_name_add').val();
   var password=hex_md5($('#password_add').val());
   var rePassword=hex_md5($('#re_password_add').val());
   if(password == rePassword){

  var length=$("input[type='checkbox'][name='role_check']:checked").length;
 if(Number(length)>0){
    var operation='saveUser';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/add_user.php",
    data:{operation:operation,loginName:loginName,password:password,designation:designation,userName:userName,login_by:login_by,image_path:image_path}
   }).done(function(result){
   
      $("#view_all_role > tr").each(function(){
    var x=$(this).find('input').serializeArray();
    if(x.length>0)
    if(x[0]['value']=='on'){
       var operation='saveUserRole';
         $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/add_user.php",
    data:{operation:operation,id:result,role_id:x[1]['value']},
    async:false
   }).done(function(result){
   });

    }
  });
        $('#add-success').fadeIn(200).delay(5000).fadeOut(1000);
    add_clear();
   });
 }else{
 $('#select-role').fadeIn(200).delay(5000).fadeOut(1000);
 }
 }
else{
$('#select_same_password').fadeIn(200).delay(5000).fadeOut(1000);
}
}

   }
   else{
$('#select-designation').fadeIn(200).delay(5000).fadeOut(1000);
   }

  	}
  });

  $('#active_btn').click(function(){
 	var id=$("input[type='radio'][name='ch']:checked").attr('id');
 	if(id>0){
 		var x=confirm('Do you want to reaaly Active/Deactive user...?')
      if(x){
      	 var operation='activeDeactiveUser';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/add_user.php",
    data:{operation:operation,id:id}
   }).done(function(result){
   	load_all();
  $('#delete-success').fadeIn(200).delay(5000).fadeOut(1000);


   });

      }
 	}
 	else{
      $('#select-entry').fadeIn(200).delay(5000).fadeOut(1000);
  }
 });
   $('#clear_btn').click(function(){
   	$('#designation_search').val('');
   	$('#s_lname').val('');
   	load_all();
   	load_designation();

   });

     $('#cancel_add').click(function(){
   	add_clear();

   });
      $('#image-upload-btn').click(function(){
     $('#image-upload-btn').hide();
        $('#upload-img-shw').show();
       });

     var uploader = new plupload.Uploader({
                runtimes : 'html5,flash,silverlight', // Set runtimes, here it will use HTML5, if not supported will use flash, etc.
                browse_button : 'pickfiles', // The id on the select files button
                multi_selection: false, // Allow to select one file each time
                container : 'uploader', // The id of the upload form container
                max_file_size : '10Mb', // Maximum file size allowed
                url : '../php/user_image_upload.php', // The url to the upload.php file
                flash_swf_url : 'js/plupload.flash.swf', // The url to thye flash file
                silverlight_xap_url : 'js/plupload.silverlight.xap', // The url to the silverlight file
                filters : [
                                        {title : "All files", extensions : "jpg,gif,tiff,png,psd,php,htm,html,css,zip,rar,tar,gzip,txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv,mp3,ogg,wav,mov,mp4,f4v,flv"},
                                        {title : "Image files", extensions : "jpg,gif,tiff,png,psd"},
                                        {title : "Web files", extensions : "php,htm,html,css"},
                                        {title : "Archive files", extensions : "zip,rar,tar,gzip"},
                                        {title : "Document files", extensions : "txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv"},
                                        
                                                 ] // Filter the files that will be showed on the select files window
            });

            // RUNTIME
            uploader.bind('Init', function(up, params) {
                $('#runtime').text(params.runtime);
            });

            // Start Upload ////////////////////////////////////////////
            // When the button with the id "#uploadfiles" is clicked the upload will start
            $('#uploadfiles').click(function(e) {
                uploader.start();
                e.preventDefault();
                $('#update_flag').val(1);
                
            });

            uploader.init(); // Initializes the Uploader instance and adds internal event listeners.

            // Selected Files //////////////////////////////////////////
            // When the user select a file it wiil append one div with the class "addedFile" and a unique id to the "#filelist" div.
            // This appended div will contain the file name and a remove button
            uploader.bind('FilesAdded', function(up, files) {
                $.each(files, function(i, file) {
                    $('#filelist').append('<div class="addedFile" id="' + file.id + '">' + file.name + '<a href="#" id="' + file.id + '" class="removeFile"></a>' + '</div>');
                file=file.name;
                $('#document').val(file);
               
                });
                up.refresh(); // Reposition Flash/Silverlight
            });

            // Error Alert /////////////////////////////////////////////
            // If an error occurs an alert window will popup with the error code and error message.
            // Ex: when a user adds a file with now allowed extension
            uploader.bind('Error', function(up, err) {
                alert("Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "");
                up.refresh(); // Reposition Flash/Silverlight
            });

            

            // Progress bar ////////////////////////////////////////////
            // Add the progress bar when the upload starts
            // Append the tooltip with the current percentage
            uploader.bind('UploadProgress', function(up, file) {
              
                var progressBarValue = up.total.percent;
                
                $('#progressbar').fadeIn().progressbar({
                    value: progressBarValue
                 

                });
                $('#progressbar .ui-progressbar-value').html('<span class="progressTooltip">' + up.total.percent + '%</span>');
            });

             uploader.bind('FileUploaded', function(up, file, info) {
              var obj = JSON.parse(info.response);
             
              filename.push(file.name);
              console.log(filename);
             
            });




});

function load_designation(){
	 var operation='getAllDesignation';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/add_user.php",
    data:{operation:operation}
   }).done(function(result){
      var dis='<option id="-1">---</option>'
      for (var i = 0; i < result.length; i++) {
        dis+='<option id='+result[i]['uid']+'>'+result[i]['designation']+'</option>'
      };
      $('#designation_add').html(dis);
      $('#designation_search').html(dis);
    });
}

function load_all(){

	  var operation='loadAllUser';
    $.ajax({
        type: "POST",
    url: "../php/ajaxInterface/add_user.php",
    data:{operation:operation}
   }).done(function(result){
   	var dis='';
   	 for(i=0;i<result.length;i++){
         dis+='<tr>';
         dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";
         dis+="<td>"+result[i]['user_name']+"</td>"
        dis+="<td>"+result[i]['name']+"</td>"
        dis+="<td>"+result[i]['designation']+"</td>"
        if(result[i]['active_flag']==1){
       dis+="<td style='color:green'>"+'Active'+"</td>"
        }
        else{
    dis+="<td style='color:red'>"+'DeActive'+"</td>"
        }
      dis+='</tr>';
  }
      $('#view').html(dis);
       $('.filter').multifilter({
        'target' : $('#myTable1')
        });

   });
}

function add_clear(){
  $('#upload_flag').val(0);
   $('#document').val(0);

   $('#progressbar').html('');
    $('#pbar').html('');
     $('#filename').html('');
     $('#filelist').html('');
  $('#upload-img-shw').hide();
  $('#image-upload-btn').show();
  $('#table-box').show();
    $('#search-box').show();
    $('#add-box').hide();
    $('#add-form')[0].reset();
     $('#designation_add').html('');
     load_all();
     load_designation();
       filename=[];

}

function load_all_roles(){
      var operation='getAllRoles';
      $.ajax({
          type: "POST",
      url: "../php/ajaxInterface/add_user.php",
      data:{operation:operation}
     }).done(function(result){
      console.log(result);
           var dis='';

        for(i=0;i<result.length;i++){
           dis+='<tr>';
           dis+="<td  style='width:10px;'><input type='checkbox' class='flat-red' name='role_check' id="+result[i]['uid']+"><input type='hidden' name='uid' value="+result[i]['uid']+"></td>";
          dis+="<td>"+result[i]['role']+"</td>"
           dis+='</tr>';
  }
        $('#view_all_role').html(dis);
        

  });
}