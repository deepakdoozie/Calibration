$(function(){
	load_all();
	$('#export_excel').click(function(e){
       
      var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "-" + month + "-" + year + "_" + hour + ":" + mins;
        var a = document.createElement('a');
        var data_type = 'data:application/issued_instrument_report.ms-excel';
        var table_div = document.getElementById('show_tbl_row');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'issued_instrument_report' + postfix + '.xls';
        a.click();
        e.preventDefault();
         
    })

    $('#clear_btn').click(function(){
    	$('#s_cali_type').val('');
        $('#s_type').val('');
    	$('#s_iname').val('');
    	$('#s_ino').val('');
    	load_all();
    });

});

function load_all(){

        var operation='loadAllIssuedInstrument';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/issue_report.php",
    data:{operation:operation}
   }).done(function(result){

     var date=new Date();
      var today=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate());
      var dis='';

      for(i=0;i<result.length;i++){
        var expected_date=new Date(result[i]['expected_date']);
        if(expected_date>date){
         var date1 = new Date(result[i]['expected_date']);
        var date2 = new Date();
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
         console.log(diffDays);
        dis+='<tr>';
        dis+="<td>"+result[i]['calibrator_type_name']+"</td>"
        dis+="<td>"+result[i]['instrument_type_name']+"</td>"
        dis+="<td>"+result[i]['instrument_name']+"</td>"
        dis+="<td>"+result[i]['instrument_no']+"</td>"
        dis+="<td>"+get_print_format(result[i]['expected_date'])+"</td>"
        dis+="<td>"+diffDays+"</td>"
        dis+='</tr>';
        }
        else if(expected_date<=date){

        var date1 = new Date(result[i]['expected_date']);
        var date2 = new Date();
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
         console.log(diffDays);
        dis+='<tr style="background-color:orange;">';
        dis+="<td>"+result[i]['calibrator_type_name']+"</td>"
        dis+="<td>"+result[i]['instrument_type_name']+"</td>"
        dis+="<td>"+result[i]['instrument_name']+"</td>"
        dis+="<td>"+result[i]['instrument_no']+"</td>"   
        dis+="<td>"+get_print_format(result[i]['expected_date'])+"</td>"
        dis+="<td>"+diffDays+"</td>"
        dis+='</tr>';
        }
         
       
  }

      $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
       
      

});

}