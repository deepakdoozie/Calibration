 var filename=[];
 var oldFileName='';
 var path='';
$(function(){
  var login_by=$.cookie('i');
	load_all();
	load_calibrator_type();
   

  $('#clear_btn').click(function(){
    $('#s_iname').val('');
     $('#s_ino').val('');
    $('#s_itype').val('');
   load_all();

  });


	$('#edit_btn').click(function(){
  var id=$("input[type='radio'][name='ch']:checked").attr('id');
   if(id>0){
    $('#edit_box').show();
    $('#table_box').hide();
    $('#search_box').hide();
     $('#edit_id').val(id);

     $('#calibrator_type').val('');
    $('#ctype_div').html('<label>Calibrator Type</label><select class="form-control required" name="calibrator_type" id="calibrator_type" data-col="Calibrator Type Name" notEqual="---" data-live-search="true"></select>'); 
    $('#calibrator').val('');
    $('#calibrator_div').html('<label>Calibrator </label><select class="form-control required" name="calibrator" id="calibrator"  notEqual="---" data-live-search="true"></select>');
      

    var operation='editSpecificCalibration';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/manage_calibration.php",
    data:{operation:operation,id:id},
    async:false
    }).done(function(result)
    { 

      $('#calibration_details').val(result['calibration_details']);
      $('#calibrator_date').val(result['calibration_date']);
      $('#calibrator_charge').val(result['calibration_charges']);
      $('#other_charge').val(result['other_charges']);
      $('#instrument_name').val(result['instrument_name']);
     $('#add_report_no').val(result['calibration_report_no']);
    $('#add_std').val(result['reference_standard']);
     $('#add_code').val(result['punch_code']);
    $('#add_remark').val(result['remarks']); 

  $('#instrument_no').val(result['instrument_no']);
  $('#instrument_type').val(result['instrument_type_name']);

    
   oldFileName=result['calibration_certificate_path'];
    
 });
}
else{
$('#select-task-entry').fadeIn(200).delay(5000).fadeOut(1000);

}
});



$('#image-upload-btn').click(function(){
     $('#image-upload-btn').hide();
        $('#upload-img-shw').show();
       });

       var uploader = new plupload.Uploader({
                runtimes : 'html5,flash,silverlight', // Set runtimes, here it will use HTML5, if not supported will use flash, etc.
                browse_button : 'pickfiles', // The id on the select files button
                multi_selection: false, // Allow to select one file each time
                container : 'uploader', // The id of the upload form container
                max_file_size : '10Mb', // Maximum file size allowed
                url : '../php/upload.php', // The url to the upload.php file
                flash_swf_url : 'js/plupload.flash.swf', // The url to thye flash file
                silverlight_xap_url : 'js/plupload.silverlight.xap', // The url to the silverlight file
                filters : [
                                        {title : "All files", extensions : "jpg,gif,tiff,png,psd,php,htm,html,css,zip,rar,tar,gzip,txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv,mp3,ogg,wav,mov,mp4,f4v,flv"},
                                        {title : "Image files", extensions : "jpg,gif,tiff,png,psd"},
                                        {title : "Web files", extensions : "php,htm,html,css"},
                                        {title : "Archive files", extensions : "zip,rar,tar,gzip"},
                                        {title : "Document files", extensions : "txt,doc,docx,xls,xlsx,ppt,pptx,pps,odt,ods,odp,sxw,sxc,sxi,wpd,pdf,rtf,csv,tsv"},
                                        
                                                 ]
            });

            // RUNTIME
            uploader.bind('Init', function(up, params) {
                $('#runtime').text(params.runtime);
            });

            // Start Upload ////////////////////////////////////////////
            // When the button with the id "#uploadfiles" is clicked the upload will start
            $('#uploadfiles').click(function(e) {
                uploader.start();
                e.preventDefault();
                $('#update_flag').val(1);
            });

            uploader.init(); // Initializes the Uploader instance and adds internal event listeners.

            // Selected Files //////////////////////////////////////////
            // When the user select a file it wiil append one div with the class "addedFile" and a unique id to the "#filelist" div.
            // This appended div will contain the file name and a remove button
            uploader.bind('FilesAdded', function(up, files) {
                $.each(files, function(i, file) {
                    $('#filelist').append('<div class="addedFile" id="' + file.id + '">' + file.name + '<a href="#" id="' + file.id + '" class="removeFile"></a>' + '</div>');
                file=file.name;
                $('#document').val(file);
            
                });
                up.refresh(); // Reposition Flash/Silverlight
            });

            // Error Alert /////////////////////////////////////////////
            // If an error occurs an alert window will popup with the error code and error message.
            // Ex: when a user adds a file with now allowed extension
            uploader.bind('Error', function(up, err) {
                alert("Error: " + err.code + ", Message: " + err.message + (err.file ? ", File: " + err.file.name : "") + "");
                up.refresh(); // Reposition Flash/Silverlight
            });

            

            // Progress bar ////////////////////////////////////////////
            // Add the progress bar when the upload starts
            // Append the tooltip with the current percentage
            uploader.bind('UploadProgress', function(up, file) {
                var progressBarValue = up.total.percent;
                $('#progressbar').fadeIn().progressbar({
                    value: progressBarValue
                });
                $('#progressbar .ui-progressbar-value').html('<span class="progressTooltip">' + up.total.percent + '%</span>');
            });

             uploader.bind('FileUploaded', function(up, file, info) {
              var obj = JSON.parse(info.response);
             
              filename.push(file.name);
           
            });

$('#update_btn').click(function(){
	var x=confirm('Do you want to really update...?')
  if(x){
  	if($('#edit_form').valid()){
  		var id=$('#edit_id').val();
    var calibrationDetails=$('#calibration_details').val();
    var nextCalibrationDate=$('#calibrator_date').val();
     var calibrationCharges=$('#calibrator_charge').val();
      var otherCharges=$('#other_charge').val();
     
      var reportNo=$('#add_report_no').val();
    var reffStd=$('#add_std').val();
     var code=$('#add_code').val();
    var remark=$('#add_remark').val(); 

     var file=$('#document').val();
     var updateFlag=$('#update_flag').val();
    

         if(updateFlag==''){
      y=confirm('Do you want to really add without documents...?')
         if(y){
          updateFlag=1;
          file=oldFileName;
         }

    }
    if(updateFlag=='1'){

     var operation='updateCalibrationDetails';
         $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/manage_calibration.php",
    data:{operation:operation,id:id,calibrationDetails:calibrationDetails,nextCalibrationDate:nextCalibrationDate,calibrationCharges:calibrationCharges,otherCharges:otherCharges,file:file,reportNo:reportNo,reffStd:reffStd,code:code,remark:remark,login_by:login_by}
   }).done(function(result){
      
        if(result){
            $('#update-task-success').fadeIn(200).delay(5000).fadeOut(1000);
             load_all();
          $('#edit_form')[0].reset();
          $('#edit_box').hide();
          $('#table_box').show();
          $('#search_box').show();
          $('#image-upload-btn').show();
          $('#upload-img-shw').hide();
          $('#document').val('');
           $('#update_flag').val('');
            $('#edit_id').val('');
            $('#calibrator_type').val('');
             $('#ctype_div').html('<label>Calibrator Type</label><select class="form-control required" name="calibrator_type" id="calibrator_type" data-col="Calibrator Type Name" notEqual="---" data-live-search="true"></select>'); 
             $('#calibrator').val('');
             $('#calibrator_div').html('<label>Calibrator </label><select class="form-control required" name="calibrator" id="calibrator"  notEqual="---" data-live-search="true"></select>');
            
              $('#pbar').html('');
               $('#progressbar').html('');
                 $('#filelist').html('');
   OnChangeCalibratorType();
   $('#document').val('');
              $('#update_flag').val('');

        }
   });
}
else{
 $('#select-calibrator').fadeIn(200).delay(5000).fadeOut(1000);
}
      }	
}


});

 $('#edit_cancel_btn').click(function(){
      load_all();
          $('#edit_form')[0].reset();
          $('#edit_box').hide();
          $('#table_box').show();
          $('#search_box').show();
          $('#image-upload-btn').show();
          $('#upload-img-shw').hide();
          $('#document').val('');
           $('#update_flag').val('');
            $('#edit_id').val('');
            $('#calibrator_type').val('');
             $('#ctype_div').html('<label>Calibrator Type</label><select class="form-control required" name="calibrator_type" id="calibrator_type" data-col="Calibrator Type Name" notEqual="---" data-live-search="true"></select>'); 
             $('#calibrator').val('');
             $('#calibrator_div').html('<label>Calibrator </label><select class="form-control required" name="calibrator" id="calibrator"  notEqual="---" data-live-search="true"></select>');
            
              $('#pbar').html('');
               $('#progressbar').html('');
                 $('#filelist').html('');
   OnChangeCalibratorType();
   $('#document').val('');
              $('#update_flag').val('');
 });


});

function load_all(){
    var operation='loadCalibrationDetails';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/manage_calibration.php",
    data:{operation:operation}
   }).done(function(result){

   	 path=result;
      var dis='';
        
      for(i=0;i<result.length;i++){
         dis+='<tr>';
        dis+="<td  style='width:10px;'><input type='radio' class='flat-red' name='ch' id="+result[i]['uid']+"></td>";

        dis+="<td>"+result[i]['instrument_type_name']+"</td>";
        dis+="<td>"+result[i]['instrument_name']+"</td>"
        dis+="<td>"+result[i]['instrument_no']+"</td>"
        dis+="<td>"+get_print_format(result[i]['calibration_date'])+"</td>"
        if(result[i]['calibration_certificate_path']=='')
        {
          dis+="<td>---</td>"
        }else{
          dis+="<td><button id="+result[i]['uid']+" name="+result[i]['calibration_certificate_path']+" onclick=show_document("+i+") > <a target='_blank' href='../documents/"+result[i]['calibration_certificate_path']+"' >View</button></td>"
        }
        

      dis+='</tr>';
  
}
      $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
       
      

});
}

function load_calibrator_type(){
	  var operation='loadAllcalibratorType';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/manage_calibration.php",
    data:{operation:operation}
   }).done(function(result1){
   	var dis='<option>---</option>';
      for(i=0;i<result1.length;i++){  

         dis+='<option id='+result1[i]['uid']+'>'+result1[i]['calibrator_type_name']+'</option>';
  
}
      $('#s_itype').html(dis).selectpicker();
    
});
}

function show_document(index){
  var path2=path[index]['calibration_certificate_path'];
  var sourcePath="../../documents/"+path2+"";
 
    $('#document_show').attr('href','../../documents/'+path2+'');

}

function OnChangeCalibratorType(){
   $('#calibrator_type').on('change',function(){
$('#calibrator').val('');
  var calibratorType=$('#calibrator_type').val();
   var calibratorTypeId=$('#calibrator_type').children(':selected').attr('id');
   
   if(calibratorType != '---'){
       $('#calibrator').val('');
      $('#calibrator_div').html('<label>Calibrator </label><select class="form-control required" name="calibrator" id="calibrator"  notEqual="---" data-live-search="true"></select>');

    var operation='loadCalibrator';
      $.ajax({
    type: "POST",
    url: "../php/ajaxInterface/manage_calibration.php",
    data:{operation:operation,calibratorTypeId:calibratorTypeId}
    }).done(function(result)
    { 
  
       var dis='';
          
        dis+='<option id>---</option>';
      for(i=0;i<result.length;i++){    
         dis+='<option id='+result[i]['uid']+'>'+result[i]['calibrator_name']+'</option>';
  }
      $('#calibrator').html(dis).selectpicker();
    });

   }else{
    
    $('#calibrator').val('');

    $('#calibrator_div').html('<label>Calibrator </label><select class="form-control required" name="calibrator" id="calibrator"  notEqual="---" data-live-search="true"></select>');
    

   }
 });
}