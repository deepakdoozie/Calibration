$(function(){
	load_all();

	$('#export_excel').click(function(e){
       
      var dt = new Date();
        var day = dt.getDate();
        var month = dt.getMonth() + 1;
        var year = dt.getFullYear();
        var hour = dt.getHours();
        var mins = dt.getMinutes();
        var postfix = day + "-" + month + "-" + year + "_" + hour + ":" + mins;
        var a = document.createElement('a');
        var data_type = 'data:application/calibration_due_report.ms-excel';
        var table_div = document.getElementById('show_tbl_row');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');
        a.href = data_type + ', ' + table_html;
        a.download = 'calibration_due_report_' + postfix + '.xls';
        a.click();
        e.preventDefault();
         
    })

    $('#clear_btn').click(function(){
    	$('#s_itype').val('');
    	$('#s_iname').val('');
    	$('#s_ino').val('');
    	load_all();

    });

});
function load_all(){
         
      var operation='loadAllDueInstrumentCalibration';
    $.ajax({
        type: "POST",
      url: "../php/ajaxInterface/calibration_due_report.php",
    data:{operation:operation}
   }).done(function(result1){
 console.log(result1);

  var date=new Date();
      var today=date.getFullYear()+'-'+(date.getMonth()+1)+'-'+(date.getDate());
var dis='';
     for(j=0;j<result1.length;j++){
  var calibrationDate=result1[j]['first_calibration_date'];
   console.log(calibrationDate,"calibrationDate");
   var m=moment(calibrationDate,'YYYY-MM-DD');
   console.log(m,"m");
   var dif=result1[j]['calibration_frequency'];
  var calibrationPeriods=dif;
  console.log(dif,"dif");
  var newdate=moment(m).add(calibrationPeriods,'days');
  console.log(newdate,"newdate");
  var calibrationpending=moment(newdate).toDate();
  var date = moment(newdate).format('YYYY-MM-DD');
  
  var diffDate=new Date(today)-new Date(date);
  // var diffDate=new Date(date)-new Date(today);
console.log(today,"today");
console.log(date,"date");
console.log(diffDate,"diffDate");
if(diffDate>0){
  var calibrationDate=result1[j]['first_calibration_date'];
       var m=moment(calibrationDate,'YYYY-MM-DD');
       var newdate=moment(m).add(result1[j]['calibration_frequency'],'days');
       var calibrationpending=moment(newdate).toDate();
      var date = moment(calibrationpending).format('YYYY-MM-DD');
         dis+='<tr>';
        dis+="<td>"+result1[j]['instrument_type_name']+"</td>";
        dis+="<td>"+result1[j]['instrument_name']+"</td>"
        dis+="<td>"+result1[j]['instrument_no']+"</td>"
        dis+="<td>"+result1[j]['purchesed_by']+"</td>" 
        dis+="<td>"+get_print_format(result1[j]['purchesed_date'])+"</td>"
        dis+="<td>"+get_print_format(date)+"</td>"
      dis+='</tr>';
  }
}

      $('#view').html(dis);
       $('.filter').multifilter({
          'target' : $('#show_tbl_row')
        });
       
      
});

}