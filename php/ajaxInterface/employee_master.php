<?php
include_once('../classes/employee_master.php');

if($_REQUEST["operation"]=="addEmployee")
  {
    $check=employeeDetails::checkEmployeeID($_POST['add_eid']);
    if($check>0){
      echo 0;
    }else{
    $obj = new employeeData($_POST['add_eid'],$_POST['add_fname'],$_POST['add_mname'],$_POST['add_lname'],$_POST['add_dob'],$_POST['add_doj'],$_POST['add_desination'],$_POST['add_department'],$_POST['add_esi'],$_POST['add_pf'],$_POST['add_ecno'],$_POST['add_eecno'],$_POST['add_eeid'],$_POST['add_ebld'],$_POST['add_eadrs'],$_POST['add_epadrs'],$_POST['add_bank_name'],$_POST['add_acc_no'],$_POST['add_ifsc_code'],$_POST['add_pan_no'],$_POST['payment_id'],$_POST['emply_typ_id'],$_POST['login_by']);
      $response=employeeDetails::addEmployee($obj);
     header('Content-type: application/json');
    
    echo 1;
  }
  }


  if($_REQUEST["operation"]=="updateEmply")
  {
    $check=employeeDetails::checkEmployeeIDForUpdate($_POST['update_id'],$_POST['edit_eid']);
    if($check){
      echo 0;
      
    }else{
      $obj = new editEmployeeData($_POST['edit_eid'],$_POST['edit_fname'],$_POST['edit_mname'],$_POST['edit_lname'],$_POST['edit_dob'],$_POST['edit_doj'],$_POST['edit_desination'],$_POST['edit_department'],$_POST['edit_esi'],$_POST['edit_pf'],$_POST['edit_ecno'],$_POST['edit_eecno'],$_POST['edit_eeid'],$_POST['edit_ebld'],$_POST['edit_eadrs'],$_POST['edit_epadrs'],$_POST['edit_bank_name'],$_POST['edit_acc_no'],$_POST['edit_ifsc_code'],$_POST['edit_pan_no'],$_POST['payment_id'],$_POST['emply_typ_id'],$_POST['update_id'],$_POST['login_by']);
      $response=employeeDetails::updateEmply($obj);
     header('Content-type: application/json');
    echo 1;

  }
  }

  if($_REQUEST["operation"]=="getAllEmployee")
  {
  	$data  =employeeDetails::getAllEmployee();
   header('Content-type: application/json');
     echo json_encode($data);
  }

  if($_REQUEST["operation"]=="loadEmploymentType")
  {
    $data  =employeeDetails::loadEmploymentType();
   header('Content-type: application/json');
     echo json_encode($data);
  }

  if($_REQUEST["operation"]=="loadModeOfPaymentType")
  {
    $data  =employeeDetails::loadModeOfPaymentType();
   header('Content-type: application/json');
     echo json_encode($data);
  }

  if($_REQUEST["operation"]=="deleteEmply")
  {
    $data=employeeDetails::checkEmployeeFlag($_POST['id']);
     // echo($data);
    if($data==0){
     $data1  =employeeDetails::updateFlag1($_POST['id']);
   header('Content-type: application/json');
     echo json_encode($data1);
    }
    else{
    $data2  =employeeDetails::updateFlag0($_POST['id']);
   header('Content-type: application/json');
     echo json_encode($data2);
   }
  }

  if($_REQUEST["operation"]=="viewEmply")
  {
    $data  =employeeDetails::viewEmply($_POST['id']);
   header('Content-type: application/json');
     echo json_encode($data);
  }
  if($_REQUEST["operation"]=="editEmply")
  {
    $data  =employeeDetails::editEmply($_POST['id']);
   header('Content-type: application/json');
     echo json_encode($data);
  }

  // if ($_REQUEST["operation"]=="updateEmply")
  //  {

  //   $response=employeeDetails::updateEmply($_POST['id'],$_POST['eid'],$_POST['ename'],$_POST['edob'],$_POST['edoj'],$_POST['ecno'],$_POST['eecno'],$_POST['email'],$_POST['ebld'],$_POST['econAdrs'],$_POST['epemAdrs']);
  //    header('Content-type: application/json');
  //     echo json_encode($response);
  // }

  if ($_REQUEST["operation"]=="loadname")
   {

    $response=employeeDetails::loadname();
     header('Content-type: application/json');
      echo json_encode($response);
  }

  if ($_REQUEST["operation"]=="searchEmplyName")
   {

    $response=employeeDetails::searchEmplyName($_POST['ename'],$_POST['date']);
     header('Content-type: application/json');
      echo json_encode($response);
  }
?>