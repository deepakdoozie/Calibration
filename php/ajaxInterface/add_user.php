<?php
include_once('../classes/add_user.php');

if($_REQUEST["operation"]=="getAllDesignation")
  {
      $response=addUser::getAllDesignation();
     header('Content-type: application/json');  
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="saveUser")
  {
      $response=addUser::saveUser($_POST['loginName'],$_POST['password'],$_POST['designation'],$_POST['userName'],$_POST['login_by'],$_POST['image_path']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="loadAllUser")
  {
      $response=addUser::loadAllUser();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }


    if($_REQUEST["operation"]=="activeDeactiveUser")
  {
      $response=addUser::activeDeactiveUser($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

      if($_REQUEST["operation"]=="getAllRoles")
  {
      $response=addUser::getAllRoles();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

        if($_REQUEST["operation"]=="saveUserRole")
  {
      $response=addUser::saveUserRole($_POST['id'],$_POST['role_id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }