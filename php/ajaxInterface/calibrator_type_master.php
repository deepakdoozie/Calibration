<?php
include_once('../classes/calibrator_type_master.php');

if($_REQUEST["operation"]=="savecalibratorType")
  {
    $check=calibratorDetails::checkCalibratorTypeForAdd($_POST['cname']);
  if($check){
    echo 0;
  }
  else{
  $response=calibratorDetails::savecalibratorType($_POST['cname'],$_POST['description'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }
  }

  if($_REQUEST["operation"]=="getAllcalibratorType")
  {
    
      $response=calibratorDetails::getAllcalibratorType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="loadAllcalibratorType")
  {
    
      $response=calibratorDetails::loadAllcalibratorType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="viewSpecificcalibratorType")
  {
    
      $response=calibratorDetails::viewSpecificcalibratorType($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="editSpecificcalibratorType")
  {
    
      $response=calibratorDetails::editSpecificcalibratorType($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="updatecalibratorType")
  {
    $check=calibratorDetails::checkCalibratorTypeForUpdate($_POST['id'],$_POST['cname']);
  
    if($check){
      echo 0;
    }
    else{
      $response=calibratorDetails::updatecalibratorType($_POST['id'],$_POST['cname'],$_POST['description'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }
  }
