<?php
include_once('../classes/calibration_details.php');

if($_REQUEST["operation"]=="loadAllCalibration")
  {
  $response=calibrationDetails::loadAllCalibration();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="loadAllInstrumentCalibration")
  {
  $response=calibrationDetails::loadAllInstrumentCalibration();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

if($_REQUEST["operation"]=="viewSpecificInstrument")
  {
    
      $response=calibrationDetails::viewSpecificInstrument($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="loadAllcalibratorType")
  {
    
      $response=calibrationDetails::loadAllcalibratorType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="saveCalibrationDetails")
  {
    
      $response=calibrationDetails::saveCalibrationDetails($_POST['id'],$_POST['calibrationDetails'],$_POST['nextCalibrationDate'],$_POST['calibrationCharges'],$_POST['otherCharges'],$_POST['file'],$_POST['reportNo'],$_POST['reffStd'],$_POST['code'],$_POST['remark'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="loadAllInstrumentType")
  {
    
      $response=calibrationDetails::loadAllInstrumentType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

      if($_REQUEST["operation"]=="loadSpecificInstrumentName")
  {
    
      $response=calibrationDetails::loadSpecificInstrumentName($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="loadSelectedInstrumentDetails")
  {
    
$response=calibrationDetails::loadSelectedInstrumentDetails($_POST['instrument_id'],$_POST['nameId']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="loadCalibrator")
  {
    
$response=calibrationDetails::loadCalibrator($_POST['calibratorTypeId']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }