<?php
include_once('../classes/issue_instrument.php');

if($_REQUEST["operation"]=="loadAllCalibratorType")
  {
  $response=issueInstrumentDetails::loadAllCalibratorType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }


if($_REQUEST["operation"]=="loadSpecificCalibrator")
  {
  $response=issueInstrumentDetails::loadSpecificCalibrator($_POST['calibrator_type_id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="loadSpecificCalibratorInstrument")
  {
  $response=issueInstrumentDetails::loadSpecificCalibratorInstrument($_POST['calibrator_id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

if($_REQUEST["operation"]=="loadSpecificInstrumentDetails")
  {
  $response=issueInstrumentDetails::loadSpecificInstrumentDetails($_POST['instrument_type_id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }


  if($_REQUEST["operation"]=="loadSpecificInstrumentName")
  {
  $response=issueInstrumentDetails::loadSpecificInstrumentName($_POST['instrument_name_id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="saveDC")
  {
  $response=issueInstrumentDetails::saveDC($_POST['refStd'],$_POST['note'],$_POST['transport_id'],$_POST['login_by'],$_POST['add_calibrator_type']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

      if($_REQUEST["operation"]=="saveDCItems")
  {
  $response=issueInstrumentDetails::saveDCItems($_POST['dc_id'],$_POST['instrument_name_id'],$_POST['e_date']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="loadALLDC")
  {
  $response=issueInstrumentDetails::loadALLDC();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="viewDCItems")
  {
  $response=issueInstrumentDetails::viewDCItems($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="loadALLDCItems")
  {
  $response=issueInstrumentDetails::loadALLDCItems($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="loadALLDCInstrument")
  {
  $response=issueInstrumentDetails::loadALLDCInstrument($_POST['instrument_type_details_id'],$_POST['dc_id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

      if($_REQUEST["operation"]=="loadAllTranport")
  {
  $response=issueInstrumentDetails::loadAllTranport();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="loadAllDueInstrumentCalibration")
  {
  $response=issueInstrumentDetails::loadAllDueInstrumentCalibration($_POST['calibrator_id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }


  if($_REQUEST["operation"]=="loadAllPendingInstrumentCalibration")
  {
  $response=issueInstrumentDetails::loadAllPendingInstrumentCalibration();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }



    if($_REQUEST["operation"]=="loadAllDueCalibration")
  {
  $response=issueInstrumentDetails::loadAllDueCalibration($_POST['calibrator_id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }