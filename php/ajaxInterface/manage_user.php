<?php
include_once('../classes/manage_user.php');

if($_REQUEST["operation"]=="manageUser")
  {
      $response=manageUser::manageUser($_POST['id']);
     header('Content-type: application/json');  
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="updateUser")
  {
      $response=manageUser::updateUser($_POST['loginName'],$_POST['designation'],$_POST['userName'],$_POST['updated_by'],$_POST['id'],$_POST['image_path']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="updateUserPassword")
  {
      $response=manageUser::updateUserPassword($_POST['password'],$_POST['updated_by'],$_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }