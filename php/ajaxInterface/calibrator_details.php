<?php
include_once('../classes/calibrator_details.php');

if($_REQUEST["operation"]=="addCalibratorDetails")
  {
  $response=calibratorDetails::addCalibratorDetails($_POST['calibrator_id'],$_POST['calibrator_name'],$_POST['calibrator_email'],$_POST['calibrator_contact'],$_POST['contact_name'],$_POST['contact_number'],$_POST['additional_contact_number'],$_POST['email'],$_POST['additional_email'],$_POST['credit_days'],$_POST['add_address'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="loadCalibrator")
  {
  $response=calibratorDetails::loadCalibrator();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="saveCalibratorInstrument")
  {
  $response=calibratorDetails::saveCalibratorInstrument($_POST['id'],$_POST['check']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="loadAllcalibratorType")
  {
    
      $response=calibratorDetails::loadAllcalibratorType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="loadAllcalibratorname")
  {
    
      $response=calibratorDetails::loadAllcalibratorname();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="loadALLInstrumentType")
  {
      $response=calibratorDetails::loadALLInstrumentType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

      if($_REQUEST["operation"]=="loadAllCalibratorDetails")
  {
      $response=calibratorDetails::loadAllCalibratorDetails();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

 if($_REQUEST["operation"]=="viewSpecificCalibratorDetails")
  {
    $response=calibratorDetails::viewSpecificCalibratorDetails($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="editSpecificCalibratorDetails")
  {
    $response=calibratorDetails::editSpecificCalibratorDetails($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="updateCalibratorDetails")
  {
    $response=calibratorDetails::updateCalibratorDetails($_POST['calibrator_id'],$_POST['edit_calibrator_name'],$_POST['edit_calibrator_email'],$_POST['edit_calibrator_contact'],$_POST['edit_contact_name'],$_POST['edit_contact_number'],$_POST['edit_additional_contact_number'],$_POST['edit_email'],$_POST['edit_additional_email'],$_POST['edit_credit_days'],$_POST['edit_add_address'],$_POST['id'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="deleteCalibratorInstrument")
  {
  $response=calibratorDetails::deleteCalibratorInstrument($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="updateCalibratorInstrument")
  {
  $response=calibratorDetails::updateCalibratorInstrument($_POST['id'],$_POST['edit_check']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }


  if($_REQUEST["operation"]=="activeDeactiveCalibrator")
  {
$check=calibratorDetails::checkActiveFlag($_POST['id']);
// echo json_encode($check);
if($check=='1'){
  $activeFlag=0;
  $response=calibratorDetails::updateActiveFlag($_POST['id'],$activeFlag);
     header('Content-type: application/json');
    echo json_encode($response);
  }
  else{
$activeFlag=1;
  $response=calibratorDetails::updateActiveFlag($_POST['id'],$activeFlag);
     header('Content-type: application/json');
    echo json_encode($response);
  }
}