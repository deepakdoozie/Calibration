<?php
include_once('../classes/calibration_due_report.php');

if($_REQUEST["operation"]=="loadAllDueCalibration")
  {
  $response=calibrationDueDetails::loadAllDueCalibration();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="loadAllDueInstrumentCalibration")
  {
  $response=calibrationDueDetails::loadAllDueInstrumentCalibration();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }