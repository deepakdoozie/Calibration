<?php
include_once('../classes/employment_type_master.php');

if($_REQUEST["operation"]=="invalidate")
  {
      $response=employeeDetails::invalidate($_POST['type']);
     header('Content-type: application/json');   
    echo json_encode($response);
  }
  if($_REQUEST["operation"]=="invalidate1")
  {
      $response=employeeDetails::invalidate1($_POST['type'],$_POST['id']);
     header('Content-type: application/json');   
    echo json_encode($response);
  }


if($_REQUEST["operation"]=="activate")
  {
      $response=employeeDetails::activate($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="deactivate")
  {
      $response=employeeDetails::deactivate($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

if($_REQUEST["operation"]=="getActivateFlag")
  {
      $response=employeeDetails::getActivateFlag($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="getEmploymentType")
  {
      $response=employeeDetails::getEmploymentType($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

if($_REQUEST["operation"]=="addEmploymentType")
  {
      $response=employeeDetails::addEmploymentType($_POST['type'],$_POST['desc'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="editEmploymentType")
  {
      $response=employeeDetails::editEmploymentType($_POST['type'],$_POST['desc'],$_POST['id'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="getAllEmploymentType")
  {
      $response=employeeDetails::getAllEmploymentType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="loademployment")
  {
      $response=employeeDetails::loademployment();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  ?>