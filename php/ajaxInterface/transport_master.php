<?php
  include_once('../classes/transport_master.php');

   if($_REQUEST["operation"]=="getAllTransportMaster")
   {
    $data  =Transport::getAllTransportMaster();
   
   header('Content-type: application/json');
   echo json_encode($data);
   }

   if($_REQUEST["operation"]=="getSpecificTransportMaster")
  {
   $data  =Transport::getSpecificTransportMaster($_POST['t_id']);
  
  header('Content-type: application/json');
  echo json_encode($data);
  }

  if($_REQUEST["operation"]=="createTransportMaster")
  {
    $obj = new TransportData(0,$_POST['t_type'],$_POST['d_type'],$_POST['desc']); 
    $response=Transport::checkTransportMaster($obj);
    if($response){
      echo 0;
    }else{
      $response=Transport::createTransportMaster($obj);
      echo 1;
    }
  }

  if($_REQUEST["operation"]=="updateTransportMaster")
  {
    $obj = new TransportData($_POST['t_id'],$_POST['t_type'],$_POST['d_type'],$_POST['desc']); 
    $response=Transport::updateTransportMaster($obj);
    echo $response;
  }
?>