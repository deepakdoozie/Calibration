<?php
include_once('../classes/instrument_details.php');


    if($_REQUEST["operation"]=="loadAllInstrumentType")
  {
    
      $response=instrumentDetails::loadAllInstrumentType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

     if($_REQUEST["operation"]=="loadAllInstrumentName")
  {
    
      $response=instrumentDetails::loadAllInstrumentName();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

      if($_REQUEST["operation"]=="loadAllEmployee")
  {
    
      $response=instrumentDetails::loadAllEmployee();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

     if($_REQUEST["operation"]=="saveInstrumentDetails")
  {
      $response=instrumentDetails::saveInstrumentDetails($_POST['instument_id'],$_POST['add_iname'],$_POST['add_ino'],$_POST['add_pname'],$_POST['add_pdate'],$_POST['add_cdate'],$_POST['add_cfreq'],$_POST['add_adays'],$_POST['add_cost'],$_POST['file'],$_POST['add_code'],$_POST['add_remark'],$_POST['add_used'],$_POST['uom'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

       if($_REQUEST["operation"]=="assignEmployeeToInstrument")
  {
    
      $response=instrumentDetails::assignEmployeeToInstrument($_POST['eid'],$_POST['instrument_id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }


      if($_REQUEST["operation"]=="getAllInstrument")
  {
    
      $response=instrumentDetails::getAllInstrument();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }


  
      if($_REQUEST["operation"]=="viewSpecificInstrument")
  {
    
      $response=instrumentDetails::viewSpecificInstrument($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

       if($_REQUEST["operation"]=="editSpecificInstrument")
  {
    
      $response=instrumentDetails::editSpecificInstrument($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="updateInstrumentDetails")
  {
    
      $response=instrumentDetails::updateInstrumentDetails($_POST['instument_id'],$_POST['edit_iname'],$_POST['edit_ino'],$_POST['edit_pname'],$_POST['edit_pdate'],$_POST['edit_cdate'],$_POST['edit_cfreq'],$_POST['edit_adays'],$_POST['edit_cost'],$_POST['uom'],$_POST['file'],$_POST['id'],$_POST['edit_code'],$_POST['edit_remark'],$_POST['edit_used'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="activeDeactiveInstrument")
  {

   $response=instrumentDetails::saveInstrumentFaults($_POST['id'],$_POST['reason']);
     header('Content-type: application/json'); 
    echo json_encode($response);
}

if($_REQUEST["operation"]=="updateAassignEmployeeToInstrument")
  {
    $response=instrumentDetails::updateAassignEmployeeToInstrument($_POST['eid'],$_POST['instrument_id']);
     header('Content-type: application/json');
    echo json_encode($response);
  }

