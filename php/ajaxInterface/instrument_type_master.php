<?php
include_once('../classes/instrument_type_master.php');

if($_REQUEST["operation"]=="saveInstrumentType")
  {
    
      $response=instrumentDetails::saveInstrumentType($_POST['iname'],$_POST['description'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }


  if($_REQUEST["operation"]=="checkForItemExists")
  {
    
      $response=instrumentDetails::checkForItemExists($_POST['iname']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }



  if($_REQUEST["operation"]=="checkForItemUpdate")
  {
    
      $response=instrumentDetails::checkForItemUpdate($_POST['id'],$_POST['iname']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }




  if($_REQUEST["operation"]=="getAllInstrumentType")
  {
    
      $response=instrumentDetails::getAllInstrumentType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="loadAllInstrumentType")
  {
    
      $response=instrumentDetails::loadAllInstrumentType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="viewSpecificInstrumentType")
  {
    
      $response=instrumentDetails::viewSpecificInstrumentType($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="editSpecificInstrumentType")
  {
    
      $response=instrumentDetails::editSpecificInstrumentType($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="updateInstrumentType")
  {
    
      $response=instrumentDetails::updateInstrumentType($_POST['id'],$_POST['iname'],$_POST['description'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }
