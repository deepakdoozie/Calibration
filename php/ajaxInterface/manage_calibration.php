<?php
include_once('../classes/manage_calibration.php');

if($_REQUEST["operation"]=="loadCalibrationDetails")
  {
  $response=calibrationDetails::loadCalibrationDetails();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="editSpecificCalibration")
  {
  $response=calibrationDetails::editSpecificCalibration($_POST['id']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

     if($_REQUEST["operation"]=="loadAllcalibratorType")
  {
    
      $response=calibrationDetails::loadAllcalibratorType();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

   if($_REQUEST["operation"]=="updateCalibrationDetails")
  {
    
      $response=calibrationDetails::updateCalibrationDetails($_POST['id'],$_POST['calibrationDetails'],$_POST['nextCalibrationDate'],$_POST['calibrationCharges'],$_POST['otherCharges'],$_POST['file'],$_POST['reportNo'],$_POST['reffStd'],$_POST['code'],$_POST['remark'],$_POST['login_by']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

    if($_REQUEST["operation"]=="loadCalibrator")
  {
    
$response=calibrationDetails::loadCalibrator($_POST['calibratorTypeId']);
     header('Content-type: application/json');
    
    echo json_encode($response);
  }