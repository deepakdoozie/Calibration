<?php
include_once('../classes/calibration_pending_report.php');

if($_REQUEST["operation"]=="loadAllPendingCalibration")
  {
  $response=calibrationPendingDetails::loadAllPendingCalibration();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }

  if($_REQUEST["operation"]=="loadAllPendingInstrumentCalibration")
  {
  $response=calibrationPendingDetails::loadAllPendingInstrumentCalibration();
     header('Content-type: application/json');
    
    echo json_encode($response);
  }