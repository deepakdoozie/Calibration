<?php
include_once("config.php");

  class printDC
{

  function __construct()
  {
  }
  public static function loadALLDC(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT d.uid,d.created_at FROM `dc` AS d WHERE d.uid IN (SELECT dc_id FROM `dc_items`) ORDER BY d.uid DESC";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

     public static function loadALLDCItems($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
    $sql="SELECT d.uid,d.created_at,di.instrument_type_details_id,d.reference_standard,d.note,di.expected_date,tm.t_type,tm.d_type 
          FROM `dc` AS d,`dc_items` AS di,`transport_masters` AS tm 
          WHERE d.uid=di.dc_id AND d.transport_id=tm.t_id AND d.uid='".$id."'";
   
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function loadALLDCInstrument($instrument_type_details_id,$dc_id){

     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

      $sql=" SELECT itd.instrument_name,itd.instrument_no,itd.next_calibration_date,it.instrument_type_name,ct.calibrator_type_name,cd.calibrator_name,cd.calibrator_address,di.expected_date,itd.uom
          FROM `dc_items` AS di, `instrument_type_details` AS itd,`instrument_type` AS it,`calibrator_details` AS cd,`calibrator_instrument` AS ci,`calibrator_type` AS ct 
          WHERE di.instrument_type_details_id=itd.uid AND
            itd.instrument_type_id=it.uid AND itd.active_flag=1 
          AND ci.instrument_type_id=it.uid
           AND cd.uid IN(SELECT calibrator_details_id FROM `calibrator_instrument`) 
           AND cd.calibrator_type_id=ct.uid 
           AND ci.calibrator_details_id=cd.uid
           AND itd.uid='".$instrument_type_details_id."'
           AND di.dc_id='".$dc_id."'";

    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     
    $row=mysql_fetch_assoc($rs_result);
   
    return $row;
   }
 }

