<?php
include_once("config.php");
  class manageUser
{

  function __construct()
  {
  }
    public static function manageUser($id){
      $con = mysqli_connect(DBHOST , DBUSER  ,DBPASS , DBNAME);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }

      $sql="SELECT u.name,d.designation,u.designation_id,u.user_name,u.image_path,urm.role_id
            FROM `user` AS u LEFT JOIN `user_role_mapping` AS urm ON u.uid=urm.user_id,`designation` AS d 
            WHERE u.designation_id=d.uid AND u.uid='".$id."'";
     $rs_result = mysqli_query ($con,$sql)  or die("error");
     mysqli_close($con);
    $data=array();
    while($row=mysqli_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

     public static function updateUser($loginName,$designation,$userName,$updated_by,$id,$image_path){
      $con = mysqli_connect(DBHOST , DBUSER  ,DBPASS , DBNAME);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }

      $sql="UPDATE  `user`SET `user_name`='".$userName."',`name`='".$loginName."',`designation_id`='".$designation."',`updated_by`='".$updated_by."',`updated_at`='".date('Y-m-d')."',`image_path`='".$image_path."' WHERE uid='".$id."' ";
     $rs_result = mysqli_query ($con,$sql)  or die("error");

       $sql1="DELETE FROM `user_role_mapping` WHERE  user_id='".$id."' ";
     $rs_result1 = mysqli_query ($con,$sql1)  or die("error");
     mysqli_close($con);
    return $rs_result;
   }


     public static function updateUserPassword($password,$updated_by,$id){
      $con = mysqli_connect(DBHOST , DBUSER  ,DBPASS , DBNAME);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }

      $sql="UPDATE  `user`SET `password`='".$password."',`updated_by`='".$updated_by."',`updated_at`='".date('Y-m-d')."' WHERE uid='".$id."' ";
     $rs_result = mysqli_query ($con,$sql)  or die("error");
     mysqli_close($con);
    return $rs_result;
   }

 }