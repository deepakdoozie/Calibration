<?php
include_once("config.php");

  class calibrationDetails
{

  function __construct()
  {
  }
  public static function loadCalibrationDetails(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    // $sql=" SELECT itd.instrument_name,cbtr.calibrator_name,cd.calibration_details,cd.calibration_date,cd.uid,cd.calibration_certificate_path,ct.calibrator_type_name 
    //        FROM `calibrator_details` AS cbtr,`instrument_type_details` AS itd,`calibration_details` AS cd,`calibrator_type` AS ct 
    //        WHERE cd.instrument_type_details_id=itd.uid AND itd.active_flag=1  AND ct.uid=cbtr.calibrator_type_id ORDER BY cd.uid DESC";
     
     $sql="SELECT itd.instrument_name,cd.calibration_details,cd.calibration_date,cd.uid,cd.calibration_certificate_path,itd.instrument_no,it.instrument_type_name
          
 FROM `instrument_type_details` AS itd,`calibration_details` AS cd,`instrument_type` AS it

  WHERE cd.instrument_type_details_id=itd.uid AND itd.active_flag=1 AND it.uid=itd.instrument_type_id  ORDER BY cd.uid DESC";

    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function editSpecificCalibration($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT itd.instrument_name,cd.calibration_details,cd.calibration_date,cd.calibration_charges,cd.other_charges,cd.calibration_certificate_path,cd.calibration_report_no,cd.reference_standard,cd.punch_code,cd.remarks,it.instrument_type_name,itd.instrument_no
           FROM `instrument_type_details` AS itd,`calibration_details` AS cd,`instrument_type` AS it 
           WHERE cd.instrument_type_details_id=itd.uid AND itd.active_flag=1 AND it.uid=itd.instrument_type_id  AND cd.uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
  $row=mysql_fetch_assoc($rs_result);
   
    return $row;
   }

       public static function loadAllcalibratorType(){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `calibrator_type`";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }


  public static function updateCalibrationDetails($id,$calibrationDetails,$nextCalibrationDate,$calibrationCharges,$otherCharges,$file,$reportNo,$reffStd,$code,$remark,$login_by){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="UPDATE `calibration_details` SET `calibration_details`='".$calibrationDetails."',`calibration_date`='".$nextCalibrationDate."',`calibration_charges`='".$calibrationCharges."',`other_charges`='".$otherCharges."',`calibration_certificate_path`='".$file."',`calibration_report_no`='".$reportNo."',`reference_standard`='".$reffStd."',`punch_code`='".$code."',`remarks`='".$remark."',`updated_by`='".$login_by."',`updated_at`='".date('Y-m-d')."'  WHERE uid='".$id."'";
     $rs_result = mysql_query ($sql)  or die("error");
  
    $sql1="SELECT `instrument_type_details_id` FROM `calibration_details` WHERE uid='".$id."'";
    $rs_result1 = mysql_query ($sql1)  or die("error1");
    $row=mysql_fetch_assoc($rs_result1);

    $instrument_type_details_id=$row['instrument_type_details_id'];

     $sql2="UPDATE `instrument_type_details` SET first_calibration_date='".$nextCalibrationDate."',`punch_code`='".$code."' WHERE uid='".$instrument_type_details_id."'";
    $rs_result2 = mysql_query ($sql2)  or die("error1");


    return $rs_result;
   }

     public static function loadCalibrator($calibratorTypeId){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
   
    $sql=" SELECT uid,calibrator_name FROM `calibrator_details` WHERE active_flag=1 AND calibrator_type_id='".$calibratorTypeId."'";
 
    $rs_result = mysql_query ($sql)  or die("error1");
     
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   
   }
 }