<?php
include_once("config.php");

  class issueInstrumentDetails
{

  function __construct()
  {
  }
  public static function loadAllCalibratorType(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT * FROM `calibrator_type`";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }


  public static function loadSpecificCalibrator($calibrator_type_id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT * FROM `calibrator_details` WHERE calibrator_type_id='".$calibrator_type_id."'";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
    
     
     $row=mysql_fetch_assoc($rs_result);
     
    return $row;
   }


     public static function loadSpecificCalibratorInstrument($calibrator_id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT it.instrument_type_name,it.uid 
           FROM `calibrator_details` cd,`calibrator_instrument` AS ci,`instrument_type` AS it 
           WHERE cd.uid=ci.calibrator_details_id AND ci.instrument_type_id=it.uid AND cd.uid='".$calibrator_id."'";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }


 public static function loadSpecificInstrumentDetails($instrument_type_id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT itd.uid,itd.instrument_name,itd.instrument_no,itd.remarks,it.instrument_type_name,itd.uom FROM `instrument_type` AS it,`instrument_type_details` AS itd WHERE it.uid=itd.instrument_type_id AND itd.active_flag=1 AND itd.issue_flag=0 AND it.uid='".$instrument_type_id."'";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }


    public static function loadSpecificInstrumentName($instrument_name_id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT * FROM `instrument_type_details`  WHERE uid='".$instrument_name_id."' AND active_flag=1 AND issue_flag=0";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     
     
     $row=mysql_fetch_assoc($rs_result);
   
    return $row;
   }

public static function saveDC($refStd,$note,$transport_id,$login_by,$add_calibrator_type){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" INSERT INTO dc(`reference_standard`,`note`,`transport_id`,`created_by`,`calibrator_type_id`) VALUES('".$refStd."','".$note."','".$transport_id."','".$login_by."','".$add_calibrator_type."')";
    $rs_result = mysql_query ($sql)  or die("error1");
    

    $sql1="SELECT max(uid) AS uid FROM `dc`";
    $rs_result1 = mysql_query ($sql1)  or die("error2");
     mysql_close($con);
     
     
     $row=mysql_fetch_assoc($rs_result1);
   
    return $row['uid'];
   }

   public static function saveDCItems($dc_id,$instrument_name_id,$e_date){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" INSERT INTO dc_items(`dc_id`,`instrument_type_details_id`,`expected_date`) VALUES('".$dc_id."','".$instrument_name_id."','".$e_date."')";
    $rs_result = mysql_query ($sql)  or die("error1");
   
   $sql1="UPDATE `instrument_type_details` SET issue_flag=1 WHERE uid='".$instrument_name_id."'";
    $rs_result1 = mysql_query ($sql1)  or die("error1");

    return $rs_result1;
   }

    public static function loadALLDC(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

     $sql="SELECT ct.calibrator_type_name,d.uid,cd.calibrator_contact_no,count(di.uid) AS total,d.created_at

           FROM `instrument_type_details` AS itd,`calibrator_details` AS cd,`calibrator_type` AS ct,`dc_items` AS di,`dc` AS d

           WHERE  itd.active_flag=1 AND itd.issue_flag=1
                 -- AND ci.calibrator_details_id=cd.uid
                 AND cd.calibrator_type_id=ct.uid
                 AND di.instrument_type_details_id=itd.uid
AND d.uid=di.dc_id AND d.calibrator_type_id=ct.uid GROUP By di.dc_id ORDER BY d.uid DESC";

   /* $sql2=" SELECT ct.calibrator_type_name,d.uid,cd.calibrator_contact_no,count(di.uid) AS total,d.created_at

           FROM `instrument_type` AS it,`instrument_type_details` AS itd,`calibrator_details` AS cd,`calibrator_instrument` AS ci,`calibrator_type` AS ct,`dc_items` AS di,`dc` AS d

           WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=1
                 AND it.uid=ci.instrument_type_id
                 AND ci.calibrator_details_id=cd.uid
                 AND cd.calibrator_type_id=ct.uid
                 AND di.instrument_type_details_id=itd.uid
AND d.uid=di.dc_id GROUP By di.dc_id";*/
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

      public static function viewDCItems($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT d.reference_standard,d.note,d.created_date,di.expected_date,itd.instrument_name,itd.instrument_no,itd.next_calibration_date,it.instrument_type_name,ct.calibrator_type_name,cd.calibrator_name 
          FROM `dc` AS d,`dc_items` AS di,`instrument_type_details` AS itd,`instrument_type` AS it,`calibrator_details` AS cd,`calibrator_instrument` AS ci,`calibrator_type` AS ct 
          WHERE d.uid =di.dc_id AND di.instrument_type_details_id=itd.uid AND itd.instrument_type_id=it.uid AND itd.active_flag=1 AND d.uid='".$id."' AND 
               ci.instrument_type_id=it.uid AND cd.uid IN(SELECT calibrator_details_id FROM `calibrator_instrument`) AND cd.calibrator_type_id=ct.uid";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

   public static function loadALLDCItems($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
    $sql="SELECT d.uid,d.created_at,di.instrument_type_details_id,d.reference_standard,d.note,di.expected_date FROM `dc` AS d,`dc_items` AS di WHERE d.uid=di.dc_id AND d.uid='".$id."'";
   
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function loadALLDCInstrument($instrument_type_details_id,$dc_id){

     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

      $sql=" SELECT itd.instrument_name,itd.instrument_no,itd.next_calibration_date,it.instrument_type_name,ct.calibrator_type_name,cd.calibrator_name,cd.calibrator_address,di.expected_date,itd.uom,ct.calibrator_type_name,cd.calibrator_name
          FROM `dc_items` AS di, `instrument_type_details` AS itd,`instrument_type` AS it,`calibrator_details` AS cd,`calibrator_instrument` AS ci,`calibrator_type` AS ct 
          WHERE di.instrument_type_details_id=itd.uid AND
            itd.instrument_type_id=it.uid AND itd.active_flag=1 
          AND ci.instrument_type_id=it.uid
           AND cd.uid IN(SELECT calibrator_details_id FROM `calibrator_instrument`) 
           AND cd.calibrator_type_id=ct.uid 
           AND ci.calibrator_details_id=cd.uid
           AND itd.uid='".$instrument_type_details_id."'
           AND di.dc_id='".$dc_id."'";

    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     
    $row=mysql_fetch_assoc($rs_result);
   
    return $row;
   }

    public static function loadAllTranport(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT * FROM `transport_masters`";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

       public static function loadAllDueInstrumentCalibration($calibrator_id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

     $sql="SELECT itd.instrument_name,itd.instrument_no,itd.first_calibration_date,itd.calibration_alert_days,it.instrument_type_name,itd.uid,itd.uom,ct.uid as calibrator_type_id,ct.calibrator_type_name
           FROM `instrument_type` AS it,`instrument_type_details` AS itd,`calibrator_details` AS cd,`calibrator_type` AS ct
           WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=0 AND itd.uid NOT IN(SELECT instrument_type_details_id FROM `calibration_details`)
                  AND cd.calibrator_type_id=ct.uid GROUP BY itd.uid";

    
    $rs_result = mysql_query ($sql)  or die("error1");
     // mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    }
// return $data;

    for($i=0;$i<count($data);$i++)
    {
       $sql2="SELECT ct.uid,ct.calibrator_type_name FROM `calibrator_instrument` as ci,`calibrator_details` AS cd,`calibrator_type` AS ct WHERE ci.calibrator_details_id=cd.uid AND cd.calibrator_type_id=ct.uid AND ci.instrument_type_id=".$data[$i]['uid']."";
     
       // var_dump($sql2,$i,11,$data[$i]["uid"]);
       $rs_result2 = mysql_query($sql2)or die("error12");
       
       // return $rs_result2;
      
       $calibrator_type_data=array();    
       while($row2=mysql_fetch_assoc($rs_result2))
        {
          $calibrator_type_data[]=$row2;
        } 
      $data[$i]['calibrator_type_array']=$calibrator_type_data;
    }
 mysql_close($con);  
    return $data;

    
   }




   public static function loadAllPendingInstrumentCalibration(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

     $sql="SELECT itd.instrument_name,itd.instrument_no,itd.first_calibration_date,itd.calibration_alert_days,it.instrument_type_name,itd.uid,itd.uom,ct.uid as calibrator_type_id,ct.calibrator_type_name
           FROM `instrument_type` AS it,`instrument_type_details` AS itd,`calibrator_details` AS cd,`calibrator_type` AS ct
           WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=0 AND itd.uid NOT IN(SELECT instrument_type_details_id FROM `calibration_details`)
                  AND cd.calibrator_type_id=ct.uid GROUP BY itd.uid";

    
    $rs_result = mysql_query ($sql)  or die("error1");
     // mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    }
// return $data;

    for($i=0;$i<count($data);$i++)
    {
       $sql2="SELECT ct.uid,ct.calibrator_type_name FROM `calibrator_instrument` as ci,`calibrator_details` AS cd,`calibrator_type` AS ct WHERE ci.calibrator_details_id=cd.uid AND cd.calibrator_type_id=ct.uid AND ci.instrument_type_id=".$data[$i]['uid']."";
     
       // var_dump($sql2,$i,11,$data[$i]["uid"]);
       $rs_result2 = mysql_query($sql2)or die("error12");
       
       // return $rs_result2;
      
       $calibrator_type_data=array();    
       while($row2=mysql_fetch_assoc($rs_result2))
        {
          $calibrator_type_data[]=$row2;
        } 
      $data[$i]['calibrator_type_array']=$calibrator_type_data;
    }
 mysql_close($con);  
    return $data;
   }




      public static function loadAllDueCalibration($calibrator_id){

    
    $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

     $sql="SELECT itd.instrument_name,itd.instrument_no,itd.first_calibration_date,itd.calibration_alert_days,it.instrument_type_name,itd.uid,itd.uom,ct.uid as calibrator_type_id,ct.calibrator_type_name
           FROM `instrument_type` AS it,`instrument_type_details` AS itd,`calibrator_details` AS cd,`calibrator_type` AS ct
           WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=0 AND itd.uid NOT IN(SELECT instrument_type_details_id FROM `calibration_details`)
                  AND cd.calibrator_type_id=ct.uid GROUP BY itd.uid";

    
    $rs_result = mysql_query ($sql)  or die("error1");
     // mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    }
// return $data;

    for($i=0;$i<count($data);$i++)
    {
       $sql2="SELECT ct.uid,ct.calibrator_type_name FROM `calibrator_instrument` as ci,`calibrator_details` AS cd,`calibrator_type` AS ct WHERE ci.calibrator_details_id=cd.uid AND cd.calibrator_type_id=ct.uid AND ci.instrument_type_id=".$data[$i]['uid']."";
     
       // var_dump($sql2,$i,11,$data[$i]["uid"]);
       $rs_result2 = mysql_query($sql2)or die("error12");
       
       // return $rs_result2;
      
       $calibrator_type_data=array();    
       while($row2=mysql_fetch_assoc($rs_result2))
        {
          $calibrator_type_data[]=$row2;
        } 
      $data[$i]['calibrator_type_array']=$calibrator_type_data;
    }
 mysql_close($con);  
    return $data;
   }


 }