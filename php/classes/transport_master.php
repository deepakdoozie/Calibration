<?php 
  include_once("config.php");

  class TransportData
  {
    var $t_id ;
    var $t_type;
    var $d_type;
    var $desc;
    
    function __construct($t_id,$t_type,$d_type,$desc)
    {
      $this->t_id = $t_id;
      $this->t_type=$t_type;
      $this->d_type=$d_type;
      $this->desc=$desc;
      
    }
  }

class Transport
{
  function __construct()
  {
  }

  public static function createTransportMaster(TransportData $c)
  {
    $con =mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME);
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }
    mysqli_query($con,"INSERT INTO `transport_masters`(`t_type` ,`d_type` ,`desc`) VALUES ('". $c->t_type."','". $c->d_type."','". $c->desc."')") or die("error") ;
    mysqli_close($con);
  }


  public static function  updateTransportMaster(TransportData $c)
  {
    $con = mysqli_connect(DBHOST , DBUSER  ,DBPASS , DBNAME);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }
    $query ="UPDATE `transport_masters` SET  `t_type`='". $c->t_type."',`d_type`='". $c->d_type."',`desc`='". $c->desc."' WHERE t_id='". $c->t_id ."'";

    mysqli_query($con , $query) or die("error");

    mysqli_close($con);

  }

  public static function getAllTransportMaster()
  {
    $con = mysql_connect(DBHOST,DBUSER,DBPASS);
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }
    $db= mysql_select_db(DBNAME, $con) ;

    $sql = "SELECT  *  FROM `transport_masters` order by t_id";
    $rs_result = mysql_query ($sql)  or die("error");
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
  }

  public static function checkTransportMaster(TransportData $c)
  {
    $con = mysql_connect(DBHOST,DBUSER,DBPASS);
    $db= mysql_select_db(DBNAME, $con);
    $sql = "SELECT  *  FROM  `transport_masters` WHERE `t_type`='". $c->t_type."' AND `d_type`='". $c->d_type."'";
    $rs_result = mysql_query ($sql) or die("error");
    $row=mysql_fetch_assoc($rs_result);
    return $row;
  }

  public static function getSpecificTransportMaster($t_id)
  {
    $con = mysql_connect(DBHOST,DBUSER,DBPASS);
    $db= mysql_select_db(DBNAME, $con);
    $sql = "SELECT  *  FROM  `transport_masters` WHERE t_id=". $t_id ."";
    $rs_result = mysql_query ($sql) or die("error");
  
    $row=mysql_fetch_assoc($rs_result);
     
    return $row;
  }

  public static function getTID($t_id)
  {
    $con = mysql_connect(DBHOST,DBUSER,DBPASS);
    $db= mysql_select_db(DBNAME, $con);
    $sql = "SELECT  t_id  FROM  `transport_masters` WHERE t_type='". $t_id ."'";
    $rs_result = mysql_query ($sql) or die("error");
  
    $row=mysql_fetch_assoc($rs_result);
     
    return $row['t_id'];
  }
}