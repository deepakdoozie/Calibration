<?php
include_once("config.php");

  class calibrationPendingDetails
{

  function __construct()
  {
  }
  public static function loadAllPendingCalibration(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT itd.instrument_name,itd.purchesed_by,itd.purchesed_date,itd.instrument_no,itd.first_calibration_date,itd.calibration_alert_days,it.instrument_type_name,itd.uid,itd.uom,ct.uid as calibrator_type_id,ct.calibrator_type_name
           FROM `instrument_type` AS it,`instrument_type_details` AS itd,`calibrator_details` AS cd,`calibrator_type` AS ct
           WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=0 AND itd.uid NOT IN(SELECT instrument_type_details_id FROM `calibration_details`)
                  AND cd.calibrator_type_id=ct.uid GROUP BY itd.uid";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function loadAllPendingInstrumentCalibration(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT itd.instrument_name,itd.instrument_no,itd.purchesed_by,itd.purchesed_date,itd.first_calibration_date,itd.next_calibration_date,itd.calibration_alert_days,it.instrument_type_name 
           FROM `instrument_type` AS it,`instrument_type_details` AS itd 
           WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=0";

    $sql_one=" SELECT itd.instrument_name,itd.instrument_no,itd.purchesed_by,itd.purchesed_date,itd.next_calibration_date,itd.first_calibration_date,it.instrument_type_name,itd.calibration_alert_days,itd.calibration_frequency 
           FROM `instrument_type` AS it,`instrument_type_details` AS itd 
           WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=0 AND itd.uid NOT IN(SELECT instrument_type_details_id FROM `calibration_details`)";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }
 }