<?php
include_once("config.php");

  class calibrationDetails
{

  function __construct()
  {
  }
  public static function loadAllCalibration(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    // $sql=" SELECT itd.uid,itd.instrument_name,itd.instrument_no,itd.purchesed_by,itd.purchesed_date,itd.first_calibration_date,itd.next_calibration_date,itd.calibration_frequency,itd.calibration_alert_days,it.instrument_type_name 
    //        FROM `instrument_type` AS it,`instrument_type_details` AS itd,`calibration_details` AS cd 
    //        WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=1 AND itd.uid IN(SELECT instrument_type_details_id FROM `calibration_details`) AND itd.uid=cd.instrument_type_details_id";
     
     /*$old_sql=" SELECT itd.uid,itd.instrument_name,itd.instrument_no,itd.first_calibration_date,itd.next_calibration_date,itd.calibration_frequency,itd.calibration_alert_days,it.instrument_type_name,ct.calibrator_type_name,di.dc_id
           FROM `instrument_type` AS it,`instrument_type_details` AS itd,`calibrator_details` AS cd,`calibrator_instrument` AS ci,`calibrator_type` AS ct,`dc_items` AS di
           WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=1 
                 AND it.uid=ci.instrument_type_id
                 AND ci.calibrator_details_id=cd.uid
                 AND cd.calibrator_type_id=ct.uid
                 AND di.instrument_type_details_id=itd.uid";*/

     $sql=" SELECT itd.uid,itd.instrument_name,itd.instrument_no,it.instrument_type_name,di.dc_id
           FROM `instrument_type` AS it,`instrument_type_details` AS itd,`dc_items` as di
           WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=1 AND di.instrument_type_details_id=itd.uid
                  GROUP BY di.dc_id,di.instrument_type_details_id";            
    
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function loadAllInstrumentCalibration(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT itd.uid,itd.instrument_name,itd.instrument_no,itd.purchesed_by,itd.purchesed_date,itd.first_calibration_date,itd.next_calibration_date,itd.calibration_frequency,itd.calibration_alert_days,it.instrument_type_name 
           FROM `instrument_type` AS it,`instrument_type_details` AS itd 
           WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.issue_flag=1 AND itd.uid NOT IN(SELECT instrument_type_details_id FROM `calibration_details`)";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

 public static function viewSpecificInstrument($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT itd.uid,itd.instrument_no,itd.instrument_name,itd.purchesed_by,itd.purchesed_date,itd.next_calibration_date,it.instrument_type_name,itd.calibration_frequency,itd.calibration_alert_days,itd.purchesed_cost 
         FROM `instrument_type` AS it,`instrument_type_details` AS itd
         WHERE it.uid=itd.instrument_type_id  
         AND itd.uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
    $row=mysql_fetch_assoc($rs_result);
    
    return $row;
   }

    public static function loadAllcalibratorType(){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `calibrator_type`";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

      public static function saveCalibrationDetails($id,$calibrationDetails,$nextCalibrationDate,$calibrationCharges,$otherCharges,$file,$reportNo,$reffStd,$code,$remark,$login_by){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
    $sql="UPDATE `instrument_type_details` SET first_calibration_date='".$nextCalibrationDate."',`punch_code`='".$code."',issue_flag=0,email_flag=1, updated_at='".date('Y-m-d')."' WHERE uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error1");

    $sql2="INSERT INTO `calibration_details`(`instrument_type_details_id`,`calibration_details`,`calibration_date`,`calibration_charges`,`other_charges`,`calibration_certificate_path`,`calibration_report_no`,`reference_standard`,`punch_code`,`remarks`,`created_by`) VALUES('".$id."','".$calibrationDetails."','".$nextCalibrationDate."','".$calibrationCharges."','".$otherCharges."','".$file."','".$reportNo."','".$reffStd."','".$code."','".$remark."','".$login_by."');";
    $rs_result2 = mysql_query ($sql2)  or die("error2");
  
    return $rs_result2;
   }

       public static function loadAllInstrumentType(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `instrument_type`";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

 public static function loadSpecificInstrumentName($id){ 
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT uid,instrument_name FROM `instrument_type_details` WHERE instrument_type_id='".$id."' AND active_flag=1";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function loadSelectedInstrumentDetails($instrumentId,$nameId){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
   
    $sql=" SELECT itd.uid,itd.instrument_name,itd.instrument_no,itd.purchesed_by,itd.purchesed_date,itd.first_calibration_date,itd.next_calibration_date,itd.calibration_frequency,itd.calibration_alert_days,it.instrument_type_name FROM `instrument_type` AS it,`instrument_type_details` AS itd WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.instrument_type_id='".$instrumentId."' AND itd.uid='".$nameId."'";
 
    $rs_result = mysql_query ($sql)  or die("error1");
     
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   
   }

    public static function loadCalibrator($calibratorTypeId){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
   
    $sql=" SELECT uid,calibrator_name FROM `calibrator_details` WHERE active_flag=1 AND calibrator_type_id='".$calibratorTypeId."'";
 
    $rs_result = mysql_query ($sql)  or die("error1");
     
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   
   }
 }