<?php
include_once("config.php");

  class countDetails
{

  function __construct()
  {
  }
  public static function countAllInstrumentType(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT count(uid) AS total FROM `instrument_type`";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
      
     $row=mysql_fetch_assoc($rs_result);
    
    return $row;
   }

    public static function countAllInstrument(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT count(uid) AS total FROM `instrument_type_details` WHERE active_flag=1";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
      
     $row=mysql_fetch_assoc($rs_result);
    
    return $row;
   }

    public static function countAllPendingReport(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT itd.instrument_name,itd.instrument_no,itd.purchesed_by,itd.purchesed_date,itd.next_calibration_date,itd.calibration_frequency,itd.calibration_alert_days,it.instrument_type_name FROM `instrument_type` AS it,`instrument_type_details` AS itd, `calibration_details` AS cd WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.uid IN(SELECT instrument_type_details_id FROM `calibration_details`) AND itd.uid=cd.instrument_type_details_id";

    $rs_result = mysql_query ($sql)  or die("error1");
      mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

     public static function countAllInstrumentPendingReport(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT itd.instrument_name,itd.instrument_no,itd.purchesed_by,itd.purchesed_date,itd.next_calibration_date,itd.first_calibration_date,it.instrument_type_name,itd.calibration_alert_days,itd.calibration_frequency FROM `instrument_type` AS it,`instrument_type_details` AS itd WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.uid NOT IN(SELECT instrument_type_details_id FROM `calibration_details`)";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

   public static function loadAllDueCalibration(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT itd.instrument_name,itd.instrument_no,itd.purchesed_by,itd.purchesed_date,itd.next_calibration_date,itd.calibration_frequency,itd.calibration_alert_days,it.instrument_type_name FROM `instrument_type` AS it,`instrument_type_details` AS itd,`calibration_details` AS cd WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.uid IN(SELECT instrument_type_details_id FROM `calibration_details`) AND itd.uid=cd.instrument_type_details_id";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function loadAllDueInstrumentCalibration(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT itd.instrument_name,itd.instrument_no,itd.purchesed_by,itd.purchesed_date,itd.first_calibration_date,itd.calibration_alert_days,it.instrument_type_name FROM `instrument_type` AS it,`instrument_type_details` AS itd WHERE itd.instrument_type_id=it.uid AND itd.active_flag=1 AND itd.uid NOT IN(SELECT instrument_type_details_id FROM `calibration_details`)";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function countDiscardedInstrument(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT count(uid) AS total FROM `instrument_faults`";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
      
     $row=mysql_fetch_assoc($rs_result);
    
    return $row;
   }

    public static function countIssuedInstrument(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT count(uid) AS total FROM `instrument_type_details` WHERE active_flag=1 AND issue_flag=1";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
      
     $row=mysql_fetch_assoc($rs_result);
    
    return $row;
   }
 }