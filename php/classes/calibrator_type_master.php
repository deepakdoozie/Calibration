<?php
include_once("config.php");

  class calibratorDetails
{

  function __construct()
  {
  }


  public static function checkCalibratorTypeForAdd($cname)
  {
    $con =mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME);
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }
  
    $sql="SELECT uid FROM `calibrator_type` WHERE calibrator_type_name='".$cname."'";
    $rs_result = mysqli_query ($con,$sql)  or die("error");
    $row=mysqli_fetch_assoc($rs_result);
    return $row['uid'];
  }


   public static function checkCalibratorTypeForUpdate($id,$cname)
  {
    $con =mysqli_connect(DBHOST,DBUSER,DBPASS,DBNAME);
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }
   // $user_id=1;
    $sql="SELECT cm.uid FROM `calibrator_type` as cm WHERE cm.calibrator_type_name='".$cname."' AND cm.uid !='".$id."'";
    $rs_result = mysqli_query ($con,$sql)  or die("error");
    $row=mysqli_fetch_assoc($rs_result);
    return $row['uid'];
  }



  public static function savecalibratorType($cname,$description,$login_by){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="INSERT INTO `calibrator_type` (`calibrator_type_name`, `calibrator_type_description`,`created_by`) VALUES ('". $cname ."','". $description ."','".$login_by."')";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
    return $rs_result;
   }

    public static function getAllcalibratorType(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `calibrator_type`";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

   public static function viewSpecificcalibratorType($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `calibrator_type` WHERE uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
    $row=mysql_fetch_assoc($rs_result);
   
    return $row;
   }

    public static function editSpecificcalibratorType($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `calibrator_type` WHERE uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
    $row=mysql_fetch_assoc($rs_result);
    return $row;
   }

     public static function updatecalibratorType($id,$cname,$desc,$login_by){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="UPDATE `calibrator_type` SET `calibrator_type_name`='".$cname."',`calibrator_type_description`='".$desc."',`updated_by`='".$login_by."',`updated_at`='".date('Y-m-d')."' WHERE uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
    
    return $rs_result;
   }

    public static function loadAllcalibratorType(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT calibrator_type_name FROM `calibrator_type`";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }
 }