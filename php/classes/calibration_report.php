<?php
include_once("config.php");

  class calibrationDetailsReport
{

  function __construct()
  {
  }
  public static function loadAllCalibration(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT cd.instrument_type_details_id,itd.instrument_name,itd.instrument_no,ct.calibrator_type_name,cd.calibration_details,cd.calibration_date,it.instrument_type_name,it.uid,it.instrument_description
          FROM `calibrator_type` AS ct,`instrument_type_details` AS itd,`calibration_details` AS cd,`calibrator_details` AS cbtr,`instrument_type` AS it 
          WHERE itd.uid=cd.instrument_type_details_id AND itd.active_flag=1 AND it.uid=itd.instrument_type_id AND cbtr.calibrator_type_id=ct.uid GROUP BY it.uid";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

     public static function loadAllcalibratorType(){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `calibrator_type`";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function loadCalibrationForSelectedInstriment($id){

     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT cd.instrument_type_details_id,itd.instrument_name,itd.instrument_no,cd.calibration_details,cd.calibration_date,cd.calibration_certificate_path,cd.calibration_charges,cd.other_charges,it.instrument_type_name 
           FROM `instrument_type_details` AS itd,`calibration_details` AS cd,`instrument_type` AS it 
           WHERE itd.uid=cd.instrument_type_details_id AND itd.active_flag=1 AND itd.issue_flag=0 AND it.uid=itd.instrument_type_id  AND it.uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error1");
     mysql_close($con);
     $data=array();
     
     while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }
 }