<?php
include_once("config.php");
class employeeData{
    var $eid;
    var $fname;
    var $mname;
    var $lname;
    var $dob;
    var $doj;
    var $designation;
    var $employmentType;
    var $department;
    var $esi;
    var $pf;
    var $cno;
    var $ecno;
    var $email;
    var $bld;
    var $conAdrs;
    var $pem;
    var $bankName;
    var $accNo;
    var $ifsc;
    var $pan;
    var $payment;
    var $login_by;
  
    function __construct($eid,$fname,$mname,$lname,$dob,$doj,$designation,$department,$esi,$pf,$cno,$ecno,$email,$bld,$conAdrs,$pem,$bankName,$accNo,$ifsc,$pan,$payment,$employmentType,$login_by)
    {
      $this->eid = $eid;
      $this->fname = $fname;
      $this->mname = $mname;
      $this->lname = $lname;
      $this->dob=$dob;
      $this->doj=$doj;

      $this->designation=$designation;
      $this->department=$department;
      $this->esi=$esi;
      $this->pf=$pf;
      $this->cno=$cno;
      $this->ecno = $ecno;
      $this->email = $email;
      $this->bld = $bld;
      $this->conAdrs =$conAdrs;
      $this->pem = $pem;
      $this->bankName=$bankName;

       $this->accNo=$accNo;
        $this->ifsc=$ifsc;
         $this->pan=$pan;
          $this->payment=$payment;
          $this->employmentType=$employmentType;
      $this->login_by=$login_by;

    }
  }


  class editEmployeeData{
    var $eid;
    var $fname;
    var $mname;
    var $lname;
    var $dob;
    var $doj;
    var $designation;
    var $employmentType;
    var $department;
    var $esi;
    var $pf;
    var $cno;
    var $ecno;
    var $email;
    var $bld;
    var $conAdrs;
    var $pem;
    var $bankName;
    var $accNo;
    var $ifsc;
    var $pan;
    var $payment;
    var $updateId;
    var $login_by;
    
    function __construct($eid,$fname,$mname,$lname,$dob,$doj,$designation,$department,$esi,$pf,$cno,$ecno,$email,$bld,$conAdrs,$pem,$bankName,$accNo,$ifsc,$pan,$payment,$employmentType,$updateId,$login_by)
    {
      $this->eid = $eid;
      $this->fname = $fname;
      $this->mname = $mname;
      $this->lname = $lname;
      $this->dob=$dob;
      $this->doj=$doj;

      $this->designation=$designation;
      $this->department=$department;
      $this->esi=$esi;
      $this->pf=$pf;
      $this->cno=$cno;
      $this->ecno = $ecno;
      $this->email = $email;
      $this->bld = $bld;
      $this->conAdrs =$conAdrs;
      $this->pem = $pem;
      $this->bankName=$bankName;

       $this->accNo=$accNo;
        $this->ifsc=$ifsc;
         $this->pan=$pan;
          $this->payment=$payment;
          $this->employmentType=$employmentType;
          $this->updateId=$updateId;
           $this->login_by=$login_by;

    }
  }



  class employeeDetails
{

  function __construct()
  {
  }

  public static function checkEmployeeID($employeeID){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT uid FROM `employee` WHERE e_id='".$employeeID."'";
    $rs_result = mysql_query ($sql)  or die("error in insert");
    $row=mysql_fetch_assoc($rs_result);
    mysql_close($con); 
    return $row['uid'];
   }

   public static function checkEmployeeIDForUpdate($update_id,$edit_eid){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `employee` WHERE e_id=".$edit_eid." AND uid !=".$update_id."";
    $rs_result = mysql_query ($sql)  or die("error in insert");
    $row=mysql_fetch_assoc($rs_result);
    mysql_close($con); 
    return $row;
   }

  public static function addEmployee(employeeData $obj){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="INSERT INTO `employee` (`e_id`, `first_name`,`middle_name`,`last_name`, `dob`, `doj`,`designation`,`employment_type_id`,`department`,`esi_no`,`pf_no`, `con_no`, `econ_no`, `email`,`blood`,`con_adrs`,`pem_adrs`,`bank_name`,`account_no`,`ifsc_code`,`pan_no`,`mode_of_payment_id`,`created_by`) VALUES ('". $obj->eid ."','". $obj->fname ."','". $obj->mname ."','". $obj->lname ."', '". $obj->dob ."', '". $obj->doj ."','". $obj->designation ."','". $obj->employmentType ."','". $obj->department ."','". $obj->esi ."','". $obj->pf ."', '". $obj->cno ."', '". $obj->ecno ."', '". $obj->email ."', '". $obj->bld ."', '". $obj->conAdrs ."', '". $obj->pem ."','". $obj->bankName ."', '". $obj->accNo ."', '". $obj->ifsc ."', '". $obj->pan ."', '". $obj->payment ."', '". $obj->login_by ."')";
    $rs_result = mysql_query ($sql)  or die("error in insert");
    $sql2="UPDATE `employment_type` SET used_flag=1 WHERE employment_type_uid='".$obj->employmentType."'";
    $rs_result1 = mysql_query ($sql2)  or die("error in updating");
    mysql_close($con); 
    return $rs_result;
   }

   public static function updateEmply(editEmployeeData $obj)
    {
    $con = mysql_connect(DBHOST,DBUSER,DBPASS);
    $db= mysql_select_db(DBNAME, $con);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
   
    $sql="UPDATE `employee` SET `e_id`='". $obj->eid ."',`first_name`='". $obj->fname ."',`middle_name`='". $obj->mname ."',`last_name`='". $obj->lname ."',`dob`='". $obj->dob ."',`doj`='". $obj->doj ."',`designation`='". $obj->designation ."',`employment_type_id`='". $obj->employmentType ."',`department`='". $obj->department ."',`esi_no`='". $obj->esi ."',`pf_no`='". $obj->pf ."',`con_no`='". $obj->cno ."',`econ_no`='". $obj->ecno ."',`email`='". $obj->email ."',`blood`= '". $obj->bld ."',`con_adrs`='". $obj->conAdrs ."',`pem_adrs`='". $obj->pem ."',`bank_name`='". $obj->bankName ."',`account_no`='". $obj->accNo ."',`ifsc_code`='". $obj->ifsc ."',`pan_no`='". $obj->pan ."',`mode_of_payment_id`='". $obj->payment ."',`updated_by`='".$obj->login_by."',`updated_at`='".date('Y-m-d')."' WHERE uid='". $obj->updateId ."'";
    $rs_result = mysql_query ($sql)  or die("error in update");
    mysql_close($con); 
    return $rs_result;
  }

   public static function getAllEmployee(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `employee`  ORDER BY uid DESC ";
    $rs_result = mysql_query ($sql)  or die("error in insert");
    mysql_close($con); 
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function loadEmploymentType(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `employment_type` WHERE active_flag=1";
    $rs_result = mysql_query ($sql)  or die("error in insert");
    mysql_close($con); 
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

   public static function loadModeOfPaymentType(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `mode_of_payments` ";
    $rs_result = mysql_query ($sql)  or die("error in insert");
    mysql_close($con); 
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

   public static function updateFlag1($id){
      $con =mysql_connect(DBHOST,DBUSER,DBPASS);
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }
      $db= mysql_select_db(DBNAME,$con);
       
    $sql = "UPDATE employee SET active_flag =1 WHERE uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
     mysql_close($con); 
    
    //$row=mysql_fetch_assoc($rs_result);
    
    return $rs_result;
    }

     public static function updateFlag0($id){
      $con =mysql_connect(DBHOST,DBUSER,DBPASS);
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }
      $db= mysql_select_db(DBNAME,$con);
       
    $sql = "UPDATE employee SET active_flag=0 WHERE uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
     mysql_close($con); 
    
    //$row=mysql_fetch_assoc($rs_result);
    
    return $rs_result;
    }

    public static function checkEmployeeFlag($id){
      $con =mysql_connect(DBHOST,DBUSER,DBPASS);
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }
      $db= mysql_select_db(DBNAME,$con);
       
    $sql = "SELECT * FROM `employee` WHERE uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
    $row=mysql_fetch_assoc($rs_result);
     
    return $row['active_flag'];
   }

    public static function viewEmply($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `employee` AS e,`mode_of_payments` AS mop,`employment_type` AS et WHERE e.uid='".$id."' AND e.mode_of_payment_id=mop.mode_of_payment_uid AND e.employment_type_id=et.employment_type_uid ";
    $rs_result = mysql_query ($sql)  or die("error in insert");
    mysql_close($con); 
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

   public static function editEmply($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `employee` AS e,`mode_of_payments` AS mop,`employment_type` AS et WHERE e.uid='".$id."' AND e.mode_of_payment_id=mop.mode_of_payment_uid AND e.employment_type_id=et.employment_type_uid ";
    $rs_result = mysql_query ($sql)  or die("error in insert");
    mysql_close($con); 
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    

   public static function loadname(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT uid,first_name,middle_name,last_name FROM `employee`WHERE active_flag=1 ";
    $rs_result = mysql_query ($sql)  or die("error in insert");
    mysql_close($con); 
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function searchEmplyName($eid,$date){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    if(strlen($eid)<=0){
    $sql="SELECT * FROM `employee` WHERE  doj <='".$date."' ORDER BY uid DESC";
  }
  else if(strlen($date)<=0){
    $sql="SELECT * FROM `employee` WHERE  uid LIKE '%".$eid."%' ORDER BY uid DESC";

  }
  else{
  $sql="SELECT * FROM `employee` WHERE  doj ='".$date."' AND uid ='".$eid."' ORDER BY uid DESC ";
  }
    $rs_result = mysql_query ($sql)  or die("error in search ");
    mysql_close($con); 
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

}