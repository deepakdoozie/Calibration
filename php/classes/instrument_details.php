<?php
include_once("config.php");

    class instrumentDetails
{

  function __construct()
  {
  }
  public static function saveInstrumentDetails($instument_id,$add_iname,$add_ino,$add_pname,$add_pdate,$add_cdate,$add_cfreq,$add_adays,$add_cost,$file,$code,$remarks,$used,$uom,$login_by){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="INSERT INTO `instrument_type_details` (`instrument_type_id`, `instrument_name`,`instrument_no`,`purchesed_by`, `purchesed_date`, `first_calibration_date`,`next_calibration_date`,`calibration_frequency`,`calibration_alert_days`,`purchesed_cost`,`document_path`,`punch_code`,`remarks`,`used_at`,`uom`,`created_by`) VALUES ('". $instument_id ."','". $add_iname ."','". $add_ino ."','". $add_pname ."', '". $add_pdate ."', '". $add_cdate ."','". $add_cdate ."','". $add_cfreq ."','". $add_adays ."','". $add_cost ."','". $file ."','". $code ."','". $remarks ."','".$used."','".$uom."','".$login_by."')";
    $rs_result = mysql_query ($sql)  or die("error in insert");

   

 
    $sql1="SELECT max(uid) AS uid FROM `instrument_type_details`";
    $rs_result1 = mysql_query ($sql1)  or die("error");
    $row=mysql_fetch_assoc($rs_result1);


     $sql2="INSERT INTO `calibration_details_history`(`instrument_type_details_id`,`calibration_date`,`created_by`) VALUES('".$row['uid']."','".$add_cdate."','".$login_by."');";
    $rs_result2 = mysql_query ($sql2)  or die("error2");

   
    mysql_close($con); 
  
    
 
    return $row['uid'];
   }

    public static function assignEmployeeToInstrument($eid,$instrument_id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="INSERT INTO `employee_instrument` (`instrument_type_details_id`, `employee_id`) VALUES ('". $instrument_id ."','". $eid ."')";
    $rs_result = mysql_query ($sql)  or die("error in insert");
 
    mysql_close($con); 

    return $rs_result;
   }

      public static function loadAllInstrumentType(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `instrument_type`";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

      public static function loadAllInstrumentName(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT instrument_name FROM `instrument_type_details`";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

         public static function loadAllEmployee(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT uid,first_name,middle_name,last_name,e_id FROM `employee` WHERE active_flag=1";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

      public static function getAllInstrument(){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT itd.uid,itd.instrument_no,itd.instrument_name,itd.purchesed_by,itd.purchesed_date,it.instrument_type_name,itd.first_calibration_date,itd.next_calibration_date FROM `instrument_type` AS it,`instrument_type_details` AS itd WHERE it.uid=itd.instrument_type_id AND itd.active_flag=1 ORDER BY uid DESC";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

  public static function viewSpecificInstrument($id){

     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT itd.uom,itd.uid,itd.instrument_no,itd.instrument_name,itd.purchesed_by,itd.purchesed_date,itd.first_calibration_date,itd.next_calibration_date,it.instrument_type_name,e.first_name,e.middle_name,e.last_name,itd.calibration_frequency,itd.calibration_alert_days,itd.purchesed_cost,itd.document_path,itd.punch_code,itd.remarks,itd.used_at FROM `instrument_type` AS it,`instrument_type_details` AS itd,`employee` AS e,`employee_instrument` AS ei WHERE it.uid=itd.instrument_type_id AND itd.uid=ei.instrument_type_details_id AND e.uid=ei.employee_id AND itd.uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
      $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

public static function editSpecificInstrument($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT itd.uom,itd.uid,itd.instrument_no,itd.instrument_name,itd.purchesed_by,itd.purchesed_date,itd.first_calibration_date,itd.next_calibration_date,it.instrument_type_name,e.first_name,e.middle_name,e.last_name,itd.calibration_frequency,itd.calibration_alert_days,itd.purchesed_cost,itd.document_path,itd.punch_code,itd.remarks,itd.used_at,ei.employee_id,itd.instrument_type_id 
         FROM `instrument_type` AS it,`instrument_type_details` AS itd,`employee` AS e,`employee_instrument` AS ei 
         WHERE it.uid=itd.instrument_type_id AND itd.uid=ei.instrument_type_details_id AND e.uid=ei.employee_id AND itd.uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
      $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function updateInstrumentDetails($instument_id,$add_iname,$add_ino,$add_pname,$add_pdate,$add_cdate,$add_cfreq,$add_adays,$add_cost,$uom,$file,$id,$code,$remark,$used,$login_by){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="UPDATE `instrument_type_details` SET `instrument_type_id`='". $instument_id ."', `instrument_name`='". $add_iname ."',`instrument_no`='". $add_ino ."',`purchesed_by`='". $add_pname ."', `purchesed_date`='". $add_pdate ."', `first_calibration_date`= '". $add_cdate ."',`next_calibration_date`= '". $add_cdate ."',`calibration_frequency`='". $add_cfreq ."',`calibration_alert_days`='". $add_adays ."',`purchesed_cost`='". $add_cost ."',`uom`='". $uom ."',`document_path`='". $file ."',`punch_code`='".$code."',`remarks`='".$remark."',`used_at`='".$used."',`updated_by`='".$login_by."',`updated_at`='".date('Y-m-d')."' WHERE uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error in insert");
    
    $sql1=" DELETE FROM `employee_instrument` WHERE instrument_type_details_id='".$id."'";
  $rs_result1 = mysql_query ($sql1)  or die("error in insert");
 
  
    mysql_close($con); 
    return $rs_result1;
   }

      public static function checkActiveFlag($id){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" SELECT active_flag FROM `instrument_type_details`WHERE uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
 
    mysql_close($con); 
     $row=mysql_fetch_assoc($rs_result);
    return $row['active_flag'];
   }

    public static function saveInstrumentFaults($id,$reason){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" UPDATE `instrument_type_details` SET active_flag=0 WHERE uid='".$id."'";
  $rs_result = mysql_query ($sql)  or die("error in insert");

   $sql1=" INSERT INTO `instrument_faults`(`instrument_type_details_id`,`reason`,`discarded_date`) VALUES('".$id."','".$reason."','".date('Y-m-d')."')";
  $rs_result1 = mysql_query ($sql1)  or die("error in insert");
 
    mysql_close($con); 
   
    return $rs_result1;
   }

       public static function updateActiveFlag($id){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql=" UPDATE `instrument_type_details` SET active_flag=1 WHERE uid='".$id."'";
  $rs_result = mysql_query ($sql)  or die("error in insert");

   $sql1=" DELETE FROM `instrument_faults` WHERE instrument_type_details_id='".$id."'";
  $rs_result1 = mysql_query ($sql1)  or die("error in insert");
 
    mysql_close($con); 
   
    return $rs_result1;
   }

      public static function updateAassignEmployeeToInstrument($eid,$instrument_id){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="INSERT INTO `employee_instrument` (`instrument_type_details_id`, `employee_id`,`created_at`) VALUES ('". $instrument_id ."','". $eid ."','".date('Y-m-d')."')";
    $rs_result = mysql_query ($sql)  or die("error in insert");
 
    mysql_close($con); 

    return $rs_result;
   }
 }