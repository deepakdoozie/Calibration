<?php
include_once("config.php");

  class calibratorDetails
{

  function __construct()
  {
  }
  public static function addCalibratorDetails($calibrator_id,$calibrator_name,$calibrator_email,$calibrator_contact,$contact_name,$contact_number,$additional_contact_number,$email,$additional_email,$credit_days,$add_address,$login_by){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="INSERT INTO `calibrator_details` (`calibrator_type_id`, `calibrator_name`,`calibrator_email`,`calibrator_contact_no`, `contact_person_name`,`contact_person_no`, `additional_contact_person_no`,`contact_person_email`, `additional_email`,`credit_days`,`calibrator_address`,`created_by`) VALUES ('". $calibrator_id ."','". $calibrator_name ."','". $calibrator_email ."','". $calibrator_contact ."','". $contact_name ."','". $contact_number ."','". $additional_contact_number ."','". $email ."','". $additional_email ."','". $credit_days ."','".$add_address."','".$login_by."')";
    $rs_result = mysql_query ($sql)  or die("error1");

     $sql2="SELECT max(uid) AS calibration_details_id FROM `calibrator_details` ";
    $rs_result2 = mysql_query ($sql2)  or die("error2");
    $row=mysql_fetch_assoc($rs_result2);
   
    mysql_close($con); 
    return $row['calibration_details_id'];
    // return $sql;

   }


 public static function saveCalibratorInstrument($id,$instrument_type_id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="INSERT INTO `calibrator_instrument` (`instrument_type_id`, `calibrator_details_id`) VALUES ('". $instrument_type_id ."','". $id ."')";
    $rs_result = mysql_query ($sql)  or die("error1");
   
    mysql_close($con); 
    return $rs_result;
    
   }
    public static function loadAllcalibratorType(){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `calibrator_type`";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

   public static function loadCalibrator(){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT * FROM `calibrator_type` WHERE uid NOT IN( SELECT calibrator_type_id  FROM `calibrator_details` WHERE active_flag=1)";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

      public static function loadAllcalibratorname(){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT uid,calibrator_name FROM `calibrator_details` WHERE active_flag=1";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }


      public static function loadALLInstrumentType(){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    // $sql="SELECT * FROM `instrument_type` WHERE uid NOT IN(SELECT instrument_type_id FROM `calibrator_instrument`)";
    // $rs_result = mysql_query ($sql)  or die("error");
     $sql="SELECT * FROM `instrument_type`";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

     public static function loadAllCalibratorDetails(){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT cd.uid,cd.calibrator_name,cd.calibrator_email,cd.calibrator_contact_no,ct.calibrator_type_name,cd.active_flag FROM `calibrator_details` AS cd,`calibrator_type` AS ct WHERE ct.uid=cd.calibrator_type_id  ORDER BY cd.uid DESC";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
     $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

        public static function viewSpecificCalibratorDetails($id){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT cd.uid,cd.calibrator_name,cd.calibrator_email,cd.calibrator_contact_no,cd.contact_person_name,cd.contact_person_no,cd.additional_contact_person_no,cd.contact_person_email,cd.additional_email,cd.credit_days,cd.calibrator_address,ct.calibrator_type_name,it.instrument_type_name,it.instrument_description
    FROM `calibrator_details` AS cd,`calibrator_type` AS ct,`calibrator_instrument` AS ci,`instrument_type` AS it
     WHERE ct.uid=cd.calibrator_type_id AND ci.calibrator_details_id=cd.uid AND ci.instrument_type_id=it.uid AND cd.active_flag=1 AND cd.uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
    // return $sql;
   }

 public static function editSpecificCalibratorDetails($id){
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="SELECT cd.uid,cd.calibrator_name,cd.calibrator_email,cd.calibrator_contact_no,cd.contact_person_name,cd.contact_person_no,cd.additional_contact_person_no,cd.contact_person_email,cd.additional_email,cd.credit_days,cd.calibrator_address,ct.calibrator_type_name,it.instrument_type_name,cd.calibrator_type_id,it.uid AS instrument_type_id,it.instrument_description FROM `calibrator_details` AS cd,`calibrator_type` AS ct,`calibrator_instrument` AS ci,`instrument_type` AS it WHERE ct.uid=cd.calibrator_type_id AND ci.calibrator_details_id=cd.uid AND ci.instrument_type_id=it.uid AND cd.active_flag=1 AND cd.uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
   
    mysql_close($con); 
    $data=array();
    while($row=mysql_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

    public static function updateCalibratorDetails($calibrator_id,$calibrator_name,$calibrator_email,$calibrator_contact,$contact_name,$contact_number,$additional_contact_number,$email,$additional_email,$credit_days,$add_address,$id,$login_by){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }

    $sql="UPDATE  `calibrator_details` SET `calibrator_type_id`='". $calibrator_id ."', `calibrator_name`='". $calibrator_name ."',`calibrator_email`='". $calibrator_email ."',`calibrator_contact_no`='". $calibrator_contact ."', `contact_person_name`='". $contact_name ."',`contact_person_no`='". $contact_number ."', `additional_contact_person_no`='". $additional_contact_number ."',`contact_person_email`='". $email ."', `additional_email`='". $additional_email ."',`credit_days`='". $credit_days ."',`calibrator_address`='".$add_address."',`updated_by`='".$login_by."',`updated_at`='".date('Y-m-d')."' WHERE uid='".$id."'";
    $rs_result = mysql_query ($sql)  or die("error");
    mysql_close($con); 
    return $rs_result;

   }

   public static function deleteCalibratorInstrument($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
    $sql="DELETE FROM `calibrator_instrument` WHERE calibrator_details_id='".$id."'";

    $rs_result = mysql_query ($sql)  or die("error in delete");
    mysql_close($con); 
    return $rs_result;
    
   }


 public static function updateCalibratorInstrument($id,$instrument_id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
   

    $sql1="INSERT INTO `calibrator_instrument` (`instrument_type_id`, `calibrator_details_id`) VALUES ('". $instrument_id ."','". $id ."')";
    $rs_result = mysql_query ($sql1)  or die("error1");
   
    mysql_close($con); 
    return $rs_result;
    
   }

   public static function checkActiveFlag($id){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
    $sql="SELECT `active_flag` FROM `calibrator_details` WHERE uid='".$id."'";

    $rs_result = mysql_query ($sql)  or die("error1");
   $row=mysql_fetch_assoc($rs_result);
   
   
    mysql_close($con); 
    return $row['active_flag'];
    // return $sql;
    
   }

    public static function updateActiveFlag($id,$active_flag){

    
     $con =mysql_connect(DBHOST,DBUSER,DBPASS);
     $db= mysql_select_db(DBNAME, $con); 
    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysql_connect_error();
      return;
    }
    $sql="UPDATE `calibrator_details` SET active_flag='".$active_flag."' WHERE uid='".$id."'";

    $rs_result = mysql_query ($sql)  or die("error02");
  
   
   
    mysql_close($con); 
    return $rs_result;
    
   }
 }