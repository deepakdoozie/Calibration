<?php
include_once("config.php");


  class addUser
{

  function __construct()
  {
  }


  public static function getAllDesignation(){
      $con = mysqli_connect(DBHOST , DBUSER  ,DBPASS , DBNAME);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }

      $sql="SELECT * FROM `designation`";
     $rs_result = mysqli_query ($con,$sql)  or die("error");
     mysqli_close($con);
     $data=array();
    while($row=mysqli_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

   public static function saveUser($loginName,$password,$designation,$userName,$login_by,$image_path){
      $con = mysqli_connect(DBHOST , DBUSER  ,DBPASS , DBNAME);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }

      $sql="INSERT INTO `user`(`name`,`password`,`designation_id`,`user_name`,`created_by`,`image_path`) VALUES('".$loginName."','".$password."','".$designation."','".$userName."','".$login_by."','".$image_path."')";
     $rs_result = mysqli_query ($con,$sql)  or die("error");
     
      $sql1="SELECT MAX(uid) AS id FROM `user`";
     $rs_result1 = mysqli_query ($con,$sql1)  or die("error");
  
    $row=mysqli_fetch_assoc($rs_result1);
    


     mysqli_close($con);
    return $row['id'];
   }

     public static function loadAllUser(){
      $con = mysqli_connect(DBHOST , DBUSER  ,DBPASS , DBNAME);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }

      $sql="SELECT u.uid,u.name,d.designation,u.active_flag,u.user_name 
            FROM `user` AS u,`designation` AS d 
            WHERE u.designation_id=d.uid ORDER BY u.uid DESC";
     $rs_result = mysqli_query ($con,$sql)  or die("error");
     mysqli_close($con);
     $data=array();
    while($row=mysqli_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

     public static function activeDeactiveUser($id){
      $con = mysqli_connect(DBHOST , DBUSER  ,DBPASS , DBNAME);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }

      $sql="SELECT `active_flag` FROM `user` WHERE uid='".$id."'";
     $rs_result = mysqli_query ($con,$sql)  or die("error");
    $row=mysqli_fetch_assoc($rs_result);

    $flag=$row['active_flag'];
    if($flag){
    $sql1="UPDATE `user` SET `active_flag`=0 WHERE uid='".$id."'";
     $rs_result1 = mysqli_query ($con,$sql1)  or die("error");
    }
    else{
  $sql1="UPDATE `user` SET `active_flag`=1 WHERE uid='".$id."'";
  $rs_result1 = mysqli_query ($con,$sql1)  or die("error");
    }
    
    return $rs_result1;
   }


  public static function getAllRoles(){
      $con = mysqli_connect(DBHOST , DBUSER  ,DBPASS , DBNAME);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }

      $sql="SELECT * FROM `roles`";
     $rs_result = mysqli_query ($con,$sql)  or die("error");
     mysqli_close($con);
     $data=array();
    while($row=mysqli_fetch_assoc($rs_result))
    {
      $data[]=$row;
    } 
    return $data;
   }

     public static function saveUserRole($id,$role_id){
      $con = mysqli_connect(DBHOST , DBUSER  ,DBPASS , DBNAME);

    if (mysqli_connect_errno())
    {
      echo "Failed to connect to Server: " . mysqli_connect_error();
      return;
    }

      $sql="INSERT INTO `user_role_mapping`(`user_id`, `role_id`) VALUES ('".$id."','".$role_id."')";
     $rs_result = mysqli_query ($con,$sql)  or die("error");
    
     mysqli_close($con);
    return $rs_result;
   }


 }