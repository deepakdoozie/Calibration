<?php
  require_once('config.php');
  session_start();
  session_destroy();
  $_SESSION["user_id"]=0;
  unset($_SESSION["user_id"]);
   header('Content-type: application/json');
  echo json_encode($_SESSION["user_id"]);
