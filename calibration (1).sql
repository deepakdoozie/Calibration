-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 18, 2015 at 10:49 AM
-- Server version: 5.1.36-community
-- PHP Version: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `calibration`
--
CREATE DATABASE IF NOT EXISTS `calibration` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `calibration`;

-- --------------------------------------------------------

--
-- Table structure for table `calibration_details`
--

CREATE TABLE IF NOT EXISTS `calibration_details` (
  `uid` int(25) NOT NULL AUTO_INCREMENT,
  `instrument_type_details_id` int(25) NOT NULL,
  `calibration_details` varchar(1000) DEFAULT NULL,
  `calibration_date` date DEFAULT NULL,
  `calibration_charges` varchar(100) DEFAULT NULL,
  `other_charges` varchar(100) DEFAULT NULL,
  `calibration_certificate_path` varchar(100) DEFAULT NULL,
  `calibration_report_no` varchar(25) DEFAULT NULL,
  `reference_standard` varchar(25) DEFAULT NULL,
  `punch_code` varchar(25) DEFAULT NULL,
  `remarks` varchar(1000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `calibration_details`
--

INSERT INTO `calibration_details` (`uid`, `instrument_type_details_id`, `calibration_details`, `calibration_date`, `calibration_charges`, `other_charges`, `calibration_certificate_path`, `calibration_report_no`, `reference_standard`, `punch_code`, `remarks`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 'xcxzczxc', '2015-06-09', '33', '33', '', 'ttttttttttttttt', 'sdasdasd', 'dasd', 'asdasdasdasd', '2015-06-18 08:25:18', 1, '2015-06-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `calibrator_details`
--

CREATE TABLE IF NOT EXISTS `calibrator_details` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `calibrator_type_id` int(10) NOT NULL,
  `calibrator_name` varchar(250) DEFAULT NULL,
  `calibrator_email` varchar(25) DEFAULT NULL,
  `calibrator_contact_no` double DEFAULT NULL,
  `contact_person_name` varchar(250) DEFAULT NULL,
  `contact_person_no` varchar(25) DEFAULT NULL,
  `additional_contact_person_no` varchar(25) DEFAULT NULL,
  `contact_person_email` varchar(100) DEFAULT NULL,
  `additional_email` varchar(100) DEFAULT NULL,
  `credit_days` int(10) DEFAULT NULL,
  `calibrator_address` varchar(1000) DEFAULT NULL,
  `active_flag` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `calibrator_details`
--

INSERT INTO `calibrator_details` (`uid`, `calibrator_type_id`, `calibrator_name`, `calibrator_email`, `calibrator_contact_no`, `contact_person_name`, `contact_person_no`, `additional_contact_person_no`, `contact_person_email`, `additional_email`, `credit_days`, `calibrator_address`, `active_flag`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 'dsfsdf', 'ff@ss.ss', 8722720132, 'fsfsdfsdf', '', '', '', '', 0, 'dsfsdfsdfsdfsdf', 1, '2015-06-18 06:51:06', 1, NULL, NULL),
(2, 1, 'ff', 'ss@sws.ss', 8722720132, 'dsfdsfsdfsdfd', '', '', '', '', 0, 'jhv ccccccccccccccc', 1, '2015-06-18 06:51:55', 1, '2015-06-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `calibrator_instrument`
--

CREATE TABLE IF NOT EXISTS `calibrator_instrument` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `instrument_type_id` int(11) NOT NULL,
  `calibrator_details_id` int(10) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `calibrator_instrument`
--

INSERT INTO `calibrator_instrument` (`uid`, `instrument_type_id`, `calibrator_details_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(6, 1, 2, '2015-06-18 07:02:29', NULL, NULL, NULL),
(7, 2, 2, '2015-06-18 07:02:29', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `calibrator_type`
--

CREATE TABLE IF NOT EXISTS `calibrator_type` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `calibrator_type_name` varchar(100) DEFAULT NULL,
  `calibrator_type_description` varchar(1000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `calibrator_type`
--

INSERT INTO `calibrator_type` (`uid`, `calibrator_type_name`, `calibrator_type_description`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'NABL', 'vb', '0000-00-00 00:00:00', NULL, NULL, NULL),
(2, 'NCPL', 'NCPL', '0000-00-00 00:00:00', NULL, NULL, NULL),
(3, 'rrrrtttttttttttt', 'rrrrttttttttttttttttyyyyyyyyyyy', '2015-06-18 06:23:50', 1, '2015-06-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `dc`
--

CREATE TABLE IF NOT EXISTS `dc` (
  `uid` int(25) NOT NULL AUTO_INCREMENT,
  `reference_standard` varchar(1000) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `transport_id` int(250) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `dc`
--

INSERT INTO `dc` (`uid`, `reference_standard`, `note`, `transport_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'aa', 'dsfsdfsdf', 1, '2015-06-18 07:43:25', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `dc_items`
--

CREATE TABLE IF NOT EXISTS `dc_items` (
  `uid` int(25) NOT NULL AUTO_INCREMENT,
  `dc_id` int(25) NOT NULL,
  `instrument_type_details_id` int(25) DEFAULT NULL,
  `expected_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `dc_items`
--

INSERT INTO `dc_items` (`uid`, `dc_id`, `instrument_type_details_id`, `expected_date`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 1, '2015-06-07', '2015-06-18 07:43:25', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `email_backup`
--

CREATE TABLE IF NOT EXISTS `email_backup` (
  `uid` int(250) NOT NULL AUTO_INCREMENT,
  `to` varchar(250) DEFAULT NULL,
  `subject` varchar(500) DEFAULT NULL,
  `message` varchar(1000) DEFAULT NULL,
  `header` varchar(500) DEFAULT NULL,
  `send_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `uid` int(100) NOT NULL AUTO_INCREMENT,
  `e_id` varchar(100) DEFAULT NULL,
  `first_name` varchar(250) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `doj` date DEFAULT NULL,
  `designation` varchar(250) DEFAULT NULL,
  `employment_type_id` int(10) DEFAULT NULL,
  `department` varchar(100) DEFAULT NULL,
  `esi_no` varchar(100) DEFAULT NULL,
  `pf_no` varchar(100) DEFAULT NULL,
  `con_no` varchar(25) DEFAULT NULL,
  `econ_no` varchar(25) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `blood` varchar(10) DEFAULT NULL,
  `con_adrs` varchar(1000) DEFAULT NULL,
  `pem_adrs` varchar(1000) DEFAULT NULL,
  `bank_name` varchar(250) DEFAULT NULL,
  `account_no` varchar(250) DEFAULT NULL,
  `ifsc_code` varchar(100) DEFAULT NULL,
  `pan_no` varchar(100) DEFAULT NULL,
  `mode_of_payment_id` int(10) NOT NULL,
  `active_flag` int(100) NOT NULL DEFAULT '1',
  `ss_revision_id` int(10) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`uid`, `e_id`, `first_name`, `middle_name`, `last_name`, `dob`, `doj`, `designation`, `employment_type_id`, `department`, `esi_no`, `pf_no`, `con_no`, `econ_no`, `email`, `blood`, `con_adrs`, `pem_adrs`, `bank_name`, `account_no`, `ifsc_code`, `pan_no`, `mode_of_payment_id`, `active_flag`, `ss_revision_id`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '1', 's', 's', 's', '1991-10-15', '2015-05-14', 'SE', 1, 'mnf', '', '', '9880086637', '', 'sa@wer.com', '', 'qaz', '', '', '', '', '', 1, 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL),
(2, 'df', 'dsff', 'sdfsdf', 'dsfsdfds', '2015-06-02', '2015-06-02', 'dsfdsf', 1, 'dsfdsf', 'dsfsd', 'fsdfsd', '8722720132', '', 'dd@ss.ss', '', 'ssss', '', '', '', '', '', 1, 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL),
(3, '2222222222222', 'fgdf', 'fdgdf', 'gdfgdfgdfg', '2015-06-16', '2015-06-08', 'dfgdfgdf', 1, 'dfgdfgdfg', 'dfgdfg', 'dfgdfg', '8722720132', '', 'bbb@ff.ff', '', 'dfssdfdsfsdf', '', '', '', '', '', 1, 1, 1, '2015-06-18 07:15:33', 1, '2015-06-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee_instrument`
--

CREATE TABLE IF NOT EXISTS `employee_instrument` (
  `uid` int(25) NOT NULL AUTO_INCREMENT,
  `instrument_type_details_id` int(25) DEFAULT NULL,
  `employee_id` int(25) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `employee_instrument`
--

INSERT INTO `employee_instrument` (`uid`, `instrument_type_details_id`, `employee_id`, `created_at`, `updated_at`) VALUES
(6, 5, 1, '2015-06-18', NULL),
(7, 5, 2, '2015-06-18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employment_type`
--

CREATE TABLE IF NOT EXISTS `employment_type` (
  `employment_type_uid` int(10) NOT NULL AUTO_INCREMENT,
  `employment_type_name` varchar(100) NOT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `used_flag` int(11) DEFAULT NULL,
  `active_flag` int(11) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`employment_type_uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `employment_type`
--

INSERT INTO `employment_type` (`employment_type_uid`, `employment_type_name`, `description`, `used_flag`, `active_flag`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'employee', 'employee', 1, 1, '0000-00-00 00:00:00', NULL, NULL, NULL),
(2, 'Developer', 'Developer111', NULL, 1, '0000-00-00 00:00:00', NULL, '2015-06-18', 1),
(3, 'R and D', 'R and D de', NULL, 1, '2015-06-18 07:25:56', 1, '2015-06-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `instrument_faults`
--

CREATE TABLE IF NOT EXISTS `instrument_faults` (
  `uid` int(10) NOT NULL AUTO_INCREMENT,
  `instrument_type_details_id` int(11) DEFAULT NULL,
  `reason` varchar(1500) DEFAULT NULL,
  `discarded_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `instrument_type`
--

CREATE TABLE IF NOT EXISTS `instrument_type` (
  `uid` int(100) NOT NULL AUTO_INCREMENT,
  `instrument_type_name` varchar(100) DEFAULT NULL,
  `instrument_description` varchar(1000) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `instrument_type`
--

INSERT INTO `instrument_type` (`uid`, `instrument_type_name`, `instrument_description`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'MRS', 'MRS', '0000-00-00 00:00:00', NULL, NULL, NULL),
(2, 'ffffffffff', 'fffffffffffffff', '2015-06-18 06:19:12', 1, '2015-06-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `instrument_type_details`
--

CREATE TABLE IF NOT EXISTS `instrument_type_details` (
  `uid` int(100) NOT NULL AUTO_INCREMENT,
  `instrument_type_id` int(100) NOT NULL,
  `instrument_name` varchar(100) DEFAULT NULL,
  `instrument_no` varchar(100) DEFAULT NULL,
  `purchesed_by` varchar(1000) DEFAULT NULL,
  `purchesed_date` date DEFAULT NULL,
  `used_at` varchar(1000) DEFAULT NULL,
  `first_calibration_date` date DEFAULT NULL,
  `next_calibration_date` date DEFAULT NULL,
  `calibration_frequency` int(100) DEFAULT NULL,
  `calibration_alert_days` int(100) DEFAULT NULL,
  `purchesed_cost` varchar(100) DEFAULT NULL,
  `assign_employee_id` int(11) DEFAULT NULL,
  `document_path` varchar(250) DEFAULT NULL,
  `punch_code` varchar(25) DEFAULT NULL,
  `remarks` varchar(1000) NOT NULL,
  `uom` varchar(25) DEFAULT NULL,
  `active_flag` int(10) NOT NULL DEFAULT '1',
  `calibration_flag` int(10) NOT NULL DEFAULT '1',
  `issue_flag` int(25) NOT NULL DEFAULT '0',
  `email_flag` int(250) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` date DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `instrument_type_details`
--

INSERT INTO `instrument_type_details` (`uid`, `instrument_type_id`, `instrument_name`, `instrument_no`, `purchesed_by`, `purchesed_date`, `used_at`, `first_calibration_date`, `next_calibration_date`, `calibration_frequency`, `calibration_alert_days`, `purchesed_cost`, `assign_employee_id`, `document_path`, `punch_code`, `remarks`, `uom`, `active_flag`, `calibration_flag`, `issue_flag`, `email_flag`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 1, 'ee', 'ee', '44', '2015-06-16', '', '2015-06-17', '2015-06-09', 44, 44, '444', NULL, '', 'dasd', 'dsfsd', '44', 1, 1, 0, 1, '0000-00-00 00:00:00', NULL, '2015-06-18', NULL),
(2, 1, 'ff', 'ff', 'sd', '2015-06-02', 'fds', '2015-06-03', '2015-06-03', 44, 444, '', NULL, '', '', '', 'ww', 1, 1, 0, 1, '0000-00-00 00:00:00', NULL, NULL, NULL),
(3, 1, 'rrrrrrrr', 'rrrrrr', 'rrr', '2015-06-01', 'rr', '2015-06-03', '2015-06-03', 44, 44, 'dsfsd', NULL, '', 'dfdsf', 'dsdsd', 'sdsdf', 1, 1, 0, 1, '2015-06-18 06:04:53', 1, NULL, NULL),
(4, 1, 'eew', 'ewrewr', 'werwer', '2015-06-10', 'ewrew', '2015-06-10', '2015-06-10', 44, 44, 'esadf', NULL, '', 'sdfsdf', '', 'ff', 1, 1, 0, 1, '2015-06-18 06:11:06', 1, NULL, NULL),
(5, 1, 'oooooooooooooooooo', 'ee', 'ee', '2015-06-04', 'ee', '2015-06-09', '2015-06-09', 33, 33, 'wee', NULL, '', 'ewrewr', '', 'eerer', 1, 1, 0, 1, '2015-06-18 06:12:32', 1, '2015-06-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mode_of_payments`
--

CREATE TABLE IF NOT EXISTS `mode_of_payments` (
  `mode_of_payment_uid` int(25) NOT NULL AUTO_INCREMENT,
  `mode_of_payment_name` varchar(100) NOT NULL,
  PRIMARY KEY (`mode_of_payment_uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `mode_of_payments`
--

INSERT INTO `mode_of_payments` (`mode_of_payment_uid`, `mode_of_payment_name`) VALUES
(1, 'Cash'),
(2, 'Cheque'),
(3, 'Demand Draft'),
(4, 'Net Banking');

-- --------------------------------------------------------

--
-- Table structure for table `transport_masters`
--

CREATE TABLE IF NOT EXISTS `transport_masters` (
  `t_id` int(25) NOT NULL AUTO_INCREMENT,
  `t_type` varchar(250) NOT NULL DEFAULT '',
  `d_type` varchar(250) NOT NULL DEFAULT '',
  `desc` varchar(250) NOT NULL DEFAULT '',
  PRIMARY KEY (`t_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `transport_masters`
--

INSERT INTO `transport_masters` (`t_id`, `t_type`, `d_type`, `desc`) VALUES
(1, 'Blue Dart', 'Courier1', 'N/A'),
(2, 'BY Hand', 'By hand', 'N/A'),
(3, 'DTDC', 'Courier', 'Desk to Desk'),
(4, 'Fedex', 'Courier', 'Federal Express'),
(5, 'SRS', 'Courier', 'SRS Travels'),
(6, 'TNT Express', 'TNT Express', 'TNT Express'),
(7, 'AirIndia', 'Flight', 'Speed and Quick'),
(8, 'GATI', 'Courier', 'GATI KWE');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `previlage` varchar(50) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `name`, `password`, `previlage`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(2, 'user', 'ee11cbb19052e40b07aac0ca060c23ee', 'admin'),
(3, 'account', 'e268443e43d93dab7ebef303bbe9642f', 'admin');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
